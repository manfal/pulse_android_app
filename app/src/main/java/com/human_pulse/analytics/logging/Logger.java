package com.human_pulse.analytics.logging;

import com.human_pulse.app.BuildConfig;

/**
 * Created by anfal on 12/12/2015.
 */
public class Logger {
    private Logger() {
    }

    public static ILog getLogger() {
        if (BuildConfig.DEBUG) {
            return new DebugLogging();
        } else {
            return new ReleaseLogging();
        }
    }
}
