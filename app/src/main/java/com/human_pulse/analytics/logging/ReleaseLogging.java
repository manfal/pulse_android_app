package com.human_pulse.analytics.logging;

import com.flurry.android.FlurryAgent;

/**
 * Created by anfal on 12/12/2015.
 */
public class ReleaseLogging implements ILog {
    @Override
    public void log(int logLevel, String tag, String message, Throwable exception) {
        FlurryAgent.onError(tag, logLevel + "", message);
    }
}
