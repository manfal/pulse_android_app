package com.human_pulse.analytics;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by anfal on 12/13/2015.
 */
public class LogEvents {
    //Event IDs
    public static final String EVENT_ID_PASSWORD_RESET = "EVENT_ID_PASSWORD_RESET";
    public static final String EVENT_ID_MAIN_STORE_DEPARTMENT = "EVENT_ID_MAIN_STORE_DEPARTMENT";
    public static final String EVENT_ID_MAIN_CUSTOMER_HELP = "EVENT_ID_MAIN_CUSTOMER_HELP";
    public static final String EVENT_ID_PERSONAL_INFO_EDIT = "EVENT_ID_PERSONAL_INFO_EDIT";
    public static final String EVENT_ID_NEW_ADDRESS_ADDITION = "EVENT_ID_NEW_ADDRESS_ADDITION";
    public static final String EVENT_ID_NUMBER_VERIFICATION = "EVENT_ID_NUMBER_VERIFICATION";
    //Triggered events
    public static final String TRIGGERED_RESET_BUTTON_CLICK_EVENT = "User triggered reset password.";
    public static final String TRIGGERED_NUMBER_VERIFICATION_MY_ACCOUNT = "User triggered phone verification from my account page.";
    public static final String TRIGGERED_NUMBER_VERIFICATION_CHECKOUT = "User triggered phone verification from checkout page.";
    public static final String TRIGGERED_ADDRESS_ADDITION_MY_ACCOUNT = "User triggered address addition from my account page.";
    public static final String TRIGGERED_ADDRESS_ADDITION_CHECKOUT = "User triggered address addition from checkout page.";
    public static final String TRIGGERED_RESET_BUTTON_MY_ACCOUNT_CLICK_EVENT = "User triggered reset password from my account page.";
    public static final String TRIGGERED_CUSTOMER_HELP_EVENT = "User triggered customer help.";
    public static final String TRIGGERED_MAIN_DEPARTMENT_LOAD_EVENT = "User clicked on department from store page.";
    public static final String TRIGGERED_LEFT_NAV_CATEGORY_ITEM_EVENT = "User clicked on category from left navigation drawer.";
    public static final String TRIGGERED_PERSONAL_INFO_EDIT = "User edited personal info from my account page, including phone number.";

    private LogEvents() {
    }

    public static Map<String, String> getEventData(String eventId, String[] data) {
        Map<String, String> parameters = new HashMap<>();
        for (String dataString : data) {
            parameters.put(eventId, dataString);
        }
        return parameters;
    }
}