package com.human_pulse.components.listeners;

import java.util.List;

public interface ParentListItemListener {

    List<?> getChildItemList();

    boolean isInitiallyExpanded();
}