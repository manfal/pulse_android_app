package com.human_pulse.components.listeners;

import android.view.View;
import android.view.View.OnClickListener;

import com.human_pulse.actions.CheckoutPageActions;

/**
 * Created by anfal on 3/13/2016.
 */
public class WantToEnterCouponCodeListener implements OnClickListener {
    @Override
    public void onClick(View v) {
        CheckoutPageActions.getCheckoutPageActionsInstance().toggleCouponCodeLayoutVisibility();
    }
}
