package com.human_pulse.components.listeners;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.app.CheckoutPage;
import com.human_pulse.app.LoginPage;
import com.human_pulse.utilities.Utilities;

import static com.human_pulse.constants.Constants.isCustomOrder;
import static com.human_pulse.constants.Constants.shouldUserBeRedirectedToCheckoutPage;

/**
 * Created by anfal on 1/27/2016.
 */
public class CheckoutButtonClickListener implements View.OnClickListener {

    private AppCompatActivity appCompatActivity = null;
    private ILog logger = null;

    public CheckoutButtonClickListener(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
        this.logger = Logger.getLogger();
    }

    @Override
    public void onClick(View v) {
        try {
            if (Utilities.getInstance().isSessionNotEmpty(this.appCompatActivity)) {
                Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, CheckoutPage.class);
            } else {
                shouldUserBeRedirectedToCheckoutPage = true;
                isCustomOrder = false;
                Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, LoginPage.class);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }
}
