package com.human_pulse.components.listeners;

import android.view.View;

import com.human_pulse.components.dataobjects.CartItemDataObject;
import com.human_pulse.components.observers.CartObserver;
import com.human_pulse.enumerations.EItemCheckout;

/**
 * Created by anfal on 1/13/2016.
 */
public class CheckoutItemChangeQuantityListener implements View.OnClickListener {

    private CartItemDataObject cartItemDataObject = null;
    private EItemCheckout eItemCheckout = null;

    public CheckoutItemChangeQuantityListener(CartItemDataObject cartItemDataObject, EItemCheckout eItemCheckout) {
        this.cartItemDataObject = cartItemDataObject;
        this.eItemCheckout = eItemCheckout;
    }

    @Override
    public void onClick(View v) {
        CartObserver.getCartObserverInstance().changeItemQuantityInCart(this.cartItemDataObject, this.eItemCheckout);
    }

}
