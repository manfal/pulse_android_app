package com.human_pulse.components.listeners;

import android.view.View;

import com.human_pulse.actions.PersonalAddressFragmentActionsSender;
import com.human_pulse.components.viewholder.AddressHolder;
import com.human_pulse.enumerations.EAddressManipulationChoice;

/**
 * Created by anfal on 1/6/2016.
 */
public class AddressEditListener implements View.OnClickListener {

    private AddressHolder addressHolder = null;

    public AddressEditListener(AddressHolder addressHolder) {
        this.addressHolder = addressHolder;
    }

    @Override
    public void onClick(View v) {
        PersonalAddressFragmentActionsSender.getPersonalAddressFragmentActions().fetchDataForAddressDialog(EAddressManipulationChoice.EDIT, this.addressHolder);
    }
}
