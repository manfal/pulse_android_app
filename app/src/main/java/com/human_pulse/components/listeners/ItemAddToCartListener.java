package com.human_pulse.components.listeners;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.app.R;
import com.human_pulse.components.dataobjects.DepartmentItemDataObject;
import com.human_pulse.components.observers.CartObserver;
import com.human_pulse.utilities.Utilities;

/**
 * Created by anfal on 1/18/2016.
 */
public class ItemAddToCartListener implements AdapterView.OnItemSelectedListener, View.OnTouchListener {

    private DepartmentItemDataObject departmentItemDataObject = null;
    private boolean userSelect = false;
    private ILog logger = null;
    private Spinner spinner = null;

    public ItemAddToCartListener(DepartmentItemDataObject departmentItemDataObject, Spinner spinner) {
        this.departmentItemDataObject = departmentItemDataObject;
        this.logger = Logger.getLogger();
        this.spinner = spinner;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        try {
            this.userSelect = true;
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
        return false;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        try {
            if (Utilities.getInstance().isInternetConnectionNotAvailable(view.getContext())) {
                this.spinner.setSelection(0);
                Toast.makeText(view.getContext(), view.getContext().getString(R.string.need_internet_to_change_item_quantity_text), Toast.LENGTH_LONG).show();
            } else {
                if (this.userSelect) {
                    this.userSelect = false;
                    CartObserver.getCartObserverInstance().addItemToCart(parent.getSelectedItem(), this.departmentItemDataObject);
                }
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
