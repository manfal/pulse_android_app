package com.human_pulse.components.listeners;

import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.human_pulse.actions.StoreActions;

/**
 * Created by anfal on 3/25/2016.
 */
public class StoreSearchBoxListener implements EditText.OnEditorActionListener {
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (EditorInfo.IME_ACTION_SEARCH == actionId) {
            StoreActions.getStoreActionInstance().sendSearchKeywordToSearchActivity(v.getText().toString());
            return true;
        }
        return false;
    }
}
