package com.human_pulse.components.listeners;

import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.human_pulse.actions.SearchActions;

/**
 * Created by anfal on 12/31/2015.
 */
public class SearchItemActionListener implements EditText.OnEditorActionListener {
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (EditorInfo.IME_ACTION_SEARCH == actionId) {
            SearchActions.getSearchActionsInstance().getSearchItems(v.getText().toString());
            return true;
        }
        return false;
    }
}
