package com.human_pulse.components.listeners;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.utilities.Utilities;

/**
 * Created by anfal on 3/28/2016.
 */
public class PlaceCustomOrderListener implements View.OnClickListener {

    private AppCompatActivity appCompatActivity = null;
    private ILog logger = null;

    public PlaceCustomOrderListener(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
        this.logger = Logger.getLogger();
    }

    @Override
    public void onClick(View v) {
        try {
            Utilities.getInstance().initiateCustomCheckout(this.appCompatActivity);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
