package com.human_pulse.components.listeners;

import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.human_pulse.actions.OrderHistoryActions;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.enumerations.EItemFetchChoice;

import static com.human_pulse.analytics.logging.Constants.ERROR;
import static com.human_pulse.analytics.logging.Constants.TAG_ERROR;

/**
 * Created by anfal on 1/8/2016.
 */
public class OrderHistoryScrollListener extends RecyclerView.OnScrollListener {
    private ILog logger = null;

    public OrderHistoryScrollListener() {
        this.logger = Logger.getLogger();
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        try {
            if (!recyclerView.canScrollVertically(1) && OrderHistoryActions.getOrderHistoryActionsInstance().isListFetchNecessary()) {
                OrderHistoryActions.getOrderHistoryActionsInstance().fetchOrderHistory(EItemFetchChoice.FETCHING_TO_APPEND);
            }
        } catch (Exception ex) {
            logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
