package com.human_pulse.components.listeners;

import android.view.View;
import android.view.ViewGroup;

import com.human_pulse.components.interfaces.IInternetConnectionObject;

/**
 * Created by anfal on 2/6/2016.
 */
public class InternetConnectivityButtonListener implements View.OnClickListener {

    private IInternetConnectionObject internetConnectionObject = null;
    private ViewGroup view = null;

    public InternetConnectivityButtonListener(IInternetConnectionObject internetConnectionObject, ViewGroup view) {
        this.internetConnectionObject = internetConnectionObject;
        this.view = view;
    }

    @Override
    public void onClick(View v) {
        if (null != this.view) {
            this.view.setVisibility(View.GONE);
        }
        this.internetConnectionObject.tryToReconnect();
    }
}
