package com.human_pulse.components.listeners;

import android.view.View;

import com.human_pulse.components.interfaces.IAddressResponseSenderReceiver;

/**
 * Created by anfal on 1/6/2016.
 */
public class NewAddressListener implements View.OnClickListener {

    private IAddressResponseSenderReceiver addressResponseSenderReceiver = null;

    public NewAddressListener(IAddressResponseSenderReceiver addressResponseSenderReceiver) {
        this.addressResponseSenderReceiver = addressResponseSenderReceiver;
    }

    @Override
    public void onClick(View v) {
        this.addressResponseSenderReceiver.openNewAddressDialog();
    }
}