package com.human_pulse.components.listeners;

import android.view.View;

import com.human_pulse.components.interfaces.IPhoneNumberVerificationInterface;

/**
 * Created by anfal on 1/4/2016.
 */
public class MyAccountVerifyPhoneNumberListener implements View.OnClickListener {

    private IPhoneNumberVerificationInterface phoneNumberVerificationInterface = null;

    public MyAccountVerifyPhoneNumberListener(IPhoneNumberVerificationInterface phoneNumberVerificationInterface) {
        this.phoneNumberVerificationInterface = phoneNumberVerificationInterface;
    }

    @Override
    public void onClick(View v) {
        this.phoneNumberVerificationInterface.handleVerifyPhoneNumberLinkClick();
    }
}
