package com.human_pulse.components.listeners;

import android.view.View;

import com.human_pulse.actions.CheckoutPageActions;

/**
 * Created by anfal on 2/1/2016.
 */
public class ResetPhoneNumberListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
        CheckoutPageActions.getCheckoutPageActionsInstance().handleResetPhoneNumberClick();
    }
}
