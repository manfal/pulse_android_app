package com.human_pulse.components.listeners;

import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.human_pulse.actions.DepartmentCategoryFragmentActions;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.enumerations.EItemFetchChoice;

import static com.human_pulse.analytics.logging.Constants.ERROR;
import static com.human_pulse.analytics.logging.Constants.TAG_ERROR;

/**
 * Created by anfal on 12/21/2015.
 */
public class DepartmentItemsRecyclerViewScrollListener extends RecyclerView.OnScrollListener {

    private ILog logger = null;
    private DepartmentCategoryFragmentActions departmentCategoryFragmentActions = null;

    public DepartmentItemsRecyclerViewScrollListener(DepartmentCategoryFragmentActions departmentCategoryFragmentActions) {
        this.logger = Logger.getLogger();

        try {
            this.departmentCategoryFragmentActions = departmentCategoryFragmentActions;
        } catch (Exception ex) {
            logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        try {
            if (!recyclerView.canScrollVertically(1) && this.departmentCategoryFragmentActions.isDataFetchNecessary()) {
                this.departmentCategoryFragmentActions.fetchItems(EItemFetchChoice.FETCHING_TO_APPEND);
            }
        } catch (Exception ex) {
            logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}