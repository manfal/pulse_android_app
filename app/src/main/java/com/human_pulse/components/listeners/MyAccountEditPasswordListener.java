package com.human_pulse.components.listeners;

import android.view.View;

import com.human_pulse.actions.PersonalInfoFragmentActions;

/**
 * Created by anfal on 1/2/2016.
 */
public class MyAccountEditPasswordListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
        PersonalInfoFragmentActions.getPersonalInfoActionInstance().handlePasswordEditHyperLinkClick();
    }
}
