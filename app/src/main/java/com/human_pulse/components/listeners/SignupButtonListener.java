package com.human_pulse.components.listeners;

import android.view.View;

import com.human_pulse.actions.SignupActions;

/**
 * Created by anfal on 12/26/2015.
 */
public class SignupButtonListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
        SignupActions.getSignupActionsInstance().signUp();
    }
}
