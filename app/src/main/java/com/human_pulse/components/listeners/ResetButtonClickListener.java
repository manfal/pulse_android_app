package com.human_pulse.components.listeners;

import android.view.View;

import com.human_pulse.actions.ResetPasswordActions;

/**
 * Created by anfal on 12/25/2015.
 */
public class ResetButtonClickListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
        ResetPasswordActions.getResetPasswordActionsInstance().resetPassword();
    }
}
