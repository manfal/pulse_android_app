package com.human_pulse.components.proxies;

import android.view.View;

import com.human_pulse.actions.DepartmentActions;
import com.human_pulse.actions.SearchActions;
import com.human_pulse.components.dataobjects.DepartmentItemDataObject;

/**
 * Created by anfal on 12/31/2015.
 */
public class DepartmentItemClickProxy {

    public void itemClicked(View v, DepartmentItemDataObject departmentItemDataObject) {
        if (null != SearchActions.getSearchActionsInstance()) {
            SearchActions.getSearchActionsInstance().revealItemDetailLayout(departmentItemDataObject, v);
        } else {
            DepartmentActions.getDepartmentActionInstance().revealItemDetailLayout(departmentItemDataObject, v);
        }
    }

}
