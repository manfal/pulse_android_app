package com.human_pulse.components;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.human_pulse.actions.CheckoutPageActions;
import com.human_pulse.actions.PersonalAddressFragmentActionsSender;
import com.human_pulse.actions.PersonalInfoFragmentActions;
import com.human_pulse.app.R;
import com.human_pulse.components.dataobjects.AddressAdditionalDataObject;
import com.human_pulse.components.dataobjects.AddressDataObject;
import com.human_pulse.components.interfaces.IAddressResponseSenderReceiver;
import com.human_pulse.components.interfaces.IPhoneNumberVerificationInterface;
import com.human_pulse.utilities.Utilities;

import static com.human_pulse.constants.HttpRoutes.OPEN_BROWSER_LINK_FOR_APP;
import static com.human_pulse.constants.HttpRoutes.OPEN_GOOGLE_PLAY_LINK_FOR_APP;


/**
 * Created by anfal on 12/8/2015.
 */
public class CustomComponents {
    private static CustomComponents ourInstance = new CustomComponents();

    private CustomComponents() {
    }

    public static CustomComponents getInstance() {
        return ourInstance;
    }

    public AlertDialog.Builder showNewVersionIsAvailableDialog(final Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.attention_text);
        builder.setMessage(R.string.new_version_is_available_text);
        builder.setPositiveButton(R.string.update_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final String appPackageName = context.getPackageName();
                try {
                    Utilities.getInstance().startNewActivityFromIntent(context, new Intent(Intent.ACTION_VIEW, Uri.parse(OPEN_GOOGLE_PLAY_LINK_FOR_APP + appPackageName)));
                } catch (Exception ex) {
                    Utilities.getInstance().startNewActivityFromIntent(context, new Intent(Intent.ACTION_VIEW, Uri.parse(OPEN_BROWSER_LINK_FOR_APP + appPackageName)));
                }
            }
        });
        builder.setNegativeButton(R.string.not_now_text, null);
        builder.setCancelable(false);
        return builder;
    }

    public AlertDialog.Builder messageDialog(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setNegativeButton(context.getString(R.string.dialog_ok_button_text), null);
        return builder;
    }

    public AlertDialog.Builder userPasswordEditDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = View.inflate(context, R.layout.myaccount_edit_password_dialog, null);
        builder.setView(view);
        builder.setNegativeButton(context.getString(R.string.dialog_cancel_button_text), null);
        builder.setPositiveButton(context.getString(R.string.dialog_ok_button_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (null != PersonalInfoFragmentActions.getPersonalInfoActionInstance()) {
                    PersonalInfoFragmentActions.getPersonalInfoActionInstance().handlePasswordEdit();
                }
            }
        });
        return builder;
    }

    public AlertDialog.Builder userPersonalInfoEditDialog(Context context, LinearLayout personalInfoEditLayout, String firstName, String lastName, String phNum) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View personalInfoDialogView = LayoutInflater.from(context).inflate(R.layout.myaccount_edit_personal_info_dialog, personalInfoEditLayout);
        final EditText fName = (EditText) personalInfoDialogView.findViewById(R.id.first_name);
        final EditText lName = (EditText) personalInfoDialogView.findViewById(R.id.last_name);
        final EditText phoneNumber = (EditText) personalInfoDialogView.findViewById(R.id.phone_number);
        fName.setText(firstName);
        lName.setText(lastName);
        phoneNumber.setText(phNum);
        builder.setView(personalInfoDialogView);
        builder.setNegativeButton(context.getString(R.string.dialog_cancel_button_text), null);
        builder.setPositiveButton(context.getString(R.string.dialog_ok_button_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (null != PersonalInfoFragmentActions.getPersonalInfoActionInstance()) {
                    PersonalInfoFragmentActions.getPersonalInfoActionInstance().handlePersonalInfoEdit(
                            fName.getText().toString(),
                            lName.getText().toString(),
                            phoneNumber.getText().toString()
                    );
                }
            }
        });
        return builder;
    }

    public void verifyPhoneNumberDialog(Context context, final IPhoneNumberVerificationInterface phoneNumberVerificationInterface) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = View.inflate(context, R.layout.myaccount_phone_verification_dialog, null);
        final EditText verificationCode = (EditText) view.findViewById(R.id.verification_code);
        builder.setView(view);
        builder.setNegativeButton(context.getString(R.string.dialog_cancel_button_text), null);
        builder.setPositiveButton(context.getString(R.string.dialog_ok_button_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                phoneNumberVerificationInterface.handlePhoneNumberVerification(verificationCode.getText().toString());
            }
        });
        builder.show();
    }

    public void newAddressDialog(Context context, final IAddressResponseSenderReceiver addressResponseSenderReceiver, AddressAdditionalDataObject addressAdditionalDataObject) throws Exception {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View dialogView = View.inflate(context, R.layout.myaccount_address_dialog, null);
        final Spinner label = (Spinner) dialogView.findViewById(R.id.addr_label);
        label.setAdapter(Utilities.getInstance().parseAddressSpinnerData(addressAdditionalDataObject.getLabels(), null, context, false));
        final EditText address = (EditText) dialogView.findViewById(R.id.address_text);
        final Spinner city = (Spinner) dialogView.findViewById(R.id.address_city);
        city.setAdapter(Utilities.getInstance().parseAddressSpinnerData(addressAdditionalDataObject.getCities(), null, context, false));
        builder.setView(dialogView);
        builder.setNegativeButton(context.getString(R.string.dialog_cancel_button_text), null);
        builder.setPositiveButton(context.getString(R.string.dialog_ok_button_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String addressLabel = (String) label.getSelectedItem();
                String addressCity = (String) city.getSelectedItem();
                addressResponseSenderReceiver.sendNewAddressRequest(addressLabel, address.getText().toString(), addressCity);
            }
        });
        builder.show();
    }

    public AlertDialog.Builder deleteAddressConfirmActionDialog(Context context, String message, final int addressId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setNegativeButton(context.getString(R.string.dialog_cancel_button_text), null);
        builder.setPositiveButton(context.getString(R.string.dialog_ok_button_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (null != PersonalInfoFragmentActions.getPersonalInfoActionInstance()) {
                    PersonalAddressFragmentActionsSender.getPersonalAddressFragmentActions().startAddressDeletion(addressId);
                }
            }
        });
        return builder;
    }

    public AlertDialog.Builder editAddressDialog(Context context, ViewGroup viewGroup, final AddressDataObject addressDataObject, AddressAdditionalDataObject addressAdditionalDataObject) throws Exception {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View editDialogLayout = LayoutInflater.from(context).inflate(R.layout.myaccount_address_dialog, viewGroup);
        final Spinner label = (Spinner) editDialogLayout.findViewById(R.id.addr_label);
        label.setAdapter(Utilities.getInstance().parseAddressSpinnerData(addressAdditionalDataObject.getLabels(), addressDataObject.getLabel(), context, true));
        final EditText address = (EditText) editDialogLayout.findViewById(R.id.address_text);
        final Spinner city = (Spinner) editDialogLayout.findViewById(R.id.address_city);
        city.setAdapter(Utilities.getInstance().parseAddressSpinnerData(addressAdditionalDataObject.getCities(), addressDataObject.getCity(), context, true));
        address.setText(addressDataObject.getAddress());
        builder.setView(editDialogLayout);
        builder.setNegativeButton(context.getString(R.string.dialog_cancel_button_text), null);
        builder.setPositiveButton(context.getString(R.string.dialog_ok_button_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String addressLabel = (String) label.getSelectedItem();
                String addressCity = (String) city.getSelectedItem();
                if (null != PersonalInfoFragmentActions.getPersonalInfoActionInstance()) {
                    PersonalAddressFragmentActionsSender.getPersonalAddressFragmentActions().startAddressEdit(
                            addressLabel,
                            address.getText().toString(),
                            addressCity,
                            addressDataObject.getId()
                    );
                }
            }
        });
        return builder;
    }

    public void enterPhoneNumberDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = View.inflate(context, R.layout.myaccount_phone_verification_dialog, null);
        view.bringToFront();
        TextView textView = (TextView) view.findViewById(R.id.phone_verification_dialog_text);
        final EditText editText = (EditText) view.findViewById(R.id.verification_code);
        editText.setInputType(InputType.TYPE_CLASS_PHONE);
        TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.verification_code_inputlayout);
        textInputLayout.setHint(context.getString(R.string.phone_number_text));
        textView.setText(context.getString(R.string.enter_phone_number_text));
        builder.setView(view);
        builder.setNegativeButton(context.getString(R.string.dialog_cancel_button_text), null);
        builder.setPositiveButton(context.getString(R.string.dialog_ok_button_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (null != CheckoutPageActions.getCheckoutPageActionsInstance()) {
                    CheckoutPageActions.getCheckoutPageActionsInstance().sendNewPhoneNumberRequest(editText.getText().toString());
                }
            }
        });
        builder.show();
    }

    public void showCompanyClosedDialog(Context context, String openingTime, String closingTime) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = View.inflate(context, R.layout.company_closed_dialog, null);
        view.bringToFront();
        TextView ourTimingView = (TextView) view.findViewById(R.id.our_timing_textview);
        String ourTimingText = context.getString(
                R.string.our_timing_text,
                openingTime,
                closingTime
        );
        ourTimingView.setText(ourTimingText);
        builder.setView(view);
        builder.setPositiveButton(context.getString(R.string.navdrawer_close), null);
        builder.show();
    }
}
