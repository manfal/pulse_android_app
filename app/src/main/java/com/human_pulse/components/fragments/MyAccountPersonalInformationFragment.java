package com.human_pulse.components.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.human_pulse.actions.PersonalInfoFragmentActions;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.app.R;

import static com.human_pulse.analytics.logging.Constants.ERROR;
import static com.human_pulse.analytics.logging.Constants.TAG_ERROR;

/**
 * Created by anfal on 1/2/2016.
 */
public class MyAccountPersonalInformationFragment extends Fragment {

    private ILog logger = null;

    public MyAccountPersonalInformationFragment() {
        this.logger = Logger.getLogger();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.myaccount_person_info_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            PersonalInfoFragmentActions.setPersonalInfoActionInstance(view);
            PersonalInfoFragmentActions.getPersonalInfoActionInstance().startUserAccountInfoRetrieval();
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PersonalInfoFragmentActions.getPersonalInfoActionInstance().destroyPersonalInfoActionInstance();
    }
}
