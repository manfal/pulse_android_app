package com.human_pulse.components.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.human_pulse.actions.DepartmentCategoryFragmentActions;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.app.R;
import com.human_pulse.enumerations.EItemFetchChoice;

import static com.human_pulse.analytics.logging.Constants.ERROR;
import static com.human_pulse.analytics.logging.Constants.TAG_ERROR;

/**
 * Created by anfal on 12/27/2015.
 */
public class DepartmentCategoryFragment extends Fragment {

    private int categoryId, departmentId;
    private ILog logger = null;
    private DepartmentCategoryFragmentActions departmentCategoryFragmentActions = null;

    public DepartmentCategoryFragment() {
        this.logger = Logger.getLogger();
    }

    public void setCategoryId(int categoryId) {
        try {
            this.categoryId = categoryId;
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void setDepartmentId(int departmentId) {
        try {
            this.departmentId = departmentId;
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.department_category_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            this.departmentCategoryFragmentActions = new DepartmentCategoryFragmentActions(view, this.departmentId, this.categoryId);
            this.departmentCategoryFragmentActions.fetchItems(EItemFetchChoice.FETCHING_FIRST_TIME);
        } catch (Exception ex) {
            logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.departmentCategoryFragmentActions = null;
    }
}
