package com.human_pulse.components.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.human_pulse.app.R;
import com.human_pulse.components.dataobjects.OrderHistoryDataObject;
import com.human_pulse.components.viewholder.OrderHistoryItemHolder;

import java.util.List;

/**
 * Created by anfal on 1/8/2016.
 */
public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryItemHolder> {

    List<OrderHistoryDataObject> orderHistoryDataObjects = null;

    public OrderHistoryAdapter(List<OrderHistoryDataObject> orderHistoryDataObjects) {
        this.orderHistoryDataObjects = orderHistoryDataObjects;
    }

    @Override
    public OrderHistoryItemHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.order_history_item, viewGroup, false);
        return new OrderHistoryItemHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderHistoryItemHolder orderHistoryItemHolder, int position) {
        orderHistoryItemHolder.getOrderNumber().setText(
                orderHistoryItemHolder.getOrderNumber().getContext().getString(
                        R.string.order_no_text_template,
                        this.orderHistoryDataObjects.get(position).getInvoiceName(),
                        this.orderHistoryDataObjects.get(position).getDeliveryStatusLabel()
                )
        );
        orderHistoryItemHolder.getOrderDate().setText(
                orderHistoryItemHolder.getOrderDate().getContext().getString(
                        R.string.order_date_text_template,
                        this.orderHistoryDataObjects.get(position).getOrderDate()
                )
        );
        orderHistoryItemHolder.getOrderItems().setText(
                orderHistoryItemHolder.getOrderItems().getContext().getString(
                        R.string.order_items_text_tamplate,
                        this.orderHistoryDataObjects.get(position).getTotalItems()
                )
        );
        orderHistoryItemHolder.getOrderCost().setText(
                orderHistoryItemHolder.getOrderCost().getContext().getString(
                        R.string.order_cost_text_template,
                        this.orderHistoryDataObjects.get(position).getTotalPrice()
                )
        );
        orderHistoryItemHolder.setOrderHistoryDataObject(this.orderHistoryDataObjects.get(position));
    }

    @Override
    public int getItemCount() {
        return this.orderHistoryDataObjects.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
