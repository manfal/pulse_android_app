package com.human_pulse.components.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.human_pulse.app.R;
import com.human_pulse.components.dataobjects.StoreDepartmentDataObject;
import com.human_pulse.components.viewholder.StoreDepartmentHolder;

import java.util.List;

/**
 * Created by anfal on 12/17/2015.
 */
public class StoreDepartmentsAdapter extends RecyclerView.Adapter<StoreDepartmentHolder> {
    private List<StoreDepartmentDataObject> storeDepartmentDataObjects = null;

    public StoreDepartmentsAdapter(List<StoreDepartmentDataObject> storeDepartmentDataObjects) {
        this.storeDepartmentDataObjects = storeDepartmentDataObjects;
    }

    @Override
    public StoreDepartmentHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.department_card, viewGroup, false);
        return new StoreDepartmentHolder(view);
    }

    @Override
    public void onBindViewHolder(StoreDepartmentHolder storeDepartmentHolder, int position) {
        storeDepartmentHolder.getTextView().setText(this.storeDepartmentDataObjects.get(position).getDepartmentName());
        storeDepartmentHolder.setDepartmentPosition(position);
    }

    @Override
    public int getItemCount() {
        return this.storeDepartmentDataObjects.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public List<StoreDepartmentDataObject> getStoreItemList() {
        return this.storeDepartmentDataObjects;
    }
}
