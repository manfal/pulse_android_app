package com.human_pulse.components.navigation;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.human_pulse.app.R;
import com.human_pulse.components.adapter.DrawerAdapter;
import com.human_pulse.components.adapter.ExpandableRecyclerAdapter;
import com.human_pulse.components.dataobjects.ParentDataObjectListener;
import com.human_pulse.components.observers.LeftNavigationObserver;

import java.util.List;

/**
 * Created by anfal on 12/6/2015.
 */
public class LeftMenu implements ExpandableRecyclerAdapter.ExpandCollapseListener {
    private RecyclerView recyclerView = null;
    private AppCompatActivity appCompatActivity = null;
    private DrawerAdapter mExpandableAdapter = null;
    private int navFirstItemExpandPosition, currentNavItemPosition = 0;
    private boolean isFirstItemExpandPositionSet = false;
    private LinearLayoutManager linearLayoutManager = null;

    public LeftMenu(RecyclerView recyclerView, AppCompatActivity appCompatActivity) {
        this.recyclerView = recyclerView;
        this.appCompatActivity = appCompatActivity;
    }

    public LeftMenu instantiateMenu(List<ParentDataObjectListener> dataSet) {
        this.mExpandableAdapter = new DrawerAdapter(this.appCompatActivity, dataSet);
        this.mExpandableAdapter.setExpandCollapseListener(this);
        this.linearLayoutManager = new LinearLayoutManager(this.appCompatActivity);
        this.recyclerView.setLayoutManager(this.linearLayoutManager);
        this.recyclerView.setAdapter(this.mExpandableAdapter);
        return this;
    }

    public LeftMenu bindDrawerToggle(DrawerLayout drawerLayout, Toolbar toolbar) {
        final ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this.appCompatActivity, drawerLayout, toolbar, R.string.navdrawer_open, R.string.navdrawer_close);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                actionBarDrawerToggle.syncState();
            }
        });
        return this;
    }

    @Override
    public void onListItemExpanded(int position) {
        ParentDataObjectListener parentDataObject = (ParentDataObjectListener) this.getExpandableAdapter().getParentItemList().get(position);
        if (0 <= parentDataObject.getParentId()) {
            if (!this.isFirstItemExpandPositionSet) {
                this.navFirstItemExpandPosition = position;
                this.isFirstItemExpandPositionSet = true;
            }
            if (position != this.navFirstItemExpandPosition) {
                this.mExpandableAdapter.collapseParent(this.navFirstItemExpandPosition);
                this.navFirstItemExpandPosition = position;
            }
            this.currentNavItemPosition = position;
        } else {
            LeftNavigationObserver.getLeftNavigationObserverInstance().navigationItemDefaultMenuListener(parentDataObject.getParentId(), parentDataObject.getParentText());
        }
    }

    @Override
    public void onListItemCollapsed(int position) {
    }

    public int getCurrentNavItemPosition() {
        return currentNavItemPosition;
    }

    public DrawerAdapter getExpandableAdapter() {
        return this.mExpandableAdapter;
    }
}
