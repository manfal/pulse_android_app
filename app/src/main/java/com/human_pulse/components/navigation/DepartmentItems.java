package com.human_pulse.components.navigation;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.human_pulse.app.R;
import com.human_pulse.components.adapter.DepartmentItemAdapter;
import com.human_pulse.components.dataobjects.DepartmentItemDataObject;

import java.util.List;

/**
 * Created by anfal on 12/20/2015.
 */
public class DepartmentItems {
    private List<DepartmentItemDataObject> departmentItemDataObjects = null;
    private RecyclerView recyclerView = null;
    private Context context = null;
    private DepartmentItemAdapter departmentItemAdapter = null;

    public DepartmentItems(RecyclerView recyclerView, Context context) {
        this.context = context;
        this.recyclerView = recyclerView;
    }

    public boolean instantiateDepartmentItems(List<DepartmentItemDataObject> departmentItemDataObjects) throws Exception {
        int layoutManagerSpanCount = this.context.getResources().getInteger(R.integer.layout_manager_span_count);
        this.departmentItemDataObjects = departmentItemDataObjects;
        this.departmentItemAdapter = new DepartmentItemAdapter(this.departmentItemDataObjects);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this.context, layoutManagerSpanCount);
        this.recyclerView.setLayoutManager(gridLayoutManager);
        this.recyclerView.setAdapter(departmentItemAdapter);
        Animation animation = AnimationUtils.loadAnimation(this.context, R.anim.store_department_animation);
        this.recyclerView.startAnimation(animation);
        return true;
    }

    public RecyclerView getRecyclerView() {
        return this.recyclerView;
    }

    public List<DepartmentItemDataObject> getDepartmentItemDataObjects() {
        return this.departmentItemDataObjects;
    }

    public DepartmentItemAdapter getDepartmentItemAdapter() {
        return this.departmentItemAdapter;
    }
}
