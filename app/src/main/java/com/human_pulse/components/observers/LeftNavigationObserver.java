package com.human_pulse.components.observers;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.human_pulse.actions.CustomerHelpActions;
import com.human_pulse.actions.DepartmentActions;
import com.human_pulse.analytics.LogEvents;
import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.app.CheckoutPage;
import com.human_pulse.app.CustomerHelpPage;
import com.human_pulse.app.DepartmentPage;
import com.human_pulse.app.LoginPage;
import com.human_pulse.app.MyAccountPage;
import com.human_pulse.app.OrderHistoryPage;
import com.human_pulse.app.R;
import com.human_pulse.app.StorePage;
import com.human_pulse.components.adapter.DrawerAdapter;
import com.human_pulse.components.dataobjects.ChildDataObject;
import com.human_pulse.components.dataobjects.ParentDataObjectListener;
import com.human_pulse.components.listeners.ToolBarNavigationClickListener;
import com.human_pulse.components.navigation.LeftMenu;
import com.human_pulse.database.ClientDB;
import com.human_pulse.datastore.LeftNavigationObserverController;
import com.human_pulse.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.human_pulse.constants.Constants.CATEGORY_ID;
import static com.human_pulse.constants.Constants.CATEGORY_NAME;
import static com.human_pulse.constants.Constants.DEPARTMENT_CATEGORIES;
import static com.human_pulse.constants.Constants.DEPARTMENT_IDENTIFIER;
import static com.human_pulse.constants.Constants.DEPARTMENT_NAME;
import static com.human_pulse.constants.Constants.pageName;

/**
 * Created by anfal on 1/15/2016.
 */
public class LeftNavigationObserver {

    private static LeftNavigationObserver leftNavigationObserver = null;
    private ILog logger = null;
    private LeftMenu leftMenu = null;
    private AppCompatActivity appCompatActivity = null;
    private DrawerLayout drawerLayout = null;
    private LeftNavigationObserverController leftNavigationObserverController = null;
    private ClientDB clientDB = null;

    public LeftNavigationObserver(AppCompatActivity appCompatActivity, DrawerLayout drawerLayout, Toolbar toolbar) {
        this.logger = Logger.getLogger();
        try {
            this.appCompatActivity = appCompatActivity;
            this.clientDB = new ClientDB(this.appCompatActivity);
            RecyclerView leftNavRecyclerView = (RecyclerView) this.appCompatActivity.findViewById(R.id.left_menu);
            this.drawerLayout = drawerLayout;
            this.leftMenu = new LeftMenu(leftNavRecyclerView, this.appCompatActivity).bindDrawerToggle(this.drawerLayout, toolbar);
            this.leftNavigationObserverController = new LeftNavigationObserverController(this.appCompatActivity);
            this.fetchLeftNavigation();
            toolbar.setNavigationOnClickListener(new ToolBarNavigationClickListener(this.drawerLayout));
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public static void setLeftNavigationObserverInstance(AppCompatActivity appCompatActivity, DrawerLayout drawerLayout, Toolbar toolbar) {
        leftNavigationObserver = new LeftNavigationObserver(appCompatActivity, drawerLayout, toolbar);
    }

    public static LeftNavigationObserver getLeftNavigationObserverInstance() {
        return leftNavigationObserver;
    }

    public void populateLeftMenu(JSONArray itemsArray) {
        try {
            this.leftMenu.instantiateMenu(this.parseLeftNav(itemsArray));
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void navigationItemClickListener(int position) {
        try {
            DrawerAdapter drawerAdapter = this.leftMenu.getExpandableAdapter();
            int currentNavigationPosition = this.leftMenu.getCurrentNavItemPosition();
            int categoryPosition = (position - currentNavigationPosition) - 1;
            ParentDataObjectListener parentDataObject = (ParentDataObjectListener) drawerAdapter.getParentItemList().get(currentNavigationPosition);
            ChildDataObject childDataObject = (ChildDataObject) drawerAdapter.getParentItemList().get(currentNavigationPosition).getChildItemList().get(categoryPosition);
            FlurryAgent.logEvent(
                    LogEvents.EVENT_ID_MAIN_STORE_DEPARTMENT,
                    LogEvents.getEventData(
                            LogEvents.EVENT_ID_MAIN_STORE_DEPARTMENT,
                            new String[]{
                                    LogEvents.TRIGGERED_LEFT_NAV_CATEGORY_ITEM_EVENT,
                                    parentDataObject.getParentText(),
                                    childDataObject.getChildText()
                            }
                    )
            );
            Utilities.getInstance().resetStaticActivityData();
            if (this.appCompatActivity instanceof DepartmentPage) {
                if (parentDataObject.getParentId() == DepartmentActions.getDepartmentActionInstance().getDepartmentId()) {
                    DepartmentActions.getDepartmentActionInstance().selectCurrentlyActiveTab(childDataObject.getChildText());
                } else {
                    DepartmentActions.getDepartmentActionInstance().fetchCategoryData(
                            parentDataObject.getParentText(),
                            parentDataObject.getParentId(),
                            childDataObject.getChildId()
                    );
                }
            } else {
                Utilities.getInstance().startDepartmentActivity(this.appCompatActivity, parentDataObject.getParentText(), parentDataObject.getParentId(), childDataObject.getChildId());
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void navigationItemDefaultMenuListener(int position, String navigationText) {
        try {
            if (navigationText.equals(this.appCompatActivity.getString(R.string.text_store))) {
                if (this.appCompatActivity instanceof StorePage) {
                    if (this.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                        this.drawerLayout.closeDrawer(Gravity.LEFT);
                    }
                } else {
                    Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, StorePage.class);
                }
            } else if (navigationText.equals(this.appCompatActivity.getString(R.string.text_myaccount))) {
                if (this.appCompatActivity instanceof MyAccountPage) {
                    if (this.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                        this.drawerLayout.closeDrawer(Gravity.LEFT);
                    }
                } else {
                    Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, MyAccountPage.class);
                }
            } else if (navigationText.equals(this.appCompatActivity.getString(R.string.text_orderhistory))) {
                if (this.appCompatActivity instanceof OrderHistoryPage) {
                    if (this.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                        this.drawerLayout.closeDrawer(Gravity.LEFT);
                    }
                } else {
                    Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, OrderHistoryPage.class);
                }
            } else if (
                    navigationText.equals(this.appCompatActivity.getString(R.string.text_help)) ||
                            navigationText.equals(this.appCompatActivity.getString(R.string.text_locations)) ||
                            navigationText.equals(this.appCompatActivity.getString(R.string.text_tnc)) ||
                            navigationText.equals(this.appCompatActivity.getString(R.string.text_contact)) ||
                            navigationText.equals(this.appCompatActivity.getString(R.string.text_privacy_policy))
                    ) {
                pageName = navigationText;
                FlurryAgent.logEvent(
                        LogEvents.EVENT_ID_MAIN_CUSTOMER_HELP,
                        LogEvents.getEventData(
                                LogEvents.EVENT_ID_MAIN_CUSTOMER_HELP,
                                new String[]{
                                        LogEvents.TRIGGERED_CUSTOMER_HELP_EVENT,
                                        String.valueOf(position)
                                }
                        )
                );
                if (this.appCompatActivity instanceof CustomerHelpPage) {
                    if (this.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                        CustomerHelpActions.getCustomerHelpActions().loadNewWebPage(navigationText);
                        this.drawerLayout.closeDrawer(Gravity.LEFT);
                    }
                } else {
                    Utilities.getInstance().startCustomerHelpActivity(this.appCompatActivity, navigationText);
                }
            } else if (navigationText.equals(this.appCompatActivity.getString(R.string.text_logout))) {
                //Updating the value for email will make sure that email delivery charges info is not deleted
                //Because user can use cart mechanism even after logout.
                this.clientDB.updateUserEmailToDefaultForLogout();
                Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, LoginPage.class);
            } else if (navigationText.equals(this.appCompatActivity.getString(R.string.text_login))) {
                Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, LoginPage.class);
            } else if (navigationText.equals(this.appCompatActivity.getString(R.string.place_custom_order_text))) {
                Utilities.getInstance().initiateCustomCheckout(this.appCompatActivity);
            } else {
                Toast.makeText(this.appCompatActivity, R.string.could_not_recognize_menu_item_text, Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private List<ParentDataObjectListener> parseLeftNav(JSONArray departmentArray) throws JSONException {
        //Just because I had no other way to distinguish between custom item array and string
        //I am giving custom item will start from -100 and array will ids starting from -1
        List<ParentDataObjectListener> departments = new ArrayList<>();
        String[] defaultItemArray = Utilities.getInstance().getDefaultMenuArray(this.appCompatActivity);
        int departmentSize = departmentArray.length();

        if (!(this.appCompatActivity instanceof CheckoutPage)) {
            ParentDataObjectListener placeCustomOrderNavItem = new ParentDataObjectListener();
            placeCustomOrderNavItem.setParentNumber(departmentSize++);
            placeCustomOrderNavItem.setParentId(-100);
            placeCustomOrderNavItem.setParentText(this.appCompatActivity.getString(R.string.place_custom_order_text));
            departments.add(placeCustomOrderNavItem);
        }

        //These are departments and categories received from server.
        for (int departmentNumber = 0; departmentNumber < departmentArray.length(); departmentNumber++) {
            JSONObject departmentObject = departmentArray.getJSONObject(departmentNumber);
            List<ChildDataObject> categories = new ArrayList<>();
            JSONArray categoryArray = departmentObject.getJSONArray(DEPARTMENT_CATEGORIES);
            for (int categoryNumber = 0; categoryNumber < categoryArray.length(); categoryNumber++) {
                ChildDataObject category = new ChildDataObject();
                JSONObject categoryObject = categoryArray.getJSONObject(categoryNumber);
                category.setChildText(categoryObject.getString(CATEGORY_NAME));
                category.setChildId(Integer.parseInt(categoryObject.getString(CATEGORY_ID)));
                categories.add(category);
            }
            ParentDataObjectListener department = new ParentDataObjectListener();
            department.setChildItemList(categories);
            department.setParentNumber(departmentNumber);
            department.setParentId(Integer.parseInt(departmentObject.getString(DEPARTMENT_IDENTIFIER)));
            department.setParentText(departmentObject.getString(DEPARTMENT_NAME));
            if (departmentNumber == 0) {
                department.setInitiallyExpanded(false);
            }
            departments.add(department);
        }

        //This is default menu array portion.
        for (int defaultItemNumber = 0; defaultItemNumber < defaultItemArray.length; defaultItemNumber++) {
            ParentDataObjectListener defaultNavItem = new ParentDataObjectListener();
            defaultNavItem.setParentNumber(departmentSize++);
            defaultNavItem.setParentId((defaultItemNumber + 1) * -1);
            defaultNavItem.setParentText(defaultItemArray[defaultItemNumber]);
            departments.add(defaultNavItem);
        }

        return departments;
    }

    public void fetchLeftNavigation() {
        try {
            this.leftNavigationObserverController.getLeftMenu(this);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
