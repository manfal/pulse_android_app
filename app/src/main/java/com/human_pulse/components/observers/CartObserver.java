package com.human_pulse.components.observers;

import android.content.Context;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.app.DepartmentPage;
import com.human_pulse.app.R;
import com.human_pulse.app.SearchPage;
import com.human_pulse.components.dataobjects.CartItemDataObject;
import com.human_pulse.components.dataobjects.DeliveryChargesInfoDataObject;
import com.human_pulse.components.dataobjects.DepartmentItemDataObject;
import com.human_pulse.components.listeners.CheckoutButtonClickListener;
import com.human_pulse.components.navigation.Cart;
import com.human_pulse.components.viewholder.CheckoutItemHolder;
import com.human_pulse.components.viewholder.DepartmentItemHolder;
import com.human_pulse.database.ClientDB;
import com.human_pulse.enumerations.EItemCheckout;
import com.human_pulse.utilities.ItemCheckoutUtility;
import com.human_pulse.utilities.Utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.human_pulse.constants.Constants.ERROR;

/**
 * Created by anfal on 1/15/2016.
 */

public class CartObserver {

    private static CartObserver cartObserverInstance = null;
    private final int NO_ITEM_IN_CART = -1;
    private final int DISPLAY_CART = 1;
    private ILog logger = null;
    private RecyclerView cartListItemRecyclerView = null;
    private AppCompatActivity appCompatActivity = null;
    private TextView cartSubTotalText, deliveryChargesApplyText = null;
    private RelativeLayout emptyCartLayout = null;
    private RelativeLayout fullCartLayout = null;
    private Cart cart = null;
    private int totalCheckoutPrice = 0;
    private String[] itemQuantityArray = null;
    private DeliveryChargesInfoDataObject deliveryChargesInfoDataObject = null;
    private Map<Integer, CartItemDataObject> cartMap = null;
    private List<DepartmentItemHolder> departmentItemHolderList = null;
    private ClientDB clientDB = null;

    private CartObserver(AppCompatActivity appCompatActivity) {
        this.logger = Logger.getLogger();
        try {
            this.appCompatActivity = appCompatActivity;
            this.cartListItemRecyclerView = (RecyclerView) this.appCompatActivity.findViewById(R.id.cart_item_list_recycler_view);
            this.cartSubTotalText = (TextView) this.appCompatActivity.findViewById(R.id.cart_subtotal_text);
            this.deliveryChargesApplyText = (TextView) this.appCompatActivity.findViewById(R.id.delivery_charges_apply_text);
            this.emptyCartLayout = (RelativeLayout) this.appCompatActivity.findViewById(R.id.cart_empty_layout);
            this.fullCartLayout = (RelativeLayout) this.appCompatActivity.findViewById(R.id.full_cart_layout);
            Button checkoutButton = (Button) this.appCompatActivity.findViewById(R.id.cart_checkout_button);
            checkoutButton.setOnClickListener(new CheckoutButtonClickListener(this.appCompatActivity));
            this.clientDB = new ClientDB(this.appCompatActivity);
            this.itemQuantityArray = this.appCompatActivity.getResources().getStringArray(R.array.item_checkout_quantities);
            this.deliveryChargesInfoDataObject = this.clientDB.getDeliveryChargesInfo();
            if (this.appCompatActivity instanceof DepartmentPage || this.appCompatActivity instanceof SearchPage) {
                this.departmentItemHolderList = new ArrayList<>();
            }
            this.cartMap = new HashMap<>();
            this.instantiateCart();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public static CartObserver getCartObserverInstance() {
        return cartObserverInstance;
    }

    public static void setCartObserverInstance(AppCompatActivity appCompatActivity) {
        cartObserverInstance = new CartObserver(appCompatActivity);
    }

    public void instantiateCart() {
        try {
            if (!Utilities.getInstance().isInternetConnectionNotAvailable(this.appCompatActivity)) {
                List<CartItemDataObject> cartItemDataObjects = this.clientDB.getOrderHistoryItemsFromDB();
                this.cart = new Cart(this.cartListItemRecyclerView, this.appCompatActivity);
                this.cart.populateCart(cartItemDataObjects);
                if (ERROR < cartItemDataObjects.size()) {
                    this.totalCheckoutPrice = ItemCheckoutUtility.getInstance().getCheckoutPriceWithoutDeliveryCharges(cartItemDataObjects);
                    this.setTotalPriceInCart();
                    this.displayCart(DISPLAY_CART);
                    this.addToCartMap(cartItemDataObjects);
                }
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private void setTotalPriceInCart() throws Exception {
        this.cartSubTotalText.setText(
                this.cartSubTotalText.getContext().getString(
                        R.string.cart_sub_total_text_template,
                        this.totalCheckoutPrice
                )
        );
        this.updateDeliveryChargesApplyText();
    }

    private void displayCart(int flag) throws Exception {
        if (DISPLAY_CART == flag) {
            this.fullCartLayout.setVisibility(View.VISIBLE);
            this.emptyCartLayout.setVisibility(View.GONE);
        } else {
            this.fullCartLayout.setVisibility(View.GONE);
            this.emptyCartLayout.setVisibility(View.VISIBLE);
        }
    }

    private boolean isCartNotEmpty() throws Exception {
        return ERROR < this.cart.getCartItemDataObjects().size();
    }

    private int getExistingItemPositionIncart(List<CartItemDataObject> cartItemDataObjectList, int itemIdToFind) {
        int position = this.NO_ITEM_IN_CART;
        try {
            if (this.isCartNotEmpty()) {
                for (int i = 0; i < cartItemDataObjectList.size(); i++) {
                    if (cartItemDataObjectList.get(i).getItemId() == itemIdToFind) {
                        position = i;
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
        return position;
    }

    private int getItemInsertPosition() throws Exception {
        int position = 0;
        if (position < this.cart.getCartItemDataObjects().size()) {
            position = this.cart.getCartItemDataObjects().size() + 1;
        }
        return position;
    }

    private void addNewItemInCart(DepartmentItemDataObject departmentItemDataObject, int itemQuantity) {
        try {
            CartItemDataObject cartItemDataObject = new CartItemDataObject();
            cartItemDataObject.setItemPrice(departmentItemDataObject.getItemPrice());
            cartItemDataObject.setItemWeightOrQuantity(departmentItemDataObject.getItemWeightOrQuantity());
            cartItemDataObject.setQuantityToCheckout(itemQuantity);
            cartItemDataObject.setItemId(departmentItemDataObject.getItemId());
            cartItemDataObject.setItemDiscount(departmentItemDataObject.getItemDiscount());
            cartItemDataObject.setItemName(departmentItemDataObject.getItemName());
            this.cart.getCartItemDataObjects().add(cartItemDataObject);
            this.cart.getCartItemAdapter().notifyItemInserted(this.getItemInsertPosition());
            this.totalCheckoutPrice += ItemCheckoutUtility.getInstance().getItemPriceBasedOnQuantity(departmentItemDataObject.getItemPrice(), itemQuantity, departmentItemDataObject.getItemDiscount());
            this.setTotalPriceInCart();
            this.addToCartMap(cartItemDataObject);
            this.setUpdatedDepartmentItemQuantity(departmentItemDataObject.getItemId());
            this.clientDB.insertOrderHistoryRow(cartItemDataObject);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private void updateExistingItemQuantityInCart(int position, int itemQuantity) {
        try {
            CartItemDataObject cartItemDataObject = this.cart.getCartItemDataObjects().get(position);
            this.updateCartSubtotalPrice(cartItemDataObject, itemQuantity);
            cartItemDataObject.setQuantityToCheckout(itemQuantity);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void addItemToCart(Object item, DepartmentItemDataObject departmentItemDataObject) {
        try {
            String defaultCheckoutItem = this.itemQuantityArray[0];
            String checkoutItemQuantity = (String) item;
            if (!checkoutItemQuantity.equals(defaultCheckoutItem)) {
                int itemPositionInCart = this.getExistingItemPositionIncart(this.cart.getCartItemDataObjects(), departmentItemDataObject.getItemId());
                if (this.NO_ITEM_IN_CART == itemPositionInCart) {
                    this.addNewItemInCart(departmentItemDataObject, Integer.parseInt(checkoutItemQuantity));
                    this.displayCart(DISPLAY_CART);
                } else {
                    this.updateExistingItemQuantityInCart(itemPositionInCart, Integer.parseInt(checkoutItemQuantity));
                }
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void changeItemQuantityInCart(CartItemDataObject cartItemDataObject, EItemCheckout eItemCheckout) {
        try {
            if (Utilities.getInstance().isInternetConnectionNotAvailable(this.appCompatActivity)) {
                Toast.makeText(this.appCompatActivity, this.appCompatActivity.getString(R.string.need_internet_to_change_item_quantity_text), Toast.LENGTH_LONG).show();
            } else {
                int newItemQuantityToCheckout = 0;
                int itemQuantityArrayLength = this.itemQuantityArray.length - 1;

                switch (eItemCheckout) {
                    case INCREASE:
                        newItemQuantityToCheckout = cartItemDataObject.getQuantityToCheckout() + 1;
                        break;
                    case DECREASE:
                        newItemQuantityToCheckout = cartItemDataObject.getQuantityToCheckout() - 1;
                        break;
                }

                if (itemQuantityArrayLength >= newItemQuantityToCheckout && ERROR < newItemQuantityToCheckout) {
                    this.updateCartSubtotalPrice(cartItemDataObject, newItemQuantityToCheckout);
                    cartItemDataObject.setQuantityToCheckout(newItemQuantityToCheckout);
                }
                if (ERROR == newItemQuantityToCheckout) {
                    this.deleteCartItem(cartItemDataObject);
                }
                if ((itemQuantityArrayLength + 1) == newItemQuantityToCheckout) {
                    Toast.makeText(
                            this.appCompatActivity,
                            this.appCompatActivity.getString(
                                    R.string.no_more_than_items_template,
                                    itemQuantityArrayLength
                            ),
                            Toast.LENGTH_SHORT
                    ).show();
                    Vibrator denialVibrator = (Vibrator) this.appCompatActivity.getSystemService(Context.VIBRATOR_SERVICE);
                    denialVibrator.vibrate(100);
                }
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private void updateCartSubtotalPrice(CartItemDataObject cartItemDataObject, int itemQuantity) throws Exception {
        int oldItemPrice = ItemCheckoutUtility.getInstance().getItemPriceBasedOnQuantity(
                cartItemDataObject.getItemPrice(),
                cartItemDataObject.getQuantityToCheckout(),
                cartItemDataObject.getItemDiscount()
        );
        int newPrice = ItemCheckoutUtility.getInstance().getItemPriceBasedOnQuantity(
                cartItemDataObject.getItemPrice(),
                itemQuantity,
                cartItemDataObject.getItemDiscount()
        );
        this.cart.getCartItemAdapter()
                .getCheckoutItemHolder(this.getCartItemHolderPosition(cartItemDataObject.getItemId()))
                .getItemQuantityText()
                .setText(Integer.toString(itemQuantity));
        this.totalCheckoutPrice -= oldItemPrice;
        this.totalCheckoutPrice += newPrice;
        this.setTotalPriceInCart();
        this.setQuantityToCheckoutInCartMap(cartItemDataObject.getItemId(), itemQuantity);
        this.setUpdatedDepartmentItemQuantity(cartItemDataObject.getItemId());
        this.clientDB.updateOrderHistoryRow(cartItemDataObject.getItemId(), cartItemDataObject.getQuantityToCheckout());
    }

    public void deleteCartItem(CartItemDataObject cartItemDataObject) {
        try {
            if (Utilities.getInstance().isInternetConnectionNotAvailable(this.appCompatActivity)) {
                Toast.makeText(this.appCompatActivity, this.appCompatActivity.getString(R.string.need_internet_to_delete_item_quantity_text), Toast.LENGTH_LONG).show();
            } else {
                int itemRemovePosition = this.getCartItemPosition(cartItemDataObject);
                this.clientDB.deleteOrderHistoryRecord(cartItemDataObject.getItemId());
                this.cartMap.remove(cartItemDataObject.getItemId());
                this.updateCartSubtotalPrice(cartItemDataObject, 0);
                this.cart.getCartItemDataObjects().remove(itemRemovePosition);
                this.cart.getCartItemAdapter().removeCheckoutItemHolder(itemRemovePosition);
                this.cart.getCartItemAdapter().notifyItemRemoved(itemRemovePosition);
                if (ERROR == this.cart.getCartItemDataObjects().size()) {
                    this.displayCart(ERROR);
                }
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private int getCartItemPosition(CartItemDataObject cartItemDataObject) throws Exception {
        int position = 0;
        for (int i = 0; i < this.cart.getCartItemDataObjects().size(); i++) {
            if (cartItemDataObject.getItemId() == this.cart.getCartItemDataObjects().get(i).getItemId()) {
                position = i;
                break;
            }
        }
        return position;
    }

    private void updateDeliveryChargesApplyText() throws Exception {
        if (this.deliveryChargesInfoDataObject.getThresholdPrice() > this.totalCheckoutPrice) {
            this.deliveryChargesApplyText.setText(
                    this.appCompatActivity.getString(
                            R.string.delivery_charges_apply_template,
                            this.deliveryChargesInfoDataObject.getChargedPrice(),
                            (this.deliveryChargesInfoDataObject.getThresholdPrice() - this.totalCheckoutPrice)
                    )
            );
            this.deliveryChargesApplyText.setBackgroundResource(R.color.red_200);
        } else {
            this.deliveryChargesApplyText.setText(this.appCompatActivity.getString(R.string.free_delivery_text));
            this.deliveryChargesApplyText.setBackgroundResource(R.color.green_200);
        }
    }

    private void addToCartMap(CartItemDataObject cartItemDataObject) throws Exception {
        if (null == this.cartMap.get(cartItemDataObject.getItemId())) {
            this.cartMap.put(cartItemDataObject.getItemId(), cartItemDataObject);
        }
    }

    private void addToCartMap(List<CartItemDataObject> cartItemDataObjects) throws Exception {
        for (CartItemDataObject cartItemDataObject : cartItemDataObjects) {
            this.addToCartMap(cartItemDataObject);
        }
    }

    public void addToDepartmentItemList(DepartmentItemHolder departmentItemHolder) {
        if (this.appCompatActivity instanceof DepartmentPage || this.appCompatActivity instanceof SearchPage) {
            this.departmentItemHolderList.add(departmentItemHolder);
        }
    }

    public int getQuantityToCheckoutFromMap(int itemId) {
        int quantity = ERROR;
        if (null != this.cartMap.get(itemId)) {
            quantity = this.cartMap.get(itemId).getQuantityToCheckout();
        }
        return quantity;
    }

    private void setUpdatedDepartmentItemQuantity(int itemId) throws Exception {
        if (this.appCompatActivity instanceof DepartmentPage || this.appCompatActivity instanceof SearchPage) {
            int itemQuantity = this.getQuantityToCheckoutFromMap(itemId);
            for (DepartmentItemHolder departmentItemHolder : this.departmentItemHolderList) {
                if (departmentItemHolder.getDepartmentItemDataObject().getItemId() == itemId) {
                    departmentItemHolder.getDepartmentItemDataObject().setOrderedQuantity(itemQuantity);
                    departmentItemHolder.getAddToCartSpinner().setSelection(
                            departmentItemHolder.getDepartmentItemDataObject().getOrderedQuantity(),
                            false
                    );
                }
            }
        }
    }

    private void setQuantityToCheckoutInCartMap(int itemId, int itemQuantity) {
        if (null != this.cartMap.get(itemId)) {
            this.cartMap.get(itemId).setQuantityToCheckout(itemQuantity);
        }
    }

    private int getCartItemHolderPosition(int itemId) {
        int position = 0;
        for (int i = 0; i < this.cart.getCartItemAdapter().getCheckoutItemHolderSize(); i++) {
            CheckoutItemHolder checkoutItemHolder = this.cart.getCartItemAdapter().getCheckoutItemHolder(i);
            if (checkoutItemHolder.getCartItemDataObject().getItemId() == itemId) {
                position = i;
                break;
            }
        }
        return position;
    }
}