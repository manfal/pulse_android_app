package com.human_pulse.components.wrappers;

import com.human_pulse.components.listeners.ParentListItemListener;

import java.util.List;

public class ParentWrapper {

    private boolean mExpanded;

    private ParentListItemListener mParentListItemListener;

    public ParentWrapper(ParentListItemListener parentListItemListener) {
        mParentListItemListener = parentListItemListener;
        mExpanded = false;
    }

    public ParentListItemListener getParentListItem() {
        return mParentListItemListener;
    }

    public void setParentListItem(ParentListItemListener parentListItemListener) {
        mParentListItemListener = parentListItemListener;
    }

    public boolean isExpanded() {
        return mExpanded;
    }

    public void setExpanded(boolean expanded) {
        mExpanded = expanded;
    }

    public boolean isInitiallyExpanded() {
        return mParentListItemListener.isInitiallyExpanded();
    }

    public List<?> getChildItemList() {
        return mParentListItemListener.getChildItemList();
    }
}
