package com.human_pulse.components.dataobjects;

/**
 * Created by anfal on 12/17/2015.
 */
public class StoreDepartmentDataObject {
    private String departmentName = null;
    private int departmentId = 0;

    public StoreDepartmentDataObject(String departmentName, int departmentId) {
        this.departmentName = departmentName;
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return this.departmentName;
    }

    public int getDepartmentId() {
        return this.departmentId;
    }
}
