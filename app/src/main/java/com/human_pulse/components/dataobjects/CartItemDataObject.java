package com.human_pulse.components.dataobjects;

/**
 * Created by anfal on 1/13/2016.
 */
public class CartItemDataObject {
    private String itemName, itemWeightOrQuantity = null;
    private int itemId, quantityToCheckout, itemPrice, itemDiscount = 0;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getQuantityToCheckout() {
        return quantityToCheckout;
    }

    public void setQuantityToCheckout(int quantityToCheckout) {
        this.quantityToCheckout = quantityToCheckout;
    }

    public int getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(int itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getItemDiscount() {
        return itemDiscount;
    }

    public void setItemDiscount(int itemDiscount) {
        this.itemDiscount = itemDiscount;
    }

    public String getItemWeightOrQuantity() {
        return itemWeightOrQuantity;
    }

    public void setItemWeightOrQuantity(String itemWeightOrQuantity) {
        this.itemWeightOrQuantity = itemWeightOrQuantity;
    }
}