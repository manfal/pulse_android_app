package com.human_pulse.components.dataobjects;

/**
 * Created by anfal on 3/15/2016.
 */
public class AddressAdditionalDataObject {
    private String[] labels, cities = null;

    public String[] getCities() {
        return cities;
    }

    public void setCities(String[] cities) {
        this.cities = cities;
    }

    public String[] getLabels() {
        return labels;
    }

    public void setLabels(String[] labels) {
        this.labels = labels;
    }
}
