package com.human_pulse.components.dataobjects;

/**
 * Created by anfal on 1/15/2016.
 */
public class DeliveryChargesInfoDataObject {
    private int thresholdPrice, chargedPrice = 0;

    public int getThresholdPrice() {
        return thresholdPrice;
    }

    public void setThresholdPrice(int thresholdPrice) {
        this.thresholdPrice = thresholdPrice;
    }

    public int getChargedPrice() {
        return chargedPrice;
    }

    public void setChargedPrice(int chargedPrice) {
        this.chargedPrice = chargedPrice;
    }
}
