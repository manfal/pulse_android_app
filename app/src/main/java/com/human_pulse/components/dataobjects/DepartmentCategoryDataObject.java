package com.human_pulse.components.dataobjects;

/**
 * Created by anfal on 12/20/2015.
 */
public class DepartmentCategoryDataObject {
    private int categoryId;

    public int getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
