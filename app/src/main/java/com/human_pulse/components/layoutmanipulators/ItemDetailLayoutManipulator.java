package com.human_pulse.components.layoutmanipulators;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.human_pulse.app.R;
import com.human_pulse.components.dataobjects.DepartmentItemDataObject;
import com.human_pulse.components.dataobjects.ViewLocationDataObject;
import com.human_pulse.components.listeners.ItemAddToCartListener;
import com.human_pulse.components.observers.CartObserver;
import com.human_pulse.constants.Config;
import com.human_pulse.utilities.ItemCheckoutUtility;
import com.human_pulse.utilities.Utilities;

import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;

/**
 * Created by anfal on 12/19/2015.
 */
public class ItemDetailLayoutManipulator {

    private TextView itemDesription, itemNutritionFacts, itemPurchaseName, itemPurchaseQuantity, itemPurchasePrice, discountPrice, itemDiscountedPrice = null;
    private CardView itemDescriptionCard, itemNutritionFactsCard = null;
    private SupportAnimator animator = null;
    private ImageView itemImageBig = null;
    private ScrollView itemDetailLayout = null;
    private Spinner addToCartSpinner = null;
    private AppCompatActivity appCompatActivity = null;

    public ItemDetailLayoutManipulator(AppCompatActivity appCompatActivity) {
        this.itemDescriptionCard = (CardView) appCompatActivity.findViewById(R.id.department_item_description);
        this.itemNutritionFactsCard = (CardView) appCompatActivity.findViewById(R.id.department_item_nutrition_facts);
        this.itemDesription = (TextView) appCompatActivity.findViewById(R.id.item_description_text);
        this.itemNutritionFacts = (TextView) appCompatActivity.findViewById(R.id.item_nutrition_facts_text);
        this.itemImageBig = (ImageView) appCompatActivity.findViewById(R.id.department_item_image_big);
        this.itemDetailLayout = (ScrollView) appCompatActivity.findViewById(R.id.department_item_detail_layout);
        this.itemPurchaseName = (TextView) appCompatActivity.findViewById(R.id.item_detail_purchase_name);
        this.itemPurchasePrice = (TextView) appCompatActivity.findViewById(R.id.item_detail_purchase_price);
        this.itemPurchaseQuantity = (TextView) appCompatActivity.findViewById(R.id.item_detail_purchase_weight);
        this.discountPrice = (TextView) appCompatActivity.findViewById(R.id.department_item_detail_discount);
        this.itemDiscountedPrice = (TextView) appCompatActivity.findViewById(R.id.item_detail_purchase_discounted_price);
        this.addToCartSpinner = (Spinner) appCompatActivity.findViewById(R.id.department_item_add_to_cart_quantity);
        this.appCompatActivity = appCompatActivity;
    }

    public final void revealItemDetailLayout(DepartmentItemDataObject departmentItemDataObject, View itemView) throws Exception {
        if (Utilities.getInstance().isInternetConnectionNotAvailable(this.appCompatActivity)) {
            Toast.makeText(this.appCompatActivity, this.appCompatActivity.getString(R.string.enable_your_internet_text), Toast.LENGTH_LONG).show();
        } else {
            ViewLocationDataObject viewLocationDataObject = Utilities.getInstance().getViewCoordinates(itemView);
            int radiusOfItemDetailLayout = Math.max(
                    itemDetailLayout.getWidth(),
                    itemDetailLayout.getHeight()
            );
            this.animator = ViewAnimationUtils.createCircularReveal(
                    itemDetailLayout,
                    viewLocationDataObject.getX(),
                    viewLocationDataObject.getY(),
                    0,
                    radiusOfItemDetailLayout);
            this.animator.setInterpolator(new AccelerateDecelerateInterpolator());
            this.animator.setDuration(400);
            this.populateItemDetails(departmentItemDataObject);
            this.itemDetailLayout.setVisibility(View.VISIBLE);
            this.animator.start();
        }
    }

    private void populateItemDetails(DepartmentItemDataObject departmentItemDataObject) throws Exception {
        this.itemPurchaseName.setText(departmentItemDataObject.getItemName());
        this.itemPurchaseQuantity.setText(departmentItemDataObject.getItemWeightOrQuantity());
        this.itemPurchasePrice.setText(
                this.itemPurchasePrice.getContext().getString(
                        R.string.currency,
                        departmentItemDataObject.getItemPrice()
                )
        );
        if (0 < departmentItemDataObject.getItemDiscount() &&
            ItemCheckoutUtility.getInstance().isDiscountApplicable(
                    departmentItemDataObject.getItemPrice(),
                    departmentItemDataObject.getItemDiscount()
            )
        ) {
            this.discountPrice.setText(
                    this.discountPrice.getContext().getString(
                            R.string.discount_on_item,
                            departmentItemDataObject.getItemDiscount()
                    )
            );
            this.itemDiscountedPrice.setText(
                    this.itemDiscountedPrice.getContext().getString(
                            R.string.currency,
                            ItemCheckoutUtility.getInstance().getDiscountedPrice(
                                    departmentItemDataObject.getItemPrice(),
                                    departmentItemDataObject.getItemDiscount()
                            )
                    )
            );
            Utilities.getInstance().strikeThroughText(this.itemPurchasePrice);
        } else {
            Utilities.getInstance().removeStrikeThroughFromText(this.itemPurchasePrice);
        }
        if (null != departmentItemDataObject.getItemImageUrl() && !departmentItemDataObject.getItemImageUrl().equals("")) {
            Glide.with(this.itemImageBig.getContext())
                    .load(Config.API_HOST + departmentItemDataObject.getItemImageUrl())
                    .fitCenter()
                    .crossFade()
                    .into(this.itemImageBig);
            this.itemImageBig.setVisibility(View.VISIBLE);
        }
        if (null != departmentItemDataObject.getItemDescription() && !departmentItemDataObject.getItemDescription().equals("")) {
            this.itemDesription.setText(Html.fromHtml(departmentItemDataObject.getItemDescription()));
            this.itemDesription.setMovementMethod(LinkMovementMethod.getInstance());
            this.itemDescriptionCard.setVisibility(View.VISIBLE);
        }
        /*if (null != departmentItemDataObject.getNutritionFacts() && !departmentItemDataObject.getNutritionFacts().equals("")) {
            this.itemNutritionFacts.setText(Html.fromHtml(departmentItemDataObject.getNutritionFacts()));
            this.itemNutritionFactsCard.setVisibility(View.VISIBLE);
        }*/
        this.addToCartSpinner.setSelection(
                CartObserver.getCartObserverInstance().getQuantityToCheckoutFromMap(departmentItemDataObject.getItemId()),
                false
        );
        ItemAddToCartListener itemAddToCartListener = new ItemAddToCartListener(departmentItemDataObject, this.addToCartSpinner);
        this.addToCartSpinner.setOnTouchListener(itemAddToCartListener);
        this.addToCartSpinner.setOnItemSelectedListener(itemAddToCartListener);
    }

    public void hideItemDetails() throws Exception {
        this.animator.reverse().start();
        this.itemDetailLayout.setVisibility(View.INVISIBLE);
        this.itemDesription.setText(null);
        this.itemNutritionFacts.setText(null);
        this.itemPurchaseName.setText(null);
        this.itemPurchasePrice.setText(null);
        this.itemPurchaseQuantity.setText(null);
        this.discountPrice.setText(null);
        this.itemDiscountedPrice.setText(null);
        this.itemDescriptionCard.setVisibility(View.GONE);
        this.itemNutritionFactsCard.setVisibility(View.GONE);
        this.itemImageBig.setVisibility(View.INVISIBLE);
        this.animator = null;
    }

    public SupportAnimator getAnimator() {
        return this.animator;
    }
}