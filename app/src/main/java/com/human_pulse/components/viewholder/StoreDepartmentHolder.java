package com.human_pulse.components.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.human_pulse.actions.StoreActions;
import com.human_pulse.app.R;

import java.util.Random;

/**
 * Created by anfal on 12/17/2015.
 */
public class StoreDepartmentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private CardView cardView = null;
    private TextView textView = null;
    private int departmentPosition = 0;

    public StoreDepartmentHolder(View itemView) {
        super(itemView);
        this.cardView = (CardView) itemView.findViewById(R.id.department_card);
        this.setStoreDepartmentColorCode();
        this.textView = (TextView) itemView.findViewById(R.id.department_name);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        StoreActions.getStoreActionInstance().storeDepartmentClickListener(this.departmentPosition);
    }

    public TextView getTextView() {
        return this.textView;
    }

    public void setDepartmentPosition(int departmentPosition) {
        this.departmentPosition = departmentPosition;
    }

    private void setStoreDepartmentColorCode() {
        int[] colorArray = this.cardView.getResources().getIntArray(R.array.store_department_card_color);
        Random randomColorCodePosition = new Random();
        int randomColor = randomColorCodePosition.nextInt(colorArray.length);
        this.cardView.setCardBackgroundColor(colorArray[randomColor]);
    }
}
