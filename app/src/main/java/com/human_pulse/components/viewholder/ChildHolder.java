package com.human_pulse.components.viewholder;

import android.view.View;
import android.widget.TextView;

import com.human_pulse.app.R;

/**
 * Created by anfal on 12/5/2015.
 */
public class ChildHolder extends ChildViewHolder {
    public TextView titleTextView;

    public ChildHolder(View itemView) {
        super(itemView);
        titleTextView = (TextView) itemView.findViewById(R.id.titleChild);
    }

    public void bind(String childText) {
        titleTextView.setText(childText);
    }
}
