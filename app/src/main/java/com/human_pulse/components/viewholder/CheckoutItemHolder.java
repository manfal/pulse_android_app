package com.human_pulse.components.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.human_pulse.app.R;
import com.human_pulse.components.dataobjects.CartItemDataObject;
import com.human_pulse.components.listeners.CheckoutItemChangeQuantityListener;
import com.human_pulse.components.listeners.DeleteItemFromCartListener;
import com.human_pulse.enumerations.EItemCheckout;

/**
 * Created by anfal on 1/13/2016.
 */
public class CheckoutItemHolder extends RecyclerView.ViewHolder {

    private ImageView increaseItemQuantity, decreaseItemQuantity, itemDeleteFromCart = null;
    private TextView itemQuantityText, itemNameInformation = null;
    private CartItemDataObject cartItemDataObject = null;

    public CheckoutItemHolder(View itemView) {
        super(itemView);
        this.increaseItemQuantity = (ImageView) itemView.findViewById(R.id.increase_item_quantity_arrow);
        this.decreaseItemQuantity = (ImageView) itemView.findViewById(R.id.decrease_item_quantity_arrow);
        this.itemQuantityText = (TextView) itemView.findViewById(R.id.item_quantity_text);
        this.itemNameInformation = (TextView) itemView.findViewById(R.id.item_name_information);
        this.itemDeleteFromCart = (ImageView) itemView.findViewById(R.id.item_delete_from_cart);
    }

    public TextView getItemQuantityText() {
        return itemQuantityText;
    }

    public TextView getItemNameInformation() {
        return itemNameInformation;
    }

    public void setCheckoutItemListeners() {
        this.increaseItemQuantity.setOnClickListener(new CheckoutItemChangeQuantityListener(this.cartItemDataObject, EItemCheckout.INCREASE));
        this.decreaseItemQuantity.setOnClickListener(new CheckoutItemChangeQuantityListener(this.cartItemDataObject, EItemCheckout.DECREASE));
        this.itemDeleteFromCart.setOnClickListener(new DeleteItemFromCartListener(this.cartItemDataObject));
    }

    public CartItemDataObject getCartItemDataObject() {
        return this.cartItemDataObject;
    }

    public void setCartItemDataObject(CartItemDataObject cartItemDataObject) {
        this.cartItemDataObject = cartItemDataObject;
    }
}