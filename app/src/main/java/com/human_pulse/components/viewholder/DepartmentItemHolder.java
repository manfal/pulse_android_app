package com.human_pulse.components.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.human_pulse.app.R;
import com.human_pulse.components.dataobjects.DepartmentItemDataObject;
import com.human_pulse.components.listeners.ItemAddToCartListener;
import com.human_pulse.components.proxies.DepartmentItemClickProxy;

/**
 * Created by anfal on 12/20/2015.
 */
public class DepartmentItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private ImageView imageView = null;
    private TextView itemPriceTexView, itemNameTextView, itemWeightTextView, itemDiscountTextView = null;
    private Spinner addToCartSpinner = null;
    private DepartmentItemDataObject departmentItemDataObject = null;

    public DepartmentItemHolder(View itemView) {
        super(itemView);
        this.imageView = (ImageView) itemView.findViewById(R.id.department_item_image);
        this.itemPriceTexView = (TextView) itemView.findViewById(R.id.department_item_price);
        this.itemNameTextView = (TextView) itemView.findViewById(R.id.department_item_name);
        this.itemWeightTextView = (TextView) itemView.findViewById(R.id.department_item_weight);
        this.itemDiscountTextView = (TextView) itemView.findViewById(R.id.department_item_discount);
        this.addToCartSpinner = (Spinner) itemView.findViewById(R.id.department_add_to_cart_quantity);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        DepartmentItemClickProxy departmentItemClickProxy = new DepartmentItemClickProxy();
        departmentItemClickProxy.itemClicked(v, this.departmentItemDataObject);
    }

    public ImageView getImageView() {
        return this.imageView;
    }

    public TextView getItemPriceTexView() {
        return itemPriceTexView;
    }

    public TextView getItemNameTextView() {
        return itemNameTextView;
    }

    public TextView getItemWeightTextView() {
        return itemWeightTextView;
    }

    public TextView getItemDiscountTextView() {
        return itemDiscountTextView;
    }

    public void setViewListeners() {
        ItemAddToCartListener itemAddToCartListener = new ItemAddToCartListener(this.departmentItemDataObject, this.addToCartSpinner);
        this.addToCartSpinner.setOnTouchListener(itemAddToCartListener);
        this.addToCartSpinner.setOnItemSelectedListener(itemAddToCartListener);
    }

    public Spinner getAddToCartSpinner() {
        return this.addToCartSpinner;
    }

    public DepartmentItemDataObject getDepartmentItemDataObject() {
        return this.departmentItemDataObject;
    }

    public void setDepartmentItemDataObject(DepartmentItemDataObject departmentItemDataObject) {
        this.departmentItemDataObject = departmentItemDataObject;
    }
}
