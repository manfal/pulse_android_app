package com.human_pulse.components.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.human_pulse.actions.OrderHistoryActions;
import com.human_pulse.app.R;
import com.human_pulse.components.dataobjects.OrderHistoryDataObject;

/**
 * Created by anfal on 1/8/2016.
 */
public class OrderHistoryItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private OrderHistoryDataObject orderHistoryDataObject = null;
    private TextView orderNumber, orderDate, orderItems, orderCost = null;

    public OrderHistoryItemHolder(View itemView) {
        super(itemView);
        this.orderNumber = (TextView) itemView.findViewById(R.id.order_no_text);
        this.orderDate = (TextView) itemView.findViewById(R.id.order_date_text);
        this.orderItems = (TextView) itemView.findViewById(R.id.order_items_text);
        this.orderCost = (TextView) itemView.findViewById(R.id.order_cost_text);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        OrderHistoryActions.getOrderHistoryActionsInstance().displayOrderCompleteHistory(this.orderHistoryDataObject, v);
    }

    public void setOrderHistoryDataObject(OrderHistoryDataObject orderHistoryDataObject) {
        this.orderHistoryDataObject = orderHistoryDataObject;
    }

    public TextView getOrderNumber() {
        return orderNumber;
    }

    public TextView getOrderDate() {
        return orderDate;
    }

    public TextView getOrderItems() {
        return orderItems;
    }

    public TextView getOrderCost() {
        return orderCost;
    }
}
