package com.human_pulse.components.interfaces;

import com.human_pulse.components.viewholder.AddressHolder;
import com.human_pulse.enumerations.EAddressManipulationChoice;

import org.json.JSONObject;

/**
 * Created by anfal on 1/28/2016.
 */
public interface IAddressResponseSenderReceiver {
    void fetchDataForAddressDialog(EAddressManipulationChoice eAddressManipulationChoice, AddressHolder addressHolder);

    void openNewAddressDialog();

    void openNewAddressDialog(EAddressManipulationChoice addressManipulationChoice, JSONObject response);

    void sendNewAddressRequest(String label, String address, String city);

    void receiveNewAddressResponse(JSONObject response);
}
