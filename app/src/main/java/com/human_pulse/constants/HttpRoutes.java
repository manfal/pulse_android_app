package com.human_pulse.constants;

/**
 * Created by anfal on 12/8/2015.
 */
public class HttpRoutes {
    public static final String LOGIN_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "auth/login";
    public static final String REGISTER_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "auth/register";
    public static final String RESET_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "auth/reset";
    public static final String NAV_ITEMS_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "store/get_nav_items";
    public static final String STORE_DEPARTMENTS_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "store/get_store_departments";
    public static final String DEPARTMENT_CATEGORIES_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "department/get_categories";
    public static final String DEPARTMENT_ITEMS_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "department/get_department_items";
    public static final String SEARCH_ITEMS_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "search/get_search_result";
    public static final String USER_ACCOUNT_INFO_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "account/get_user_account_info";
    public static final String USER_ACCOUNT_ADDRESSES_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "account/get_user_account_addresses";
    public static final String NEW_ADDRESS_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "account/add_new_address";
    public static final String DELETE_ADDRESS_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "account/delete_address";
    public static final String EDIT_ADDRESS_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "account/edit_address";
    public static final String EDIT_ACCOUNT_PASSWORD_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "account/edit_user_account_password";
    public static final String EDIT_USER_PERSONAL_INFO_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "account/edit_user_personal_info";
    public static final String PHONE_NUMBER_VERIFICATION_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "account/accept_mobile_verification_code";
    public static final String ORDER_HISTORY_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "account/get_order_history";
    public static final String INVOICE_DETAIL_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "account/get_invoice_detail";
    public static final String CLIENT_SYNC_INFO_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "account/get_client_sync_info";
    public static final String GET_PRE_CHECKOUT_DATA_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "account/get_pre_checkout_data";
    public static final String ADD_PHONE_NUMBER_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "account/update_phone_number";
    public static final String GET_INVOICE_REVIEW_INFO_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "checkout/get_invoice_review_data";
    public static final String STORE_DELIVERY_ITEMS_REQUST = Config.API_HOST + Config.UTILSTORE_API_VERSION + "checkout/checkout_items";
    public static final String CUSTOM_ORDER_REQUEST = Config.API_HOST + Config.UTILSTORE_API_VERSION + "checkout/checkout_custom_order";
    public static final String OPEN_GOOGLE_PLAY_LINK_FOR_APP = "market://details?id=";
    public static final String OPEN_BROWSER_LINK_FOR_APP = "https://play.google.com/store/apps/details?id=";
    public static final String HELP_PAGE_ROUTE = Config.API_HOST + "help";
    public static final String LOCATION_PAGE_ROUTE = Config.API_HOST + "locations";
    public static final String TERMS_AND_CONDITIONS_PAGE_ROUTE = Config.API_HOST + "termsandconditions";
    public static final String CONTACT_PAGE_ROUTE = Config.API_HOST + "contact";
    public static final String PRIVACY_POLICY_PAGE_ROUTE = Config.API_HOST + "privacypolicy";
    public static final String GET_ADD_ADDRESS_INFO_ROUTE = Config.API_HOST + Config.UTILSTORE_API_VERSION + "account/get_add_address_info";

    private HttpRoutes() {
    }
}
