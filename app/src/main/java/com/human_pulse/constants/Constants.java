package com.human_pulse.constants;

/**
 * Created by anfal on 12/9/2015.
 */
public class Constants {
    public static final String STATUS = "STATUS";
    public static final String MESSAGE = "MESSAGE";
    public static final int SUCCESS = 1;
    public static final int ERROR = 0;
    public static final int NON_APPLICABLE_ID = -10;
    public static final String ROLE = "ROLE";
    public static final String EMAIL = "EMAIL";
    public static final String THRESHOLDPRICE = "THRESHOLDPRICE";
    public static final String COMPANY_OPEN_RESPONSE = "COMPANY_OPEN_RESPONSE";
    public static final String IS_COMPANY_OPEN = "is_company_open";
    public static final String OPENING_TIME = "opening_time";
    public static final String CLOSING_TIME = "closing_time";
    public static final String CHARGEDPRICE = "CHARGEDPRICE";
    public static final String APP_VERSION_FLAG = "APP_VERSION_FLAG";
    public static final String FLURRY_API_KEY = "4HM5R7R2966QVSBHZSYZ";
    public static final String DEPARTMENT_NAME = "department_name";
    public static final String DEPARTMENT_CATEGORIES = "department_categories";
    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_ID = "category_id";
    public static final String NAVLIST = "NAVLIST";
    public static final String DEPARTMENT_LIST = "DEPARTMENTS";
    public static final String ITEM_QUANTITY_TO_CHECKOUT = "item_quantity_to_checkout";
    public static final String DEPARTMENT_ID = "item_department";
    public static final String DEPARTMENT_PICTURE = "department_picture";
    public static final String META_DATA_NOT_FOUND_EXCEPTION = "Could not retrieve metadata for activity";
    public static final String CATEGORY_LIST = "CATEGORIES";
    public static final String DEPARTMENT_CATEGORY_ALL = "All";
    public static final String ITEM_LIST = "ITEMS";
    public static final String ITEM_ID = "item_id";
    public static final String ITEM_PRICE = "item_price";
    public static final String ITEM_DISCOUNT = "item_discount";
    public static final String ITEM_IMAGE_THUMBNAIL = "item_picture_thumbnail";
    public static final String ITEM_IMAGE = "item_picture";
    public static final String ITEM_WEIGHT_OR_QUANTITY = "item_weight";
    public static final String ITEM_NUTRITION_FACTS = "item_nutrition_facts";
    public static final String ITEM_DESCRIPTION = "item_description";
    public static final String ITEM_NAME = "item_name";
    public static final int BATCH_SIZE = 30;
    public static final String DEPARTMENT_ITEMS_PARAMS = "?department_id=%1$s&category_id=%2$s&batch_start=%3$s&batch_size=%4$s&email=%5$s";
    public static final String SINGLE_GET_PARAM = "/%1$s";
    public static final String BATCH_START_KEY = "START";
    public static final String BATCH_SIZE_KEY = "BATCH_SIZE";
    public static final int DEFAULT_CATEGORY_ID = -1;
    public static final int REQUEST_RETRY_TIMEOUT = 8000;
    public static final int REQUST_RETRY_COUNT = 5;
    public static final String DEPARTMENT_IDENTIFIER = "department_id";
    public static final String USERNAME_PARAM = "username";
    public static final String PASSWORD_PARAM = "password";
    public static final String EMAIL_PARAM = "email";
    public static final String FIRSTNAME_PARAM = "firstname";
    public static final String LASTNAME_PARAM = "lastname";
    public static final String SEARCH_ITEMS_PARAMS = "?search_keyword=%1$s&batch_start=%2$s&batch_size=%3$s&email=%4$s";
    public static final String SEARCH_KEYWORD = "SEARCH_KEYEWORD";
    public static final String USER_GET_ACCOUNT_INFO_PARAM = "?email=%1$s";
    public static final String USER_INFO = "USERINFO";
    public static final String FIRST_NAME = "FIRST_NAME";
    public static final String LAST_NAME = "LAST_NAME";
    public static final String MOBILE_NUMBER = "MOBILE_NUMBER";
    public static final String IS_MOBILE_NUMBER_CONFIRMED = "IS_MOBILE_NUMBER_CONFIRMED";
    public static final String ADDRESSES = "ADDRESSES";
    public static final String ADDRESS_ID = "address_id";
    public static final String ADDRESS_LABEL = "address_label";
    public static final String ADDRESS_TEXT = "address_address";
    public static final String ADDRESS_CITY = "address_city";
    public static final String LABEL_PARAM = "label";
    public static final String ADDRESS_PARAM = "address";
    public static final String CITY_PARAM = "city";
    public static final String NEW_ADDRESS = "NEW_ADDRESS";
    public static final String OLD_PASSWORD_PARAM = "old-password";
    public static final String NEW_PASSWORD_PARAM = "new-password";
    public static final String REPEAT_NEW_PASSWORD_PARAM = "repeat-new-password";
    public static final String MOBILE_NUMBER_PARAM = "phnum";
    public static final String VERIFICATION_CODE_PARAM = "verification_code";
    public static final String ORDER_HISTORY_PARAMS = "?email=%1$s&batch_start=%2$s&batch_size=%3$s";
    public static final String INVOICE_LIST = "INVOICES";
    public static final String INVOICE_ID = "invoice_id";
    public static final String INVOICE_NAME = "invoice_name";
    public static final String DELIVERY_STATUS_LABEL = "deliverystatus_label";
    public static final String ORDER_DELIVERY_DATE = "order_date";
    public static final String TOTAL_ORDERED_ITEMS = "total_items";
    public static final String TOTAL_ORDER_PRICE = "total_price";
    public static final String INVOICE_DETAIL_PARAMS = "?invoice_id=%1$s";
    public static final String INVOICE_INFO = "INVOICEINFO";
    public static final String ADDRESS_INFO = "ADDRESSINFO";
    public static final String DELIVERY_ADDRESS = "delivery_address";
    public static final String ITEMS_INFO = "ITEMSINFO";
    public static final String INVOICE_COUPON_CODE_VALUE = "invoice_couponcode_value";
    public static final String OH_ITEM_QUANTITY = "oh_item_quantity";
    public static final String OH_ITEM_NAME = "oh_item_name";
    public static final String OH_ITEM_CHARGED_PRICE = "item_charged_price";
    public static final String OH_TOTAL_CHARGED_PRICE = "TOTALPRICE";
    public static final String OH_DELIVERY_CHARGES = "DELIVERYCHARGES";
    public static final String OH_ITEM_ID = "oh_item";
    public static final String ADDRESSE_LABELS = "ADDRESSE_LABELS";
    public static final String INVOICE_REVIEW_REQUEST_PARAMS = "?email=%1$s&address_label=%2$s&coupon_code=%3$s&order_cost=%4$s";
    public static final String ITEMS_TO_CHECKOUT = "items_to_checkout";
    public static final String PAGE_NAME = "page_index";
    public static final String COUPON_DEDUCTION = "coupon_deduction";
    public static final String COUPON_CODE = "coupon_code";
    public static final String DEFAULT_EMAIL_ID = "dummy@dummy.com";
    public static final String DEFAULT_USER_ROLE = "USER";
    public static final String LABELS = "LABELS";
    public static final String ADDRESS_NEW_LABEL = "addresslabel_label";
    public static final String CITY_NAME = "city_name";
    public static final String CITIES = "CITIES";
    public static final String CUSTOM_ORDER = "custom_order";

    //This is the metadata for search activity to get back to.
    // It needs better way to be implemented.
    // Although these fields shouldn't be in the constants class but I am too lazy to make a new class :p
    public static Class className = null;
    public static String departmentName, categoryName, pageName = null;
    public static int departmentId = 0;
    public static int categoryId = DEFAULT_CATEGORY_ID;
    public static boolean checkForStaticData, shouldUserBeRedirectedToCheckoutPage, isCustomOrder = false;

    private Constants() {
    }
}
