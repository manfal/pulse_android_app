package com.human_pulse.constants;

/**
 * Created by anfal on 12/8/2015.
 */
public class Config {
    //public static String API_HOST = "http://192.168.43.121/braashen/";
    public static final String UTILSTORE_API_VERSION = "api/v1/";
    //public static String API_HOST = "http://192.168.8.100/pulse/";
    //public static String API_HOST = "http://192.168.8.101/pulse/";
    //public static String API_HOST = "http://10.0.2.2/pulse/";
    public static String API_HOST = "http://pulse.com.pk/";
    //public static String API_HOST = "http://localhost/pulse/";

    private Config() {
    }
}