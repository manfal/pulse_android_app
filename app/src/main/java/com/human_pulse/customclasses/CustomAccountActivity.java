package com.human_pulse.customclasses;

import android.util.Log;

import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.app.StorePage;
import com.human_pulse.utilities.Utilities;

import static com.human_pulse.constants.Constants.shouldUserBeRedirectedToCheckoutPage;

/**
 * Created by anfal on 2/22/2016.
 */
public class CustomAccountActivity extends FontCompatActivity {

    private ILog logger = null;

    public void setLogger(ILog logger) {
        this.logger = logger;
    }

    @Override
    public void onBackPressed() {
        try {
            //This line just makes sure if user got to this page from checkout button but did not used it
            //then he should be redirected to checkout page by mistake.
            shouldUserBeRedirectedToCheckoutPage = false;
            Utilities.getInstance().startNewActivityByFinishingOlder(this, StorePage.class);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
