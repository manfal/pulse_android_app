package com.human_pulse.customclasses;

import android.graphics.Bitmap;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

/**
 * Created by anfal on 2/2/2016.
 */
public class CustomWebViewClient extends WebViewClient {

    private ProgressBar progressBar = null;

    public CustomWebViewClient(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        this.progressBar.setVisibility(View.GONE);
        super.onPageFinished(view, url);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        this.progressBar.setVisibility(View.VISIBLE);
        super.onPageStarted(view, url, favicon);
    }
}
