package com.human_pulse.customclasses;

import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.app.R;
import com.human_pulse.app.SearchPage;
import com.human_pulse.utilities.Utilities;

import static com.human_pulse.constants.Constants.className;

/**
 * Created by anfal on 2/3/2016.
 */
public class CustomCompleteToolBarMenuActivity extends FontCompatActivity {

    private DrawerLayout drawerLayout = null;
    private ILog logger = null;

    public void assignDrawerLayout(DrawerLayout drawerLayout) {
        this.drawerLayout = drawerLayout;
        this.logger = Logger.getLogger();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (this.drawerLayout.isDrawerOpen(Gravity.RIGHT) || this.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            Utilities.getInstance().hideMenuItemsOnToolbar(menu, false);
        } else {
            Utilities.getInstance().hideMenuItemsOnToolbar(menu, true);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.cart_action:
                try {
                    if (this.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                        this.drawerLayout.closeDrawer(Gravity.LEFT);
                    }
                    this.drawerLayout.openDrawer(Gravity.RIGHT);
                } catch (Exception ex) {
                    this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
                }
                return true;
            case R.id.search_action:
                try {
                    className = this.getClass();
                    Utilities.getInstance().startNewActivityByFinishingOlder(this, SearchPage.class);
                } catch (Exception ex) {
                    this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
                }
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }
}
