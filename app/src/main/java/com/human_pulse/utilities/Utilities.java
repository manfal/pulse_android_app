package com.human_pulse.utilities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.human_pulse.app.CheckoutPage;
import com.human_pulse.app.CustomerHelpPage;
import com.human_pulse.app.DepartmentPage;
import com.human_pulse.app.LoginPage;
import com.human_pulse.app.R;
import com.human_pulse.app.SearchPage;
import com.human_pulse.app.StorePage;
import com.human_pulse.components.dataobjects.AddressAdditionalDataObject;
import com.human_pulse.components.dataobjects.DepartmentItemDataObject;
import com.human_pulse.components.dataobjects.ViewLocationDataObject;
import com.human_pulse.database.ClientDB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.human_pulse.constants.Constants.ADDRESS_NEW_LABEL;
import static com.human_pulse.constants.Constants.APP_VERSION_FLAG;
import static com.human_pulse.constants.Constants.BATCH_SIZE;
import static com.human_pulse.constants.Constants.BATCH_SIZE_KEY;
import static com.human_pulse.constants.Constants.BATCH_START_KEY;
import static com.human_pulse.constants.Constants.CATEGORY_ID;
import static com.human_pulse.constants.Constants.CITIES;
import static com.human_pulse.constants.Constants.CITY_NAME;
import static com.human_pulse.constants.Constants.CLOSING_TIME;
import static com.human_pulse.constants.Constants.CUSTOM_ORDER;
import static com.human_pulse.constants.Constants.DEFAULT_EMAIL_ID;
import static com.human_pulse.constants.Constants.DEPARTMENT_ID;
import static com.human_pulse.constants.Constants.DEPARTMENT_NAME;
import static com.human_pulse.constants.Constants.ITEM_DESCRIPTION;
import static com.human_pulse.constants.Constants.ITEM_DISCOUNT;
import static com.human_pulse.constants.Constants.ITEM_ID;
import static com.human_pulse.constants.Constants.ITEM_IMAGE;
import static com.human_pulse.constants.Constants.ITEM_IMAGE_THUMBNAIL;
import static com.human_pulse.constants.Constants.ITEM_NAME;
import static com.human_pulse.constants.Constants.ITEM_NUTRITION_FACTS;
import static com.human_pulse.constants.Constants.ITEM_PRICE;
import static com.human_pulse.constants.Constants.ITEM_WEIGHT_OR_QUANTITY;
import static com.human_pulse.constants.Constants.LABELS;
import static com.human_pulse.constants.Constants.OH_ITEM_QUANTITY;
import static com.human_pulse.constants.Constants.OPENING_TIME;
import static com.human_pulse.constants.Constants.PAGE_NAME;
import static com.human_pulse.constants.Constants.SEARCH_KEYWORD;
import static com.human_pulse.constants.Constants.checkForStaticData;
import static com.human_pulse.constants.Constants.isCustomOrder;
import static com.human_pulse.constants.Constants.shouldUserBeRedirectedToCheckoutPage;

/**
 * Created by anfal on 12/8/2015.
 */
public class Utilities {

    private static Utilities utilityInstance = null;

    private Utilities() {
    }

    public static Utilities getInstance() {
        if (null == utilityInstance) {
            utilityInstance = new Utilities();
        }
        return utilityInstance;
    }

    public boolean isInternetConnectionNotAvailable(Context context) throws Exception {
        boolean isConnected = false;
        ConnectivityManager conManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conManager.getActiveNetworkInfo();
        if (null == netInfo || !netInfo.isConnected()) {
            isConnected = true;
        }
        return isConnected;
    }

    public void hideMenuItemsOnToolbar(Menu menu, boolean shouldHide) {
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setVisible(shouldHide);
        }
    }

    public ViewLocationDataObject getViewCoordinates(View view) {
        ViewLocationDataObject viewLocationDataObject = new ViewLocationDataObject();
        int[] location = new int[2];
        view.getLocationInWindow(location);
        int centerX = view.getWidth() / 2;
        int centerY = view.getHeight() / 2;
        int centerXOnScreen = location[0] + centerX;
        int centerYOnScreen = location[1] + centerY;
        viewLocationDataObject.setX(centerXOnScreen);
        viewLocationDataObject.setY(centerYOnScreen);
        return viewLocationDataObject;
    }

    public List<DepartmentItemDataObject> parseItems(JSONArray itemsArray) throws JSONException {
        List<DepartmentItemDataObject> departmentItemDataObjects = new ArrayList<>();
        for (int itemNumber = 0; itemNumber < itemsArray.length(); itemNumber++) {
            JSONObject itemObject = itemsArray.getJSONObject(itemNumber);
            DepartmentItemDataObject departmentItemDataObject = new DepartmentItemDataObject();
            departmentItemDataObject.setItemId(Integer.parseInt(itemObject.getString(ITEM_ID)));
            departmentItemDataObject.setItemPrice(Integer.parseInt(itemObject.getString(ITEM_PRICE)));
            departmentItemDataObject.setItemDiscount(Integer.parseInt(itemObject.getString(ITEM_DISCOUNT)));
            departmentItemDataObject.setItemImageUrl(itemObject.getString(ITEM_IMAGE));
            departmentItemDataObject.setItemImageThumbnailUrl(itemObject.getString(ITEM_IMAGE_THUMBNAIL));
            departmentItemDataObject.setItemWeightOrQuantity(itemObject.getString(ITEM_WEIGHT_OR_QUANTITY));
            departmentItemDataObject.setNutritionFacts(itemObject.getString(ITEM_NUTRITION_FACTS));
            departmentItemDataObject.setItemDescription(itemObject.getString(ITEM_DESCRIPTION));
            departmentItemDataObject.setItemName(itemObject.getString(ITEM_NAME));
            departmentItemDataObject.setOrderedQuantity(itemObject.getInt(OH_ITEM_QUANTITY));
            departmentItemDataObjects.add(departmentItemDataObject);
        }
        return departmentItemDataObjects;
    }

    public void hideSoftKeyBoard(Context context, View view) throws Exception {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != view) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public int getColorFromResource(Context context, int id) throws Exception {
        return ContextCompat.getColor(context, id);
    }

    public void configureTabLook(TabLayout tabLayout) throws Exception {
        int categoryColor = this.getColorFromResource(tabLayout.getContext(), R.color.white);
        tabLayout.setSelectedTabIndicatorColor(categoryColor);
        tabLayout.setTabTextColors(categoryColor, categoryColor);
    }

    public void decorateHyperLink(TextView textView) throws Exception {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    public void strikeThroughText(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public void removeStrikeThroughFromText(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
    }

    public void startDepartmentActivity(AppCompatActivity appCompatActivity, String departmentName, int departmentId, int categoryId) throws Exception {
        Intent departmentActivity = new Intent(appCompatActivity, DepartmentPage.class);
        departmentActivity.putExtra(DEPARTMENT_NAME, departmentName);
        departmentActivity.putExtra(DEPARTMENT_ID, departmentId);
        departmentActivity.putExtra(CATEGORY_ID, categoryId);
        this.startNewActivityFromIntent(appCompatActivity, departmentActivity);
        appCompatActivity.finish();
    }

    public void resetStaticActivityData() {
        checkForStaticData = false;
    }

    public void startNewActivityFromIntent(Context context, Intent intent) {
        context.startActivity(intent);
    }

    public void startNewActivity(AppCompatActivity appCompatActivity, Class newClass) throws Exception {
        Intent newActivity = new Intent(appCompatActivity, newClass);
        this.startNewActivityFromIntent(appCompatActivity, newActivity);
    }

    public void startNewActivityByFinishingOlder(AppCompatActivity appCompatActivity, Class newClass) throws Exception {
        startNewActivity(appCompatActivity, newClass);
        appCompatActivity.finish();
    }

    public void startStoreActivityFromSplashScreen(AppCompatActivity appCompatActivity, int versionFlag, String openingTime, String closingTime) {
        Intent storeActivity = new Intent(appCompatActivity, StorePage.class);
        storeActivity.putExtra(APP_VERSION_FLAG, versionFlag);
        if (null != openingTime && null != closingTime) {
            storeActivity.putExtra(OPENING_TIME, openingTime);
            storeActivity.putExtra(CLOSING_TIME, closingTime);
        }
        this.startNewActivityFromIntent(appCompatActivity, storeActivity);
        appCompatActivity.finish();
    }

    public void startCustomerHelpActivity(AppCompatActivity appCompatActivity, String pageName) {
        Intent customerHelpActivity = new Intent(appCompatActivity, CustomerHelpPage.class);
        customerHelpActivity.putExtra(PAGE_NAME, pageName);
        this.startNewActivityFromIntent(appCompatActivity, customerHelpActivity);
        appCompatActivity.finish();
    }

    public Map<String, Integer> getItemBatch(int itemBatch) {
        Map<String, Integer> batch = new HashMap<>();
        batch.put(BATCH_START_KEY, itemBatch);
        batch.put(BATCH_SIZE_KEY, BATCH_SIZE);
        return batch;
    }

    public boolean setIsDataFetchNecessary(int dataSetSize) {
        return dataSetSize >= BATCH_SIZE;
    }

    public int incrementItemBatch(int itemBatch) {
        return itemBatch + BATCH_SIZE;
    }

    public boolean isSessionNotEmpty(Context context) {
        ClientDB clientDB = new ClientDB(context);
        return (clientDB.isSessionNotEmpty() && !clientDB.getUserEmail().toLowerCase().equals(DEFAULT_EMAIL_ID));
    }

    public String[] getDefaultMenuArray(Context context) {
        String[] defaultMenuArray;
        if (this.isSessionNotEmpty(context)) {
            defaultMenuArray = context.getResources().getStringArray(R.array.default_logged_in_menu_array);
        } else {
            defaultMenuArray = context.getResources().getStringArray(R.array.default_logged_out_menu_array);
        }
        return defaultMenuArray;
    }

    public void insertSessionRecordAfterDeletionInDB(Context context, String role, String email, int thresholdPrice, int chargedPrice) throws Exception {
        ClientDB clientDB = new ClientDB(context);
        //When user logs in it is necessary to check if session is empty or not if not delete older session
        if (clientDB.isSessionNotEmpty()) {
            clientDB.deleteSessionRecords();
        }
        clientDB.insertSessionRow(role, email, thresholdPrice, chargedPrice);
    }

    public AddressAdditionalDataObject parseAdditionalAddressInfo(JSONObject additionalInfo) throws Exception {
        AddressAdditionalDataObject addressAdditionalDataObject = new AddressAdditionalDataObject();
        JSONArray labels = additionalInfo.getJSONArray(LABELS);
        JSONArray cities = additionalInfo.getJSONArray(CITIES);
        String[] labelArray = new String[labels.length()];
        String[] cityArray = new String[cities.length()];
        for (int i = 0; i < labels.length(); i++) {
            labelArray[i] = labels.getJSONObject(i).getString(ADDRESS_NEW_LABEL);
        }
        for (int j = 0; j < cities.length(); j++) {
            cityArray[j] = cities.getJSONObject(j).getString(CITY_NAME);
        }
        addressAdditionalDataObject.setLabels(labelArray);
        addressAdditionalDataObject.setCities(cityArray);
        return addressAdditionalDataObject;
    }

    public ArrayAdapter<String> parseAddressSpinnerData(String[] labels, String firstLabel, Context context, boolean selective) throws Exception {
        List<String> labelList = new ArrayList<>();
        if (!selective) {
            Collections.addAll(labelList, labels);
        } else {
            labelList.add(firstLabel);
            for (String label : labels) {
                if (!label.toLowerCase().equals(firstLabel.toLowerCase())) {
                    labelList.add(label);
                }
            }
        }
        return new ArrayAdapter<>(context, R.layout.simple_spinner_item_2, labelList);
    }

    public void startSearchActivityWithMetaData(AppCompatActivity appCompatActivity, String keyWord) throws Exception {
        Intent departmentActivity = new Intent(appCompatActivity, SearchPage.class);
        departmentActivity.putExtra(SEARCH_KEYWORD, keyWord);
        this.startNewActivityFromIntent(appCompatActivity, departmentActivity);
        appCompatActivity.finish();
    }

    public void startCheckOutWithMetaData(AppCompatActivity appCompatActivity, boolean isCustomOrder) throws Exception {
        Intent checkoutActivity = new Intent(appCompatActivity, CheckoutPage.class);
        checkoutActivity.putExtra(CUSTOM_ORDER, isCustomOrder);
        this.startNewActivityFromIntent(appCompatActivity, checkoutActivity);
        appCompatActivity.finish();
    }

    public void initiateCustomCheckout(AppCompatActivity appCompatActivity) throws Exception {
        if (this.isSessionNotEmpty(appCompatActivity)) {
            this.startCheckOutWithMetaData(appCompatActivity, true);
        } else {
            shouldUserBeRedirectedToCheckoutPage = true;
            isCustomOrder = true;
            Utilities.getInstance().startNewActivityByFinishingOlder(appCompatActivity, LoginPage.class);
        }
    }
}