package com.human_pulse.utilities;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.app.R;
import com.human_pulse.components.interfaces.IInternetConnectionObject;
import com.human_pulse.components.interfaces.IInternetConnectionSubject;
import com.human_pulse.components.listeners.InternetConnectivityButtonListener;

/**
 * Created by anfal on 2/6/2016.
 */
public class InternetConnectionUtility implements IInternetConnectionSubject {

    AppCompatActivity appCompatActivity = null;
    private ILog logger = null;
    private IInternetConnectionObject internetConnectionObject = null;
    private RelativeLayout noInternetLayout = null;
    private Button reconnectButton = null;

    public InternetConnectionUtility(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
        this.logger = Logger.getLogger();
        this.noInternetLayout = (RelativeLayout) this.appCompatActivity.findViewById(R.id.reconnect_layout);
        this.reconnectButton = (Button) this.appCompatActivity.findViewById(R.id.reconnect_button);
    }

    public boolean checkInternetConnectivity() {
        boolean isConnected = true;
        try {
            if (Utilities.getInstance().isInternetConnectionNotAvailable(this.appCompatActivity)) {
                this.updateObserver();
                isConnected = false;
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
        return isConnected;
    }

    @Override
    public void registerAsObserver(IInternetConnectionObject internetConnectionObject) {
        try {
            this.internetConnectionObject = internetConnectionObject;
            if (null != this.reconnectButton) {
                this.reconnectButton.setOnClickListener(new InternetConnectivityButtonListener(this.internetConnectionObject, this.noInternetLayout));
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void unRegisterAsObserver(IInternetConnectionObject internetConnectionObject) {
        try {
            this.internetConnectionObject = null;
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void updateObserver() {
        try {
            if (null != this.internetConnectionObject) {
                if (null != this.noInternetLayout) {
                    this.noInternetLayout.setVisibility(View.VISIBLE);
                }
                this.internetConnectionObject.startReconnecting();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void showNoInternetMessage() {
        try {
            Toast.makeText(this.appCompatActivity, this.appCompatActivity.getString(R.string.alert_no_error_message), Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
