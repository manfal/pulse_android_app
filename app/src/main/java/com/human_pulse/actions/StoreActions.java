package com.human_pulse.actions;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.flurry.android.FlurryAgent;
import com.human_pulse.analytics.LogEvents;
import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.app.BuildConfig;
import com.human_pulse.app.R;
import com.human_pulse.components.CustomComponents;
import com.human_pulse.components.adapter.StoreDepartmentsAdapter;
import com.human_pulse.components.dataobjects.StoreDepartmentDataObject;
import com.human_pulse.components.interfaces.IInternetConnectionObject;
import com.human_pulse.components.listeners.StoreSearchBoxListener;
import com.human_pulse.components.navigation.StoreMenu;
import com.human_pulse.components.observers.CartObserver;
import com.human_pulse.components.observers.LeftNavigationObserver;
import com.human_pulse.datastore.StoreController;
import com.human_pulse.utilities.InternetConnectionUtility;
import com.human_pulse.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.human_pulse.constants.Constants.APP_VERSION_FLAG;
import static com.human_pulse.constants.Constants.CLOSING_TIME;
import static com.human_pulse.constants.Constants.DEFAULT_CATEGORY_ID;
import static com.human_pulse.constants.Constants.DEPARTMENT_ID;
import static com.human_pulse.constants.Constants.DEPARTMENT_NAME;
import static com.human_pulse.constants.Constants.OPENING_TIME;

/**
 * Created by anfal on 12/14/2015.
 */
public class StoreActions implements IInternetConnectionObject {
    private static StoreActions storeActionInstance = null;
    private ILog logger = null;
    private StoreMenu storeMenu = null;
    private AppCompatActivity appCompatActivity = null;
    private ProgressBar progressBar = null;
    private DrawerLayout storeDrawer = null;
    private StoreController storeController = null;
    private InternetConnectionUtility internetConnectionUtility = null;
    private LinearLayout storeContentLayout = null;

    private StoreActions(AppCompatActivity appCompatActivity) {
        logger = Logger.getLogger();
        this.appCompatActivity = appCompatActivity;
        RecyclerView departmentRecyclerView = (RecyclerView) this.appCompatActivity.findViewById(R.id.store_departments);
        this.storeMenu = new StoreMenu(departmentRecyclerView, this.appCompatActivity);
        Toolbar storeToolbar = (Toolbar) this.appCompatActivity.findViewById(R.id.store_page_toolbar);
        this.storeDrawer = (DrawerLayout) this.appCompatActivity.findViewById(R.id.store_drawer_layout);
        this.progressBar = (ProgressBar) this.appCompatActivity.findViewById(R.id.store_department_progress);
        EditText searchEditText = (EditText) this.appCompatActivity.findViewById(R.id.store_search_box);
        searchEditText.setOnEditorActionListener(new StoreSearchBoxListener());
        departmentRecyclerView.setVisibility(View.GONE);
        this.progressBar.setVisibility(View.VISIBLE);
        this.internetConnectionUtility = new InternetConnectionUtility(this.appCompatActivity);
        this.internetConnectionUtility.registerAsObserver(this);
        this.storeController = new StoreController(this.appCompatActivity);
        this.appCompatActivity.setSupportActionBar(storeToolbar);
        this.storeContentLayout = (LinearLayout) this.appCompatActivity.findViewById(R.id.store_content_layout);
        LeftNavigationObserver.setLeftNavigationObserverInstance(this.appCompatActivity, this.storeDrawer, storeToolbar);
        CartObserver.setCartObserverInstance(this.appCompatActivity);
        Bundle metaData = this.appCompatActivity.getIntent().getExtras();
        if (null != metaData) {
            this.showUpdateAppDialog(metaData.getInt(APP_VERSION_FLAG));
            String openingTime = metaData.getString(OPENING_TIME);
            String closingTime = metaData.getString(CLOSING_TIME);
            if (null != openingTime && null != closingTime) {
                CustomComponents.getInstance().showCompanyClosedDialog(
                        this.appCompatActivity,
                        openingTime,
                        closingTime
                );
            }
        }
    }

    public static StoreActions getStoreActionInstance() {
        return storeActionInstance;
    }

    public static void setStoreActionInstance(AppCompatActivity appCompatActivity) {
        if (null == storeActionInstance) {
            storeActionInstance = new StoreActions(appCompatActivity);
        }
    }

    public void destroyStoreActionInstance() {
        if (null != storeActionInstance) {
            storeActionInstance = null;
            System.gc();
        }
    }

    public void populateStoreDepartments(JSONArray items) {
        try {
            boolean isStoreMenuLoaded = this.storeMenu.instantiateStoreMenu(this.parseDepartmentNav(items));
            if (isStoreMenuLoaded) {
                this.storeMenu.getRecyclerView().setVisibility(View.VISIBLE);
                this.progressBar.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private List<StoreDepartmentDataObject> parseDepartmentNav(JSONArray departmentArray) {
        List<StoreDepartmentDataObject> storeDepartmentDataObjects = new ArrayList<>();
        try {
            for (int departmentNumber = 0; departmentNumber < departmentArray.length(); departmentNumber++) {
                JSONObject departmentObject = departmentArray.getJSONObject(departmentNumber);
                storeDepartmentDataObjects.add(
                        new StoreDepartmentDataObject(
                                departmentObject.getString(DEPARTMENT_NAME),
                                Integer.parseInt(departmentObject.getString(DEPARTMENT_ID))
                        )
                );
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
        return storeDepartmentDataObjects;
    }

    public void storeDepartmentClickListener(int position) {
        try {
            StoreDepartmentsAdapter storeDepartmentsAdapter = this.storeMenu.getStoreDepartmentsAdapter();
            StoreDepartmentDataObject storeDepartmentDataObject = storeDepartmentsAdapter.getStoreItemList().get(position);
            FlurryAgent.logEvent(
                    LogEvents.EVENT_ID_MAIN_STORE_DEPARTMENT,
                    LogEvents.getEventData(
                            LogEvents.EVENT_ID_MAIN_STORE_DEPARTMENT,
                            new String[]{
                                    LogEvents.TRIGGERED_MAIN_DEPARTMENT_LOAD_EVENT,
                                    storeDepartmentDataObject.getDepartmentName()
                            }
                    )
            );
            Utilities.getInstance().resetStaticActivityData();
            Utilities.getInstance().startDepartmentActivity(
                    this.appCompatActivity,
                    storeDepartmentDataObject.getDepartmentName(),
                    storeDepartmentDataObject.getDepartmentId(),
                    DEFAULT_CATEGORY_ID
            );
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public StoreController getStoreController() {
        return this.storeController;
    }

    public DrawerLayout getStoreDrawer() {
        return this.storeDrawer;
    }

    private void showUpdateAppDialog(int newVersionCode) {
        try {
            if (BuildConfig.VERSION_CODE < newVersionCode) {
                CustomComponents.getInstance().showNewVersionIsAvailableDialog(this.appCompatActivity).show();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void fetchStoreDepartments() {
        try {
            if (this.internetConnectionUtility.checkInternetConnectivity()) {
                this.getStoreController().getStoreDepartments();
                if (View.GONE == this.storeContentLayout.getVisibility()) {
                    this.storeContentLayout.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void startReconnecting() {
        try {
            this.progressBar.setVisibility(View.GONE);
            this.storeContentLayout.setVisibility(View.GONE);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void tryToReconnect() {
        try {
            this.progressBar.setVisibility(View.VISIBLE);
            this.fetchStoreDepartments();
            LeftNavigationObserver.getLeftNavigationObserverInstance().fetchLeftNavigation();
            CartObserver.getCartObserverInstance().instantiateCart();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void sendSearchKeywordToSearchActivity(String searchTerm) {
        try {
            Utilities.getInstance().startSearchActivityWithMetaData(this.appCompatActivity, searchTerm);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}