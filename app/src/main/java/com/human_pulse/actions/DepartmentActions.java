package com.human_pulse.actions;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;

import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.app.DepartmentPage;
import com.human_pulse.app.R;
import com.human_pulse.components.adapter.DepartmentCategoryAdapter;
import com.human_pulse.components.dataobjects.DepartmentItemDataObject;
import com.human_pulse.components.fragments.DepartmentCategoryFragment;
import com.human_pulse.components.interfaces.IInternetConnectionObject;
import com.human_pulse.components.layoutmanipulators.ItemDetailLayoutManipulator;
import com.human_pulse.components.listeners.DepartmentCategoryPagerListener;
import com.human_pulse.components.observers.CartObserver;
import com.human_pulse.components.observers.LeftNavigationObserver;
import com.human_pulse.datastore.DepartmentController;
import com.human_pulse.utilities.InternetConnectionUtility;
import com.human_pulse.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import static com.human_pulse.constants.Constants.CATEGORY_ID;
import static com.human_pulse.constants.Constants.CATEGORY_NAME;
import static com.human_pulse.constants.Constants.DEFAULT_CATEGORY_ID;
import static com.human_pulse.constants.Constants.DEPARTMENT_CATEGORY_ALL;
import static com.human_pulse.constants.Constants.DEPARTMENT_ID;
import static com.human_pulse.constants.Constants.DEPARTMENT_NAME;
import static com.human_pulse.constants.Constants.META_DATA_NOT_FOUND_EXCEPTION;
import static com.human_pulse.constants.Constants.NON_APPLICABLE_ID;

/**
 * Created by anfal on 12/19/2015.
 */
public class DepartmentActions implements IInternetConnectionObject {
    private static DepartmentActions departmentActionInstance = null;
    private ILog logger = null;
    private TabLayout departmentCategories = null;
    private AppCompatActivity appCompatActivity = null;
    private int departmentId = 0;
    private int categoryId = DEFAULT_CATEGORY_ID;
    private DrawerLayout departmentDrawer = null;
    private ViewPager viewPager = null;
    private String departmentName, categoryName, reconnectInDepartmentCategoryName = null;
    private DepartmentCategoryAdapter departmentCategoryAdapter = null;
    private DepartmentController departmentController = null;
    private ItemDetailLayoutManipulator itemDetailLayoutManipulator = null;
    private InternetConnectionUtility internetConnectionUtility = null;
    private boolean isReconnectOutsideDepartment = true;
    private ProgressBar progressBar = null;

    private DepartmentActions(AppCompatActivity appCompatActivity) {
        this.logger = Logger.getLogger();
        try {
            this.appCompatActivity = appCompatActivity;
            Bundle metaData = this.appCompatActivity.getIntent().getExtras();
            if (null == metaData) {
                throw new Exception(META_DATA_NOT_FOUND_EXCEPTION + ": " + DepartmentPage.class);
            } else {
                this.departmentDrawer = (DrawerLayout) this.appCompatActivity.findViewById(R.id.department_drawer_layout);
                Toolbar toolbar = (Toolbar) this.appCompatActivity.findViewById(R.id.department_page_toolbar);
                this.internetConnectionUtility = new InternetConnectionUtility(this.appCompatActivity);
                this.internetConnectionUtility.registerAsObserver(this);
                this.departmentController = new DepartmentController(this.appCompatActivity);
                this.appCompatActivity.setSupportActionBar(toolbar);
                LeftNavigationObserver.setLeftNavigationObserverInstance(this.appCompatActivity, this.departmentDrawer, toolbar);
                this.departmentCategories = (TabLayout) this.appCompatActivity.findViewById(R.id.department_category_tabs);
                this.departmentCategories.setTabMode(TabLayout.MODE_SCROLLABLE);
                Utilities.getInstance().configureTabLook(this.departmentCategories);
                this.setDepartmentId(metaData.getInt(DEPARTMENT_ID));
                this.setDepartmentController(departmentController);
                this.setCategoryId(metaData.getInt(CATEGORY_ID));
                this.setDepartmentName(metaData.getString(DEPARTMENT_NAME));
                this.viewPager = (ViewPager) this.appCompatActivity.findViewById(R.id.department_category_pager);
                this.progressBar = (ProgressBar) this.appCompatActivity.findViewById(R.id.department_page_progress);
                this.viewPager.addOnPageChangeListener(new DepartmentCategoryPagerListener());
                this.itemDetailLayoutManipulator = new ItemDetailLayoutManipulator(this.appCompatActivity);
                CartObserver.setCartObserverInstance(this.appCompatActivity);
            }

        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public static DepartmentActions getDepartmentActionInstance() {
        return departmentActionInstance;
    }

    public static void setDepartmentActionInstance(AppCompatActivity appCompatActivity) {
        if (null == departmentActionInstance) {
            departmentActionInstance = new DepartmentActions(appCompatActivity);
        }
    }

    public void destroyDepartmentActionInstance() {
        if (null != departmentActionInstance) {
            departmentActionInstance = null;
            System.gc();
        }
    }

    public void setDepartmentCategories(JSONArray categoryArray) {
        try {
            DepartmentCategoryAdapter departmentCategoryAdapter = this.populateCategories(categoryArray);
            this.progressBar.setVisibility(View.GONE);
            this.setDepartmentCategoryAdapter(departmentCategoryAdapter);
            this.viewPager.setAdapter(this.getDepartmentCategoryAdapter());
            this.departmentCategories.setupWithViewPager(this.viewPager);
            this.setActionBarDepartmentName(this.departmentName);
            this.selectCurrentlyActiveTab(null);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private DepartmentCategoryAdapter populateCategories(JSONArray categoryArray) {
        DepartmentCategoryAdapter departmentCategoryAdapter = new DepartmentCategoryAdapter(this.appCompatActivity.getSupportFragmentManager());
        try {
            DepartmentCategoryFragment departmentCategoryFragment = new DepartmentCategoryFragment();
            departmentCategoryFragment.setCategoryId(DEFAULT_CATEGORY_ID);
            departmentCategoryFragment.setDepartmentId(this.getDepartmentId());
            departmentCategoryAdapter.addFragment(departmentCategoryFragment, DEPARTMENT_CATEGORY_ALL);
            for (int categoryNumber = 0; categoryNumber < categoryArray.length(); categoryNumber++) {
                JSONObject categoryObject = categoryArray.getJSONObject(categoryNumber);
                int newCatId = Integer.parseInt(categoryObject.getString(CATEGORY_ID));
                String newCatName = categoryObject.getString(CATEGORY_NAME);
                departmentCategoryFragment = new DepartmentCategoryFragment();
                departmentCategoryFragment.setCategoryId(newCatId);
                departmentCategoryFragment.setDepartmentId(this.getDepartmentId());
                departmentCategoryAdapter.addFragment(departmentCategoryFragment, newCatName);
                this.fillInMissingInfo(newCatName, newCatId);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
        return departmentCategoryAdapter;
    }

    private void fillInMissingInfo(String newCategoryName, int newCategoryId) throws Exception {
        if (com.human_pulse.constants.Constants.checkForStaticData) {
            if (com.human_pulse.constants.Constants.categoryName.toLowerCase().equals(newCategoryName.toLowerCase())) {
                this.setCategoryId(newCategoryId);
                this.setCategoryName(newCategoryName);
                com.human_pulse.constants.Constants.checkForStaticData = false;
            }
        } else {
            if (null == this.reconnectInDepartmentCategoryName) {
                if (DEFAULT_CATEGORY_ID != this.getCategoryId()) {
                    if (this.getCategoryId() == newCategoryId) {
                        this.setCategoryName(newCategoryName);
                    }
                }
            } else {
                if (this.reconnectInDepartmentCategoryName.toLowerCase().equals(newCategoryName.toLowerCase())) {
                    this.setCategoryName(this.reconnectInDepartmentCategoryName);
                    this.setCategoryId(newCategoryId);
                    this.reconnectInDepartmentCategoryName = null;
                }
            }
        }
    }

    public void selectCurrentlyActiveTab(String inDepartmentCategoryName) {
        List<String> departmentCategoryFragments = this.getDepartmentCategoryAdapter().getCategoryTitles();
        if (null == inDepartmentCategoryName) {
            if (DEFAULT_CATEGORY_ID != this.getCategoryId()) {
                for (int i = 0; i < departmentCategoryFragments.size(); i++) {
                    if (this.getCategoryName().toLowerCase().equals(departmentCategoryFragments.get(i).toLowerCase())) {
                        this.viewPager.setCurrentItem(i);
                    }
                }
            }
        } else {
            for (int i = 0; i < departmentCategoryFragments.size(); i++) {
                if (inDepartmentCategoryName.toLowerCase().equals(departmentCategoryFragments.get(i).toLowerCase())) {
                    this.viewPager.setCurrentItem(i);
                }
            }
        }
        this.getDepartmentDrawer().closeDrawer(Gravity.LEFT);
    }

    public int getDepartmentId() {
        return this.departmentId;
    }

    public void setDepartmentId(int id) {
        this.departmentId = id;
        com.human_pulse.constants.Constants.departmentId = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    private void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
        com.human_pulse.constants.Constants.categoryId = categoryId;
    }

    private void setActionBarDepartmentName(String departmentName) {
        if (null != this.appCompatActivity.getSupportActionBar()) {
            this.appCompatActivity.getSupportActionBar().setTitle(departmentName);
        }
    }

    private void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
        com.human_pulse.constants.Constants.departmentName = departmentName;
    }

    public DepartmentController getDepartmentController() {
        return this.departmentController;
    }

    public void setDepartmentController(DepartmentController departmentController) {
        this.departmentController = departmentController;
    }

    public void fetchCategoryData(String departmentName, int departmentId, int categoryId) {
        try {
            if (null != departmentName) {
                this.setDepartmentName(departmentName);
            }
            if (NON_APPLICABLE_ID != departmentId) {
                this.setDepartmentId(departmentId);
            }
            if (NON_APPLICABLE_ID != categoryId) {
                this.setCategoryId(categoryId);
            }
            if (this.internetConnectionUtility.checkInternetConnectivity()) {
                this.getDepartmentController().getDepartmentCategories(this.getDepartmentId());
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public DepartmentCategoryAdapter getDepartmentCategoryAdapter() {
        return departmentCategoryAdapter;
    }

    public void setDepartmentCategoryAdapter(DepartmentCategoryAdapter departmentCategoryAdapter) {
        this.departmentCategoryAdapter = departmentCategoryAdapter;
    }

    public DrawerLayout getDepartmentDrawer() {
        return this.departmentDrawer;
    }

    public String getCategoryName() {
        return this.categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public void revealItemDetailLayout(DepartmentItemDataObject departmentItemDataObject, View itemView) {
        try {
            this.itemDetailLayoutManipulator.revealItemDetailLayout(departmentItemDataObject, itemView);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public ItemDetailLayoutManipulator getItemDetailLayoutManipulator() {
        return itemDetailLayoutManipulator;
    }

    @Override
    public void startReconnecting() {
        try {
            this.viewPager.removeAllViews();
            this.progressBar.setVisibility(View.GONE);
            if (this.isReconnectOutsideDepartment) {
                this.departmentCategories.removeAllTabs();
                this.setActionBarDepartmentName(this.appCompatActivity.getString(R.string.text_department));
            } else {
                this.isReconnectOutsideDepartment = true;
            }
            if (this.departmentDrawer.isDrawerOpen(Gravity.LEFT)) {
                this.departmentDrawer.closeDrawer(Gravity.LEFT);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void tryToReconnect() {
        try {
            this.progressBar.setVisibility(View.VISIBLE);
            this.fetchCategoryData(this.departmentName, this.getDepartmentId(), this.getCategoryId());
            LeftNavigationObserver.getLeftNavigationObserverInstance().fetchLeftNavigation();
            CartObserver.getCartObserverInstance().instantiateCart();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void checkInternetConnectivityAgain(int position) {
        try {
            String categoryName = this.departmentCategories.getTabAt(position).getText().toString();
            com.human_pulse.constants.Constants.categoryName = categoryName;
            if (Utilities.getInstance().isInternetConnectionNotAvailable(this.appCompatActivity)) {
                this.isReconnectOutsideDepartment = false;
            }
            if (!this.internetConnectionUtility.checkInternetConnectivity()) {
                com.human_pulse.constants.Constants.checkForStaticData = false;
                this.reconnectInDepartmentCategoryName = categoryName;
            } else {
                com.human_pulse.constants.Constants.checkForStaticData = true;
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
