package com.human_pulse.actions;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.human_pulse.analytics.LogEvents;
import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.app.R;
import com.human_pulse.app.StorePage;
import com.human_pulse.components.CustomComponents;
import com.human_pulse.components.dataobjects.AddressAdditionalDataObject;
import com.human_pulse.components.dataobjects.CartItemDataObject;
import com.human_pulse.components.dataobjects.DeliveryChargesInfoDataObject;
import com.human_pulse.components.interfaces.IAddressResponseSenderReceiver;
import com.human_pulse.components.interfaces.IInternetConnectionObject;
import com.human_pulse.components.interfaces.IPhoneNumberVerificationInterface;
import com.human_pulse.components.listeners.CheckoutPageButtonListener;
import com.human_pulse.components.listeners.CheckoutPhoneNumberListener;
import com.human_pulse.components.listeners.MyAccountVerifyPhoneNumberListener;
import com.human_pulse.components.listeners.NewAddressListener;
import com.human_pulse.components.listeners.ResetPhoneNumberListener;
import com.human_pulse.components.listeners.WantToEnterCouponCodeListener;
import com.human_pulse.components.observers.LeftNavigationObserver;
import com.human_pulse.components.tasks.PostCheckoutCleanUpTask;
import com.human_pulse.components.viewholder.AddressHolder;
import com.human_pulse.database.ClientDB;
import com.human_pulse.datastore.CheckoutPageController;
import com.human_pulse.enumerations.EAddressManipulationChoice;
import com.human_pulse.utilities.InternetConnectionUtility;
import com.human_pulse.utilities.ItemCheckoutUtility;
import com.human_pulse.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.human_pulse.analytics.logging.Constants.TAG_ERROR;
import static com.human_pulse.constants.Constants.ADDRESSE_LABELS;
import static com.human_pulse.constants.Constants.ADDRESS_LABEL;
import static com.human_pulse.constants.Constants.ADDRESS_PARAM;
import static com.human_pulse.constants.Constants.COUPON_CODE;
import static com.human_pulse.constants.Constants.COUPON_DEDUCTION;
import static com.human_pulse.constants.Constants.CUSTOM_ORDER;
import static com.human_pulse.constants.Constants.DELIVERY_ADDRESS;
import static com.human_pulse.constants.Constants.EMAIL;
import static com.human_pulse.constants.Constants.EMAIL_PARAM;
import static com.human_pulse.constants.Constants.ERROR;
import static com.human_pulse.constants.Constants.IS_MOBILE_NUMBER_CONFIRMED;
import static com.human_pulse.constants.Constants.ITEMS_TO_CHECKOUT;
import static com.human_pulse.constants.Constants.ITEM_DISCOUNT;
import static com.human_pulse.constants.Constants.ITEM_ID;
import static com.human_pulse.constants.Constants.ITEM_NAME;
import static com.human_pulse.constants.Constants.ITEM_PRICE;
import static com.human_pulse.constants.Constants.ITEM_QUANTITY_TO_CHECKOUT;
import static com.human_pulse.constants.Constants.ITEM_WEIGHT_OR_QUANTITY;
import static com.human_pulse.constants.Constants.MESSAGE;
import static com.human_pulse.constants.Constants.MOBILE_NUMBER;
import static com.human_pulse.constants.Constants.MOBILE_NUMBER_PARAM;
import static com.human_pulse.constants.Constants.NEW_ADDRESS;
import static com.human_pulse.constants.Constants.OH_DELIVERY_CHARGES;
import static com.human_pulse.constants.Constants.OH_ITEM_CHARGED_PRICE;
import static com.human_pulse.constants.Constants.OH_ITEM_QUANTITY;
import static com.human_pulse.constants.Constants.STATUS;
import static com.human_pulse.constants.Constants.SUCCESS;

/**
 * Created by anfal on 1/27/2016.
 */
public class CheckoutPageActions implements IAddressResponseSenderReceiver, IPhoneNumberVerificationInterface, IInternetConnectionObject {

    private static CheckoutPageActions checkoutPageActionsInstance = null;
    private final int FORWARD = 1;
    private final int BACKWARD = 0;
    private final int THANKS_FOR_SHOPPING = -1;
    private ILog logger = null;
    private AppCompatActivity appCompatActivity = null;
    private ScrollView preCheckoutInfo = null;
    private FrameLayout preCheckoutInvoiceReview = null;
    private Button checkoutForwardButton, checkoutBackwardButton = null;
    private CheckoutPageController checkoutPageController = null;
    private RelativeLayout checkoutContainerBody = null;
    private CardView mobileNumberCard, couponCodeCard = null;
    private ClientDB clientDB = null;
    private Spinner addressSpinner = null;
    private ProgressBar progressBar = null;
    private ArrayAdapter<String> addressSpinnerAdapter = null;
    private TextView deliveryAddressText, orderCost, deliveryCharges, grandTotal, resetPhoneNumber, phoneNumber, couponDeduction = null;
    private LinearLayout itemDetailLayout, itemReviewLayout = null;
    private EditText customOrderBox = null;
    private int deliverCharges = 0;
    private String addressForCheckout = null;
    private RelativeLayout thankYouForShopping = null;
    private InternetConnectionUtility internetConnectionUtility = null;
    private EditText couponCode = null;
    private TextInputLayout couponCodeTextInput = null;
    private AppCompatCheckBox wantToEnterCouponCode = null;
    private NewAddressRequestAction newAddressRequestAction = null;
    private boolean isCustomOrder = false;
    private String couponCodeBeforeInfoFetch, addressLabel = null;

    private CheckoutPageActions(AppCompatActivity appCompatActivity) {
        this.logger = Logger.getLogger();

        try {
            this.appCompatActivity = appCompatActivity;
            Toolbar toolbar = (Toolbar) this.appCompatActivity.findViewById(R.id.checkout_page_toolbar);
            this.appCompatActivity.setSupportActionBar(toolbar);
            this.clientDB = new ClientDB(this.appCompatActivity);
            DrawerLayout drawerLayout = (DrawerLayout) this.appCompatActivity.findViewById(R.id.checkout_page_drawer_layout);
            LeftNavigationObserver.setLeftNavigationObserverInstance(this.appCompatActivity, drawerLayout, toolbar);
            this.preCheckoutInfo = (ScrollView) this.appCompatActivity.findViewById(R.id.pre_checkout_info_layout);
            this.preCheckoutInvoiceReview = (FrameLayout) this.appCompatActivity.findViewById(R.id.pre_checkout_review_invoice_layout);
            this.checkoutBackwardButton = (Button) this.appCompatActivity.findViewById(R.id.checkout_backward_button);
            this.checkoutForwardButton = (Button) this.appCompatActivity.findViewById(R.id.checkout_forward_button);
            this.progressBar = (ProgressBar) this.appCompatActivity.findViewById(R.id.checkout_page_progress);
            this.couponCodeTextInput = (TextInputLayout) this.appCompatActivity.findViewById(R.id.coupon_code_input_layout_email);
            this.checkoutBackwardButton.setOnClickListener(new CheckoutPageButtonListener());
            this.checkoutForwardButton.setOnClickListener(new CheckoutPageButtonListener());
            this.checkoutPageController = new CheckoutPageController(this.appCompatActivity);
            this.internetConnectionUtility = new InternetConnectionUtility(this.appCompatActivity);
            this.internetConnectionUtility.registerAsObserver(this);
            this.fetchPreCheckoutInfo();
            this.addressSpinner = (Spinner) this.appCompatActivity.findViewById(R.id.pre_defined_address_label_spinner);
            this.phoneNumber = (TextView) this.appCompatActivity.findViewById(R.id.enter_phone_number_link);
            this.couponDeduction = (TextView) this.appCompatActivity.findViewById(R.id.oh_det_coupon_deduction);
            this.mobileNumberCard = (CardView) this.appCompatActivity.findViewById(R.id.mobile_number_prompt_card);
            this.couponCodeCard = (CardView) this.appCompatActivity.findViewById(R.id.coupon_code_prompt_card);
            this.couponCode = (EditText) this.appCompatActivity.findViewById(R.id.coupon_code);
            this.wantToEnterCouponCode = (AppCompatCheckBox) this.appCompatActivity.findViewById(R.id.want_to_enter_coupon_code_check);
            this.checkoutContainerBody = (RelativeLayout) this.appCompatActivity.findViewById(R.id.checkout_container_body);
            CardView addNewAddressCard = (CardView) this.appCompatActivity.findViewById(R.id.add_new_address_card);
            addNewAddressCard.setOnClickListener(new NewAddressListener(this));
            this.deliveryAddressText = (TextView) this.appCompatActivity.findViewById(R.id.delivery_address_text);
            this.itemDetailLayout = (LinearLayout) this.appCompatActivity.findViewById(R.id.checkout_item_review_info);
            this.itemReviewLayout = (LinearLayout) this.appCompatActivity.findViewById(R.id.conventional_order_review);
            this.customOrderBox = (EditText) this.appCompatActivity.findViewById(R.id.custom_order_box);
            this.orderCost = (TextView) this.appCompatActivity.findViewById(R.id.oh_det_order_cost);
            this.deliveryCharges = (TextView) this.appCompatActivity.findViewById(R.id.oh_det_delivery_charges);
            this.grandTotal = (TextView) this.appCompatActivity.findViewById(R.id.oh_det_grand_total);
            this.thankYouForShopping = (RelativeLayout) this.appCompatActivity.findViewById(R.id.thanks_bye_bye);
            this.resetPhoneNumber = (TextView) this.appCompatActivity.findViewById(R.id.reset_phone_number_link);
            this.resetPhoneNumber.setOnClickListener(new ResetPhoneNumberListener());
            Utilities.getInstance().decorateHyperLink(this.resetPhoneNumber);
            this.newAddressRequestAction = new NewAddressRequestAction(this.appCompatActivity);
            Bundle metaData = this.appCompatActivity.getIntent().getExtras();
            if (null != metaData) {
                this.isCustomOrder = metaData.getBoolean(CUSTOM_ORDER);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public static CheckoutPageActions getCheckoutPageActionsInstance() {
        return checkoutPageActionsInstance;
    }

    public static void setCheckoutPageActionsInstance(AppCompatActivity appCompatActivity) {
        if (null == checkoutPageActionsInstance) {
            checkoutPageActionsInstance = new CheckoutPageActions(appCompatActivity);
        }
    }

    public void destroyCheckoutPageActionsInstance() {
        if (null != checkoutPageActionsInstance) {
            checkoutPageActionsInstance = null;
            System.gc();
        }
    }

    public void checkoutButtonListenEvent(String textOnButton) {
        try {
            if (textOnButton.equals(this.appCompatActivity.getString(R.string.next_text))) {
                if (this.checkIfUserIsOkToProceed()) {
                    this.progressBar.setVisibility(View.VISIBLE);
                    this.initiateInvoiceInfoFetch();
                } else {
                    Toast.makeText(this.appCompatActivity, this.appCompatActivity.getString(R.string.alert_incomplete_information), Toast.LENGTH_LONG).show();
                }
            } else if (textOnButton.equals(this.appCompatActivity.getString(R.string.cancel_text))) {
                Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, StorePage.class);
            } else if (textOnButton.equals(this.appCompatActivity.getString(R.string.checkout_button_text))) {
                this.initiateCheckout();
            } else if (textOnButton.equals(this.appCompatActivity.getString(R.string.back_text))) {
                this.fadeInLayout(BACKWARD);
                this.checkoutBackwardButton.setText(this.appCompatActivity.getString(R.string.cancel_text));
                this.checkoutForwardButton.setText(this.appCompatActivity.getString(R.string.next_text));
                this.itemDetailLayout.removeAllViews();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private void fadeInLayout(int direction) {
        try {
            Animation fadeInAnimation = AnimationUtils.loadAnimation(this.appCompatActivity, android.R.anim.fade_in);
            switch (direction) {
                case FORWARD:
                    this.checkIfCustomOrder();
                    this.preCheckoutInvoiceReview.setVisibility(View.VISIBLE);
                    this.preCheckoutInvoiceReview.startAnimation(fadeInAnimation);
                    this.preCheckoutInfo.setVisibility(View.GONE);
                    break;
                case BACKWARD:
                    this.preCheckoutInfo.setVisibility(View.VISIBLE);
                    this.preCheckoutInfo.setAnimation(fadeInAnimation);
                    this.preCheckoutInvoiceReview.setVisibility(View.GONE);
                    break;
                case THANKS_FOR_SHOPPING:
                    this.thankYouForShopping.setVisibility(View.VISIBLE);
                    this.thankYouForShopping.setAnimation(fadeInAnimation);
                    this.preCheckoutInvoiceReview.setVisibility(View.GONE);
                    this.checkoutForwardButton.setEnabled(false);
                    this.checkoutBackwardButton.setEnabled(false);
                    break;
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private void populateAddressSpinner(JSONArray addressArray) throws Exception {
        List<String> addressList = new ArrayList<>();
        String defaultAddressString = this.appCompatActivity.getString(R.string.choose_an_existing_address);
        addressList.add(defaultAddressString);
        for (int i = 0; i < addressArray.length(); i++) {
            JSONObject jsonObject = addressArray.getJSONObject(i);
            addressList.add(jsonObject.getString(ADDRESS_LABEL));
        }
        this.addressSpinnerAdapter = new ArrayAdapter<>(this.appCompatActivity, R.layout.simple_spinner_item, addressList);
        this.addressSpinner.setAdapter(this.addressSpinnerAdapter);
    }

    public void fetchPreCheckoutInfo() {
        try {
            if (this.internetConnectionUtility.checkInternetConnectivity()) {
                this.checkoutPageController.getPreCheckoutData(this.clientDB.getUserEmail());
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private void setUpVerificationCodeString() throws Exception {
        this.phoneNumber.setText(this.appCompatActivity.getString(R.string.verify_number_text));
        this.phoneNumber.setOnClickListener(new MyAccountVerifyPhoneNumberListener(this));
        Utilities.getInstance().decorateHyperLink(this.phoneNumber);
    }

    private void setUpPhoneNumberString() throws Exception {
        this.phoneNumber.setText(this.appCompatActivity.getString(R.string.enter_phone_number_text));
        this.phoneNumber.setOnClickListener(new CheckoutPhoneNumberListener());
        Utilities.getInstance().decorateHyperLink(this.phoneNumber);
    }

    private void setMobileNumberCard(String mobileNumber, int isConfirmed) throws Exception {
        if (mobileNumber.equals("null")) {
            this.setUpPhoneNumberString();
            this.mobileNumberCard.setVisibility(View.VISIBLE);
        } else if (ERROR == isConfirmed) {
            this.setUpVerificationCodeString();
            this.resetPhoneNumber.setVisibility(View.VISIBLE);
            this.mobileNumberCard.setVisibility(View.VISIBLE);
        }
    }

    public void populatePreCheckoutData(JSONObject response) {
        try {
            JSONArray addressArray = response.getJSONArray(ADDRESSE_LABELS);
            this.populateAddressSpinner(addressArray);
            this.setMobileNumberCard(response.getString(MOBILE_NUMBER), response.getInt(IS_MOBILE_NUMBER_CONFIRMED));
            //This value is string instead of integer because in db consts have varchar column type.
            this.setUpCouponCodeCard(response.getString("SHOW_COUPON_BOX"));
            this.checkoutContainerBody.setVisibility(View.VISIBLE);
            this.progressBar.setVisibility(View.GONE);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private void setUpCouponCodeCard(String showCouponBox) {
        try {
            if (showCouponBox.equals("1")) {
                this.couponCodeCard.setVisibility(View.VISIBLE);
                this.wantToEnterCouponCode.setOnClickListener(new WantToEnterCouponCodeListener());
            } else {
                this.couponCodeCard.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private boolean checkIfUserIsOkToProceed() {
        boolean isOkToProceed = false;
        try {
            String defaultAddressString = this.appCompatActivity.getString(R.string.choose_an_existing_address);
            boolean isDefaultAddressString = this.addressSpinner.getSelectedItem().equals(defaultAddressString);
            boolean isMobileNumberVerified = this.mobileNumberCard.getVisibility() == View.GONE;
            if (!isDefaultAddressString && isMobileNumberVerified) {
                isOkToProceed = true;
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
        return isOkToProceed;
    }


    @Override
    public void fetchDataForAddressDialog(EAddressManipulationChoice eAddressManipulationChoice, AddressHolder addressHolder) {
        try {
            this.progressBar.setVisibility(View.VISIBLE);
            this.newAddressRequestAction.getAddressRequiredData(eAddressManipulationChoice, this);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void openNewAddressDialog() {
        try {
            this.fetchDataForAddressDialog(EAddressManipulationChoice.ADD, null);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void openNewAddressDialog(EAddressManipulationChoice addressManipulationChoice, JSONObject response) {
        try {
            AddressAdditionalDataObject addressAdditionalDataObject = Utilities.getInstance().parseAdditionalAddressInfo(response);
            this.progressBar.setVisibility(View.GONE);
            CustomComponents.getInstance().newAddressDialog(this.appCompatActivity, this, addressAdditionalDataObject);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void sendNewAddressRequest(String label, String address, String city) {
        try {
            if (!label.equals("") && !address.equals("") && !city.equals("")) {
                this.progressBar.setVisibility(View.VISIBLE);
                String email = this.clientDB.getUserEmail();
                this.newAddressRequestAction.sendNewAddressRequest(this, label, address, city, email);
            } else {
                Toast.makeText(this.appCompatActivity, this.appCompatActivity.getString(R.string.incomplete_info_cannot_proceed), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void receiveNewAddressResponse(JSONObject response) {
        try {
            this.progressBar.setVisibility(View.GONE);
            if (SUCCESS == Integer.parseInt(response.getString(STATUS))) {
                JSONObject jsonObject = response.getJSONObject(NEW_ADDRESS);
                String addressLabel = jsonObject.getString(ADDRESS_LABEL);
                this.addressSpinnerAdapter.add(addressLabel);
                this.addressSpinner.setSelection(this.addressSpinnerAdapter.getPosition(addressLabel), false);
                FlurryAgent.logEvent(
                        LogEvents.EVENT_ID_NEW_ADDRESS_ADDITION,
                        LogEvents.getEventData(
                                LogEvents.EVENT_ID_NEW_ADDRESS_ADDITION,
                                new String[]{
                                        LogEvents.TRIGGERED_ADDRESS_ADDITION_CHECKOUT
                                }
                        )
                );
            } else {
                Toast.makeText(this.appCompatActivity, response.getString(MESSAGE), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void showPhoneNumerDialog() {
        try {
            CustomComponents.getInstance().enterPhoneNumberDialog(this.appCompatActivity);
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void sendNewPhoneNumberRequest(String phoneNumber) {
        try {
            if (!phoneNumber.equals("")) {
                this.progressBar.setVisibility(View.VISIBLE);
                HashMap<String, String> newAddressInfo = new HashMap<>();
                String email = this.clientDB.getUserEmail();
                newAddressInfo.put(EMAIL_PARAM, email);
                newAddressInfo.put(MOBILE_NUMBER_PARAM, phoneNumber);
                this.checkoutPageController.sendPhoneNumberRequest(newAddressInfo);
            } else {
                Toast.makeText(this.appCompatActivity, this.appCompatActivity.getString(R.string.incomplete_info_cannot_proceed), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void postPhoneNumberAdditionTasks(JSONObject jsonObject) {
        try {
            //Remove mobile number card visibility and uncomment above two lines to return previous sms check functionality.
            /*this.setUpVerificationCodeString();
            this.resetPhoneNumber.setVisibility(View.VISIBLE);*/
            this.mobileNumberCard.setVisibility(View.GONE);
            this.progressBar.setVisibility(View.GONE);
            //Toast.makeText(this.appCompatActivity, jsonObject.getString(MESSAGE), Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void handleVerifyPhoneNumberLinkClick() {
        try {
            CustomComponents.getInstance().verifyPhoneNumberDialog(this.appCompatActivity, this);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    @Override
    public void handlePhoneNumberVerification(String verificationCode) {
        try {
            if (!verificationCode.equals("")) {
                this.progressBar.setVisibility(View.VISIBLE);
                FlurryAgent.logEvent(
                        LogEvents.EVENT_ID_NUMBER_VERIFICATION,
                        LogEvents.getEventData(
                                LogEvents.EVENT_ID_NUMBER_VERIFICATION,
                                new String[]{
                                        LogEvents.TRIGGERED_NUMBER_VERIFICATION_CHECKOUT
                                }
                        )
                );
                String email = this.clientDB.getUserEmail();
                PhoneNumberVerificationAction phoneNumberVerificationAction = new PhoneNumberVerificationAction();
                phoneNumberVerificationAction.sendVerficationCodeConfirmationRequest(this.appCompatActivity, this, verificationCode, email);
            } else {
                Toast.makeText(this.appCompatActivity, this.appCompatActivity.getString(R.string.incomplete_info_cannot_proceed), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    @Override
    public void postVerificationChores(JSONObject jsonObject) {
        try {
            this.mobileNumberCard.setVisibility(View.GONE);
            if (SUCCESS == Integer.parseInt(jsonObject.getString(STATUS))) {
                this.progressBar.setVisibility(View.GONE);
            }
            Toast.makeText(this.appCompatActivity, jsonObject.getString(MESSAGE), Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public void initiateInvoiceInfoFetch() {
        try {
            String email = this.clientDB.getUserEmail();
            this.addressLabel = (String) this.addressSpinner.getSelectedItem();
            this.couponCodeBeforeInfoFetch = this.couponCode.getText().toString();
            if (this.isCustomOrder) {
                this.moveToReview();
            } else {
                int orderCost = ItemCheckoutUtility.getInstance().getCheckoutPriceWithoutDeliveryCharges(this.clientDB.getOrderHistoryItemsFromDB());
                this.checkoutPageController.fetchInvoiceReviewData(email, this.addressLabel, this.couponCodeBeforeInfoFetch, orderCost);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public void loadInvoiceReviewData(JSONObject response) {
        try {
            this.addressForCheckout = response.getString(DELIVERY_ADDRESS);
            this.deliveryAddressText.setText(this.addressForCheckout);
            this.populateItemDetailLayoutFromDB();
            this.setBillingInfo(response.get(COUPON_DEDUCTION));
            this.moveToReview();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    private void moveToReview() {
        this.checkoutBackwardButton.setText(this.appCompatActivity.getString(R.string.back_text));
        this.checkoutForwardButton.setText(this.appCompatActivity.getString(R.string.checkout_button_text));
        this.progressBar.setVisibility(View.GONE);
        this.fadeInLayout(FORWARD);
    }

    private void setBillingInfo(Object couponDeduction) {
        try {
            DeliveryChargesInfoDataObject deliveryChargesInfoDataObject = this.clientDB.getDeliveryChargesInfo();
            int orderCost = ItemCheckoutUtility.getInstance().getCheckoutPriceWithoutDeliveryCharges(this.clientDB.getOrderHistoryItemsFromDB());
            if (deliveryChargesInfoDataObject.getThresholdPrice() > orderCost) {
                this.deliverCharges = deliveryChargesInfoDataObject.getChargedPrice();
            }

            this.orderCost.setText(
                    this.appCompatActivity.getString(
                            R.string.order_cost_text_template,
                            orderCost
                    )
            );

            this.deliveryCharges.setText(
                    this.appCompatActivity.getString(
                            R.string.delivery_charges_text_template,
                            this.deliverCharges
                    )
            );
            if (couponDeduction instanceof String) {
                String stringCouponDeduction = (String) couponDeduction;
                if (!stringCouponDeduction.equals("")) {
                    this.couponDeduction.setText(stringCouponDeduction);
                    this.couponDeduction.setVisibility(View.VISIBLE);
                } else {
                    this.couponDeduction.setVisibility(View.GONE);
                }
                this.grandTotal.setText(
                        this.appCompatActivity.getString(
                                R.string.grand_total_text_template,
                                orderCost + this.deliverCharges
                        )
                );
            } else if (couponDeduction instanceof Integer) {
                Integer integerCouponDeduction = (Integer) couponDeduction;
                this.couponDeduction.setText(
                        this.appCompatActivity.getString(
                                R.string.coupon_deduction_text_template,
                                integerCouponDeduction
                        )
                );
                this.couponDeduction.setVisibility(View.VISIBLE);
                Integer finalCost = orderCost + this.deliverCharges - integerCouponDeduction;
                this.grandTotal.setText(
                        this.appCompatActivity.getString(
                                R.string.grand_total_text_template,
                                finalCost
                        )
                );
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    private void populateItemDetailLayoutFromDB() {
        try {
            List<CartItemDataObject> cartItemDataObjects = this.clientDB.getOrderHistoryItemsFromDB();

            ArrayList<HashMap<String, String>> itemList = new ArrayList<>();
            for (CartItemDataObject cartItemDataObject : cartItemDataObjects) {
                HashMap<String, String> item = new HashMap<>();
                item.put(
                        OH_ITEM_QUANTITY,
                        this.appCompatActivity.getString(
                                R.string.order_item_quantity_name_template,
                                cartItemDataObject.getQuantityToCheckout(),
                                cartItemDataObject.getItemName()
                        )
                );
                item.put(
                        OH_ITEM_CHARGED_PRICE,
                        this.appCompatActivity.getString(
                                R.string.currency,
                                ItemCheckoutUtility.getInstance().getItemPriceBasedOnQuantity(cartItemDataObject.getItemPrice(), cartItemDataObject.getQuantityToCheckout(), cartItemDataObject.getItemDiscount())
                        )
                );
                itemList.add(item);
            }
            ListAdapter itemInfoListAdapter = new SimpleAdapter(
                    this.appCompatActivity,
                    itemList,
                    R.layout.order_history_detail_item_row,
                    new String[]{OH_ITEM_QUANTITY, OH_ITEM_CHARGED_PRICE},
                    new int[]{R.id.oh_det_item_description, R.id.oh_det_item_price}
            );
            for (int i = 0; i < itemInfoListAdapter.getCount(); i++) {
                this.itemDetailLayout.addView(itemInfoListAdapter.getView(i, null, null));
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    private void initiateCheckout() {
        try {
            if (this.isCustomOrder) {
                String customOrderString = this.customOrderBox.getText().toString();
                if (!customOrderString.equals("")) {
                    this.progressBar.setVisibility(View.VISIBLE);
                    HashMap<String, String> customOrder = new HashMap<>();
                    customOrder.put(ADDRESS_LABEL, this.addressLabel);
                    customOrder.put(EMAIL, this.clientDB.getUserEmail());
                    customOrder.put(COUPON_CODE, this.couponCodeBeforeInfoFetch);
                    customOrder.put(CUSTOM_ORDER, customOrderString);
                    this.checkoutPageController.sendCustomOrder(customOrder);
                } else {
                    Toast.makeText(this.appCompatActivity, R.string.custom_order_cannot_be_empty_text, Toast.LENGTH_SHORT).show();
                }
            } else {
                this.progressBar.setVisibility(View.VISIBLE);
                JSONArray jsonArray = this.createJSONFromList(this.clientDB.getOrderHistoryItemsFromDB());
                HashMap<String, Object> checkoutItems = new HashMap<>();
                checkoutItems.put(ITEMS_TO_CHECKOUT, jsonArray);
                checkoutItems.put(EMAIL, this.clientDB.getUserEmail());
                checkoutItems.put(OH_DELIVERY_CHARGES, this.deliverCharges);
                checkoutItems.put(ADDRESS_PARAM, this.addressForCheckout);
                checkoutItems.put(COUPON_CODE, this.couponCode.getText().toString());
                this.checkoutPageController.sendCheckoutItems(checkoutItems);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    private JSONArray createJSONFromList(List<CartItemDataObject> cartItemDataObjects) throws Exception {
        JSONArray jsonArray = new JSONArray();
        for (CartItemDataObject cartItemDataObject : cartItemDataObjects) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ITEM_NAME, cartItemDataObject.getItemName());
            jsonObject.put(ITEM_WEIGHT_OR_QUANTITY, cartItemDataObject.getItemWeightOrQuantity());
            jsonObject.put(ITEM_ID, cartItemDataObject.getItemId());
            jsonObject.put(ITEM_QUANTITY_TO_CHECKOUT, cartItemDataObject.getQuantityToCheckout());
            jsonObject.put(ITEM_PRICE, cartItemDataObject.getItemPrice());
            jsonObject.put(ITEM_DISCOUNT, cartItemDataObject.getItemDiscount());
            jsonObject.put(ITEM_WEIGHT_OR_QUANTITY, cartItemDataObject.getItemWeightOrQuantity());
            jsonArray.put(jsonObject);
        }
        return jsonArray;
    }

    public void cleanUpAfterCheckout(JSONObject response) {
        try {
            if (SUCCESS == Integer.parseInt(response.getString(STATUS))) {
                this.progressBar.setVisibility(View.GONE);
                this.fadeInLayout(THANKS_FOR_SHOPPING);
                PostCheckoutCleanUpTask postCheckoutCleanUpTask = new PostCheckoutCleanUpTask(this.clientDB);
                postCheckoutCleanUpTask.execute();
            } else {
                this.progressBar.setVisibility(View.GONE);
                Toast.makeText(this.appCompatActivity, response.getString(MESSAGE), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public void goBackToStore() {
        try {
            Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, StorePage.class);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public void handleResetPhoneNumberClick() {
        try {
            this.setUpPhoneNumberString();
            this.resetPhoneNumber.setVisibility(View.GONE);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    @Override
    public void startReconnecting() {
        try {
            this.progressBar.setVisibility(View.GONE);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    @Override
    public void tryToReconnect() {
        try {
            this.progressBar.setVisibility(View.VISIBLE);
            LeftNavigationObserver.getLeftNavigationObserverInstance().fetchLeftNavigation();
            this.fetchPreCheckoutInfo();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }

    public void toggleCouponCodeLayoutVisibility() {
        Animation animation;
        if (this.wantToEnterCouponCode.isChecked()) {
            animation = AnimationUtils.loadAnimation(this.appCompatActivity, android.R.anim.fade_in);
            this.couponCodeTextInput.setVisibility(View.VISIBLE);
        } else {
            animation = AnimationUtils.loadAnimation(this.appCompatActivity, android.R.anim.fade_out);
            this.couponCodeTextInput.setVisibility(View.GONE);
        }
        this.couponCodeTextInput.startAnimation(animation);
    }

    public void checkIfCustomOrder() {
        try {
            if (this.isCustomOrder) {
                this.itemReviewLayout.setVisibility(View.GONE);
                this.customOrderBox.setVisibility(View.VISIBLE);
            } else {
                this.itemReviewLayout.setVisibility(View.VISIBLE);
                this.customOrderBox.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }
}