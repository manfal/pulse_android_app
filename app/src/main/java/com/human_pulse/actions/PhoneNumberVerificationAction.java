package com.human_pulse.actions;

import android.content.Context;

import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.components.interfaces.IPhoneNumberVerificationInterface;
import com.human_pulse.datastore.PhoneNumberVerficationCodeController;

import java.util.HashMap;

import static com.human_pulse.constants.Constants.EMAIL_PARAM;
import static com.human_pulse.constants.Constants.VERIFICATION_CODE_PARAM;

/**
 * Created by anfal on 1/29/2016.
 */
public class PhoneNumberVerificationAction {

    private ILog logger = null;

    public PhoneNumberVerificationAction() {
        this.logger = Logger.getLogger();
    }

    public void sendVerficationCodeConfirmationRequest(Context context, IPhoneNumberVerificationInterface phoneNumberVerificationInterface, String verificationCode, String email) {
        HashMap<String, String> phoneVerificationParams = new HashMap<>();
        phoneVerificationParams.put(EMAIL_PARAM, email);
        phoneVerificationParams.put(VERIFICATION_CODE_PARAM, verificationCode);
        PhoneNumberVerficationCodeController phoneNumberVerficationCodeController = new PhoneNumberVerficationCodeController(context);
        phoneNumberVerficationCodeController.makePhoneNumberVerificationRequest(phoneVerificationParams, phoneNumberVerificationInterface);
    }
}
