package com.human_pulse.actions;

import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.app.R;
import com.human_pulse.components.dataobjects.OrderHistoryDataObject;
import com.human_pulse.components.dataobjects.ViewLocationDataObject;
import com.human_pulse.components.interfaces.IInternetConnectionObject;
import com.human_pulse.components.listeners.OrderHistoryScrollListener;
import com.human_pulse.components.navigation.LeftMenu;
import com.human_pulse.components.navigation.OrderHistoryList;
import com.human_pulse.components.observers.CartObserver;
import com.human_pulse.components.observers.LeftNavigationObserver;
import com.human_pulse.database.ClientDB;
import com.human_pulse.datastore.OrderHistoryController;
import com.human_pulse.enumerations.EItemFetchChoice;
import com.human_pulse.utilities.InternetConnectionUtility;
import com.human_pulse.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;

import static com.human_pulse.constants.Constants.ADDRESS_INFO;
import static com.human_pulse.constants.Constants.DELIVERY_ADDRESS;
import static com.human_pulse.constants.Constants.DELIVERY_STATUS_LABEL;
import static com.human_pulse.constants.Constants.ERROR;
import static com.human_pulse.constants.Constants.INVOICE_COUPON_CODE_VALUE;
import static com.human_pulse.constants.Constants.INVOICE_ID;
import static com.human_pulse.constants.Constants.INVOICE_INFO;
import static com.human_pulse.constants.Constants.INVOICE_LIST;
import static com.human_pulse.constants.Constants.INVOICE_NAME;
import static com.human_pulse.constants.Constants.ITEMS_INFO;
import static com.human_pulse.constants.Constants.MESSAGE;
import static com.human_pulse.constants.Constants.OH_DELIVERY_CHARGES;
import static com.human_pulse.constants.Constants.OH_ITEM_CHARGED_PRICE;
import static com.human_pulse.constants.Constants.OH_ITEM_NAME;
import static com.human_pulse.constants.Constants.OH_ITEM_QUANTITY;
import static com.human_pulse.constants.Constants.OH_TOTAL_CHARGED_PRICE;
import static com.human_pulse.constants.Constants.ORDER_DELIVERY_DATE;
import static com.human_pulse.constants.Constants.STATUS;
import static com.human_pulse.constants.Constants.SUCCESS;
import static com.human_pulse.constants.Constants.TOTAL_ORDERED_ITEMS;
import static com.human_pulse.constants.Constants.TOTAL_ORDER_PRICE;

/**
 * Created by anfal on 1/8/2016.
 */
public class OrderHistoryActions implements IInternetConnectionObject {

    private static OrderHistoryActions orderHistoryActionsInstance = null;
    private AppCompatActivity appCompatActivity = null;
    private ILog logger = null;
    private LeftMenu leftMenu = null;
    private Toolbar toolbar = null;
    private DrawerLayout navigationDrawer = null;
    private OrderHistoryController orderHistoryController = null;
    private ProgressBar progressBar = null;
    private RecyclerView orderHistoryRecyclerView = null;
    private OrderHistoryList orderHistoryList = null;
    private int itemBatch = 0;
    private ScrollView orderHistoryDetailLayout = null;
    private boolean isListFetchNecessary = true;
    private LinearLayout orderHistoryPageLayout, orderHistoryItemInfo = null;
    private SupportAnimator animater = null;
    private View viewToBeRevealed = null;
    private TextView ohDetailName, ohDetailDate, ohDetailAddress, ohDetailDeliveryCharges, ohDetailGrandTotal, ohDetailOrderCost, noItemsInOrderHistory, couponDeduction = null;
    private InternetConnectionUtility internetConnectionUtility = null;

    private OrderHistoryActions(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
        this.logger = Logger.getLogger();
        this.toolbar = (Toolbar) this.appCompatActivity.findViewById(R.id.oh_toolbar);
        this.navigationDrawer = (DrawerLayout) this.appCompatActivity.findViewById(R.id.oh_drawer_layout);
        this.appCompatActivity.setSupportActionBar(this.getToolbar());
        LeftNavigationObserver.setLeftNavigationObserverInstance(this.appCompatActivity, this.navigationDrawer, this.toolbar);
        this.internetConnectionUtility = new InternetConnectionUtility(this.appCompatActivity);
        this.internetConnectionUtility.registerAsObserver(this);
        this.orderHistoryController = new OrderHistoryController(this.appCompatActivity);
        this.progressBar = (ProgressBar) this.appCompatActivity.findViewById(R.id.order_history_progress);
        this.orderHistoryRecyclerView = (RecyclerView) this.appCompatActivity.findViewById(R.id.oh_list);
        this.orderHistoryList = new OrderHistoryList(this.orderHistoryRecyclerView, this.appCompatActivity);
        this.orderHistoryRecyclerView.addOnScrollListener(new OrderHistoryScrollListener());
        this.orderHistoryDetailLayout = (ScrollView) this.appCompatActivity.findViewById(R.id.oh_item_detail_layout);
        this.orderHistoryPageLayout = (LinearLayout) this.appCompatActivity.findViewById(R.id.oh_page_layout);
        this.ohDetailName = (TextView) this.appCompatActivity.findViewById(R.id.oh_det_name);
        this.ohDetailDate = (TextView) this.appCompatActivity.findViewById(R.id.oh_det_date);
        this.couponDeduction = (TextView) this.appCompatActivity.findViewById(R.id.oh_det_coupon_deduction);
        this.ohDetailAddress = (TextView) this.appCompatActivity.findViewById(R.id.oh_det_address);
        this.ohDetailDeliveryCharges = (TextView) this.appCompatActivity.findViewById(R.id.oh_det_delivery_charges);
        this.ohDetailGrandTotal = (TextView) this.appCompatActivity.findViewById(R.id.oh_det_grand_total);
        this.ohDetailOrderCost = (TextView) this.appCompatActivity.findViewById(R.id.oh_det_order_cost);
        this.orderHistoryItemInfo = (LinearLayout) this.appCompatActivity.findViewById(R.id.oh_det_item_info);
        this.noItemsInOrderHistory = (TextView) this.appCompatActivity.findViewById(R.id.oh_no_items_layout);
        CartObserver.setCartObserverInstance(this.appCompatActivity);
    }

    public static OrderHistoryActions getOrderHistoryActionsInstance() {
        return orderHistoryActionsInstance;
    }

    public static void setOrderHistoryActionsInstance(AppCompatActivity appCompatActivity) {
        if (null == orderHistoryActionsInstance) {
            orderHistoryActionsInstance = new OrderHistoryActions(appCompatActivity);
        }
    }

    public void destroyOrderHistoryActionsInstance() {
        if (null != orderHistoryActionsInstance) {
            orderHistoryActionsInstance = null;
            System.gc();
        }
    }

    public OrderHistoryController getOrderHistoryController() {
        return orderHistoryController;
    }

    public DrawerLayout getNavigationDrawer() {
        return navigationDrawer;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void fetchOrderHistory(EItemFetchChoice eItemFetchChoice) {
        try {
            String email = new ClientDB(this.appCompatActivity).getUserEmail();
            if (this.internetConnectionUtility.checkInternetConnectivity()) {
                this.getOrderHistoryController().getOrderHistory(
                        email,
                        Utilities.getInstance().getItemBatch(this.itemBatch),
                        eItemFetchChoice
                );
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void setOrderHistory(JSONObject response) {
        try {
            if (SUCCESS == Integer.parseInt(response.getString(STATUS))) {
                JSONArray orderHistoryArray = response.getJSONArray(INVOICE_LIST);
                if (ERROR < orderHistoryArray.length()) {
                    if (this.orderHistoryList.populateOrderHistory(parseOrderHistory(orderHistoryArray))) {
                        this.itemBatch = Utilities.getInstance().incrementItemBatch(this.itemBatch);
                    }
                    this.isListFetchNecessary = Utilities.getInstance().setIsDataFetchNecessary(orderHistoryArray.length());
                } else {
                    this.noItemsInOrderHistory.setVisibility(View.VISIBLE);
                }
                this.progressBar.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void displayOrderCompleteHistory(OrderHistoryDataObject orderHistoryDataObject, View view) {
        try {
            this.progressBar.setVisibility(View.VISIBLE);
            this.viewToBeRevealed = view;
            this.getOrderHistoryController().getInvoiceDetail(orderHistoryDataObject.getInvoiceId());
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private List<OrderHistoryDataObject> parseOrderHistory(JSONArray orderHistoryArray) throws JSONException {
        List<OrderHistoryDataObject> orderHistoryDataObjects = new ArrayList<>();
        for (int orderHistoryItemNumber = 0; orderHistoryItemNumber < orderHistoryArray.length(); orderHistoryItemNumber++) {
            OrderHistoryDataObject orderHistoryDataObject = new OrderHistoryDataObject();
            JSONObject jsonObject = orderHistoryArray.getJSONObject(orderHistoryItemNumber);
            orderHistoryDataObject.setInvoiceId(jsonObject.getInt(INVOICE_ID));
            orderHistoryDataObject.setInvoiceName(jsonObject.getString(INVOICE_NAME));
            orderHistoryDataObject.setDeliveryStatusLabel(jsonObject.getString(DELIVERY_STATUS_LABEL));
            orderHistoryDataObject.setOrderDate(jsonObject.getString(ORDER_DELIVERY_DATE));
            orderHistoryDataObject.setTotalItems(jsonObject.getInt(TOTAL_ORDERED_ITEMS));
            orderHistoryDataObject.setTotalPrice(jsonObject.getString(TOTAL_ORDER_PRICE));
            orderHistoryDataObjects.add(orderHistoryDataObject);
        }
        return orderHistoryDataObjects;
    }

    public boolean isListFetchNecessary() {
        return this.isListFetchNecessary;
    }

    public void appendOrderHistory(JSONObject response) {
        try {
            if (SUCCESS == Integer.parseInt(response.getString(STATUS))) {
                JSONArray orderHistoryArray = response.getJSONArray(INVOICE_LIST);
                List<OrderHistoryDataObject> orderHistoryDataObjects = parseOrderHistory(orderHistoryArray);
                int startPosition = this.orderHistoryList.getOrderHistoryDataObjects().size();
                this.orderHistoryList.getOrderHistoryDataObjects().addAll(orderHistoryDataObjects);
                this.orderHistoryList.getOrderHistoryAdapter().notifyItemRangeInserted(
                        startPosition + 1,
                        orderHistoryDataObjects.size()
                );
                this.itemBatch = Utilities.getInstance().incrementItemBatch(this.itemBatch);
                this.progressBar.setVisibility(View.GONE);
                this.isListFetchNecessary = Utilities.getInstance().setIsDataFetchNecessary(orderHistoryArray.length());
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void hideOrderHistoryDetailLayout() {
        try {
            this.orderHistoryPageLayout.setVisibility(View.VISIBLE);
            this.animater.reverse().start();
            this.orderHistoryDetailLayout.setVisibility(View.INVISIBLE);
            this.ohDetailName.setText(null);
            this.ohDetailDate.setText(null);
            this.ohDetailAddress.setText(null);
            this.ohDetailDeliveryCharges.setText(null);
            this.ohDetailOrderCost.setText(null);
            this.ohDetailGrandTotal.setText(null);
            this.orderHistoryItemInfo.removeAllViews();
            this.animater = null;
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public SupportAnimator getAnimater() {
        return this.animater;
    }

    public void startDetailReveal(View view) {
        try {
            ViewLocationDataObject viewLocationDataObject = Utilities.getInstance().getViewCoordinates(view);
            int radiusOfItemDetailLayout = Math.max(
                    this.orderHistoryDetailLayout.getWidth(),
                    this.orderHistoryDetailLayout.getHeight()
            );
            this.animater = ViewAnimationUtils.createCircularReveal(
                    this.orderHistoryDetailLayout,
                    viewLocationDataObject.getX(),
                    viewLocationDataObject.getY(),
                    0,
                    radiusOfItemDetailLayout
            );
            this.animater.setInterpolator(new AccelerateDecelerateInterpolator());
            this.animater.setDuration(400);
            this.orderHistoryDetailLayout.setVisibility(View.VISIBLE);
            this.animater.start();
            this.orderHistoryPageLayout.setVisibility(View.GONE);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void populateInvoiceDetail(JSONObject response) {
        try {
            if (SUCCESS == Integer.parseInt(response.getString(STATUS))) {
                if (this.parseInvoiceDetail(response)) {
                    this.progressBar.setVisibility(View.GONE);
                    this.startDetailReveal(this.viewToBeRevealed);
                }
            } else {
                Snackbar.make(this.orderHistoryPageLayout, response.getString(MESSAGE), Snackbar.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private boolean parseInvoiceDetail(JSONObject response) {
        boolean isDetailLoaded = false;
        try {
            JSONObject invoiceInfo = response.getJSONObject(INVOICE_INFO);
            JSONObject addressInfo = response.getJSONObject(ADDRESS_INFO);
            JSONArray itemsArray = response.getJSONArray(ITEMS_INFO);
            int couponDeductionValue = invoiceInfo.getInt(INVOICE_COUPON_CODE_VALUE);

            this.ohDetailName.setText(
                    this.appCompatActivity.getString(
                            R.string.order_no_text_template,
                            invoiceInfo.getString(INVOICE_NAME),
                            invoiceInfo.getString(DELIVERY_STATUS_LABEL)
                    )
            );
            this.ohDetailDate.setText(
                    this.appCompatActivity.getString(
                            R.string.order_date_text_template,
                            invoiceInfo.getString(ORDER_DELIVERY_DATE)
                    )
            );
            this.ohDetailAddress.setText(addressInfo.getString(DELIVERY_ADDRESS));
            this.ohDetailDeliveryCharges.setText(
                    this.appCompatActivity.getString(
                            R.string.delivery_charges_text_template,
                            response.getString(OH_DELIVERY_CHARGES)
                    )
            );

            if (0 < couponDeductionValue) {
                this.couponDeduction.setText(
                        this.appCompatActivity.getString(
                                R.string.coupon_deduction_text_template,
                                couponDeductionValue
                        )
                );
                this.couponDeduction.setVisibility(View.VISIBLE);
            } else {
                this.couponDeduction.setVisibility(View.GONE);
            }

            this.ohDetailGrandTotal.setText(
                    this.appCompatActivity.getString(
                            R.string.grand_total_text_template,
                            response.getString(OH_TOTAL_CHARGED_PRICE)
                    )
            );
            this.ohDetailOrderCost.setText(
                    this.appCompatActivity.getString(
                            R.string.order_cost_text_template,
                            invoiceInfo.getString(TOTAL_ORDER_PRICE)
                    )
            );
            this.populateItemsInfo(itemsArray);
            isDetailLoaded = true;
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
        return isDetailLoaded;
    }

    private void populateItemsInfo(JSONArray itemsInfo) {
        try {
            ArrayList<HashMap<String, String>> itemList = new ArrayList<>();
            for (int i = 0; i < itemsInfo.length(); i++) {
                JSONObject itemObject = itemsInfo.getJSONObject(i);
                HashMap<String, String> item = new HashMap<>();
                item.put(
                        OH_ITEM_QUANTITY,
                        this.appCompatActivity.getString(
                                R.string.order_item_quantity_name_template,
                                itemObject.getString(OH_ITEM_QUANTITY),
                                itemObject.getString(OH_ITEM_NAME)
                        )
                );
                item.put(
                        OH_ITEM_CHARGED_PRICE,
                        this.appCompatActivity.getString(
                                R.string.currency,
                                itemObject.getString(OH_ITEM_CHARGED_PRICE)
                        )
                );
                itemList.add(item);
            }
            ListAdapter itemInfoListAdapter = new SimpleAdapter(
                    this.appCompatActivity,
                    itemList,
                    R.layout.order_history_detail_item_row,
                    new String[]{OH_ITEM_QUANTITY, OH_ITEM_CHARGED_PRICE},
                    new int[]{R.id.oh_det_item_description, R.id.oh_det_item_price}
            );
            for (int i = 0; i < itemInfoListAdapter.getCount(); i++) {
                this.orderHistoryItemInfo.addView(itemInfoListAdapter.getView(i, null, null));
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void startReconnecting() {
        try {
            this.progressBar.setVisibility(View.GONE);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void tryToReconnect() {
        try {
            this.progressBar.setVisibility(View.VISIBLE);
            LeftNavigationObserver.getLeftNavigationObserverInstance().fetchLeftNavigation();
            CartObserver.getCartObserverInstance().instantiateCart();
            this.fetchOrderHistory(EItemFetchChoice.FETCHING_FIRST_TIME);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}