package com.human_pulse.actions;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.app.CheckoutPage;
import com.human_pulse.app.LoginPage;
import com.human_pulse.app.R;
import com.human_pulse.app.StorePage;
import com.human_pulse.components.CustomComponents;
import com.human_pulse.components.interfaces.IInternetConnectionObject;
import com.human_pulse.components.listeners.LoginLinkListener;
import com.human_pulse.components.listeners.SignupButtonListener;
import com.human_pulse.datastore.SignupController;
import com.human_pulse.utilities.InternetConnectionUtility;
import com.human_pulse.utilities.Utilities;
import com.human_pulse.utilities.Validators;

import org.json.JSONObject;

import java.util.HashMap;

import static com.human_pulse.constants.Constants.CHARGEDPRICE;
import static com.human_pulse.constants.Constants.EMAIL;
import static com.human_pulse.constants.Constants.EMAIL_PARAM;
import static com.human_pulse.constants.Constants.FIRSTNAME_PARAM;
import static com.human_pulse.constants.Constants.LASTNAME_PARAM;
import static com.human_pulse.constants.Constants.MESSAGE;
import static com.human_pulse.constants.Constants.PASSWORD_PARAM;
import static com.human_pulse.constants.Constants.ROLE;
import static com.human_pulse.constants.Constants.STATUS;
import static com.human_pulse.constants.Constants.SUCCESS;
import static com.human_pulse.constants.Constants.THRESHOLDPRICE;
import static com.human_pulse.constants.Constants.isCustomOrder;
import static com.human_pulse.constants.Constants.shouldUserBeRedirectedToCheckoutPage;

/**
 * Created by anfal on 12/26/2015.
 */
public class SignupActions implements IInternetConnectionObject {

    private static SignupActions signupActions = null;
    private AppCompatActivity appCompatActivity = null;
    private ProgressDialog progressDialog = null;
    private SignupController signupController = null;
    private ILog logger = null;
    private EditText firstName, lastName, email, password = null;
    private InternetConnectionUtility internetConnectionUtility = null;

    private SignupActions(AppCompatActivity appCompatActivity) {
        this.logger = Logger.getLogger();
        try {
            this.appCompatActivity = appCompatActivity;
            this.signupController = new SignupController(this.appCompatActivity);
            Button loginButton = (Button) this.appCompatActivity.findViewById(R.id.login_button);
            Button signupButton = (Button) this.appCompatActivity.findViewById(R.id.signup_button);
            this.firstName = (EditText) this.appCompatActivity.findViewById(R.id.signup_fname);
            this.lastName = (EditText) this.appCompatActivity.findViewById(R.id.signup_lname);
            this.email = (EditText) this.appCompatActivity.findViewById(R.id.signup_email);
            this.password = (EditText) this.appCompatActivity.findViewById(R.id.signup_password);
            loginButton.setOnClickListener(new LoginLinkListener());
            signupButton.setOnClickListener(new SignupButtonListener());
            this.internetConnectionUtility = new InternetConnectionUtility(this.appCompatActivity);
            this.internetConnectionUtility.registerAsObserver(this);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public static SignupActions getSignupActionsInstance() {
        return signupActions;
    }

    public static void setSignupActionsInstance(AppCompatActivity appCompatActivity) {
        if (null == signupActions) {
            signupActions = new SignupActions(appCompatActivity);
        }
    }

    public void destroySignupActionsInstance() {
        if (null != signupActions) {
            signupActions = null;
            System.gc();
        }
    }

    public void signUp() {
        try {
            if (this.internetConnectionUtility.checkInternetConnectivity()) {
                String firstName = this.firstName.getText().toString();
                String lastName = this.lastName.getText().toString();
                String email = this.email.getText().toString();
                String password = this.password.getText().toString();

                if (firstName.isEmpty() || lastName.isEmpty() || email.isEmpty() || password.isEmpty()) {
                    CustomComponents.getInstance().messageDialog(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_error_prompt), this.appCompatActivity.getString(R.string.alert_incomplete_information)).show();
                } else if (!Validators.getInstance().isEmailValid(email)) {
                    CustomComponents.getInstance().messageDialog(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_error_prompt), this.appCompatActivity.getString(R.string.email_not_valid)).show();
                } else {
                    final HashMap<String, String> signupParams = new HashMap<>();
                    signupParams.put(FIRSTNAME_PARAM, firstName);
                    signupParams.put(LASTNAME_PARAM, lastName);
                    signupParams.put(EMAIL_PARAM, email);
                    signupParams.put(PASSWORD_PARAM, password);
                    this.progressDialog = ProgressDialog.show(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_signingup_prompt), this.appCompatActivity.getString(R.string.text_please_wait), true);
                    this.signupController.signUpUser(signupParams);
                }
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void userRegistered(JSONObject response) {
        try {
            if (SUCCESS == Integer.parseInt(response.getString(STATUS))) {
                Utilities.getInstance().insertSessionRecordAfterDeletionInDB(
                        this.appCompatActivity,
                        response.getString(ROLE),
                        response.getString(EMAIL),
                        Integer.parseInt(response.getString(THRESHOLDPRICE)),
                        Integer.parseInt(response.getString(CHARGEDPRICE))
                );
                this.progressDialog.dismiss();
                if (shouldUserBeRedirectedToCheckoutPage) {
                    shouldUserBeRedirectedToCheckoutPage = false;
                    if (isCustomOrder) {
                        isCustomOrder = false;
                        Utilities.getInstance().startCheckOutWithMetaData(this.appCompatActivity, true);
                    } else {
                        Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, CheckoutPage.class);
                    }
                } else {
                    Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, StorePage.class);
                }
            } else {
                this.progressDialog.dismiss();
                CustomComponents.getInstance().messageDialog(this.appCompatActivity, this.appCompatActivity.getString(R.string.text_error_prompt), response.getString(MESSAGE)).show();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void startLoginActivity() {
        try {
            Utilities.getInstance().startNewActivityByFinishingOlder(this.appCompatActivity, LoginPage.class);
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void startReconnecting() {
        this.internetConnectionUtility.showNoInternetMessage();
    }

    @Override
    public void tryToReconnect() {
        try {
            this.signUp();
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
