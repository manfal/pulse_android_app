package com.human_pulse.actions;

import android.content.Context;
import android.util.Log;

import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.components.interfaces.IAddressResponseSenderReceiver;
import com.human_pulse.datastore.NewAddressRequestController;
import com.human_pulse.enumerations.EAddressManipulationChoice;

import java.util.HashMap;

import static com.human_pulse.analytics.logging.Constants.ERROR;
import static com.human_pulse.analytics.logging.Constants.TAG_ERROR;
import static com.human_pulse.constants.Constants.ADDRESS_PARAM;
import static com.human_pulse.constants.Constants.CITY_PARAM;
import static com.human_pulse.constants.Constants.EMAIL_PARAM;
import static com.human_pulse.constants.Constants.LABEL_PARAM;

/**
 * Created by anfal on 1/28/2016.
 */
public class NewAddressRequestAction {

    private ILog logger = null;
    private NewAddressRequestController newAddressRequestController = null;

    public NewAddressRequestAction(Context context) {
        this.logger = Logger.getLogger();
        this.newAddressRequestController = new NewAddressRequestController(context);
    }

    public void sendNewAddressRequest(IAddressResponseSenderReceiver addressResponseSenderReceiver, String label, String address, String city, String email) {
        try {
            HashMap<String, String> newAddress = new HashMap<>();
            newAddress.put(LABEL_PARAM, label);
            newAddress.put(ADDRESS_PARAM, address);
            newAddress.put(CITY_PARAM, city);
            newAddress.put(EMAIL_PARAM, email);
            this.newAddressRequestController.makeNewAddressRequest(newAddress, addressResponseSenderReceiver);
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void getAddressRequiredData(EAddressManipulationChoice eAddressManipulationChoice, IAddressResponseSenderReceiver addressResponseSenderReceiver) {
        try {
            this.newAddressRequestController.getAddressRequiredData(eAddressManipulationChoice, addressResponseSenderReceiver);
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
