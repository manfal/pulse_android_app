package com.human_pulse.actions;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.flurry.android.FlurryAgent;
import com.human_pulse.analytics.LogEvents;
import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.app.R;
import com.human_pulse.components.CustomComponents;
import com.human_pulse.components.dataobjects.AddressAdditionalDataObject;
import com.human_pulse.components.dataobjects.AddressDataObject;
import com.human_pulse.components.interfaces.IAddressResponseSenderReceiver;
import com.human_pulse.components.navigation.Addresses;
import com.human_pulse.components.viewholder.AddressHolder;
import com.human_pulse.database.ClientDB;
import com.human_pulse.datastore.PersonalAddressFragmentController;
import com.human_pulse.enumerations.EAddressManipulationChoice;
import com.human_pulse.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.human_pulse.analytics.logging.Constants.ERROR;
import static com.human_pulse.analytics.logging.Constants.TAG_ERROR;
import static com.human_pulse.constants.Constants.ADDRESS_CITY;
import static com.human_pulse.constants.Constants.ADDRESS_ID;
import static com.human_pulse.constants.Constants.ADDRESS_INFO;
import static com.human_pulse.constants.Constants.ADDRESS_LABEL;
import static com.human_pulse.constants.Constants.ADDRESS_PARAM;
import static com.human_pulse.constants.Constants.ADDRESS_TEXT;
import static com.human_pulse.constants.Constants.CITY_PARAM;
import static com.human_pulse.constants.Constants.EMAIL_PARAM;
import static com.human_pulse.constants.Constants.LABEL_PARAM;
import static com.human_pulse.constants.Constants.MESSAGE;
import static com.human_pulse.constants.Constants.NEW_ADDRESS;
import static com.human_pulse.constants.Constants.STATUS;
import static com.human_pulse.constants.Constants.SUCCESS;

/**
 * Created by anfal on 1/5/2016.
 */
public class PersonalAddressFragmentActionsSender implements IAddressResponseSenderReceiver {

    private static PersonalAddressFragmentActionsSender personalAddressFragmentActions = null;
    private ILog logger = null;
    private Context context = null;
    private PersonalAddressFragmentController personalAddressFragmentController = null;
    private ProgressBar progressBar = null;
    private RecyclerView recyclerView = null;
    private CardView newAddressCard = null;
    private Addresses addressAdapter = null;
    private FrameLayout addressLayout = null;
    private AddressHolder currentlyUnderEditAddressHolder = null;
    private LinearLayout addressDialogLayout = null;
    private NewAddressRequestAction newAddressRequestAction = null;

    private PersonalAddressFragmentActionsSender(Context context, ProgressBar progressBar, RecyclerView recyclerView, CardView newAddressCard, FrameLayout addressLayout, LinearLayout addressDialogLayout) {
        this.logger = Logger.getLogger();
        this.context = context;
        this.progressBar = progressBar;
        this.recyclerView = recyclerView;
        this.newAddressCard = newAddressCard;
        this.addressLayout = addressLayout;
        this.addressDialogLayout = addressDialogLayout;
        this.personalAddressFragmentController = new PersonalAddressFragmentController(this.context);
        this.newAddressRequestAction = new NewAddressRequestAction(this.context);
    }

    public static void setPersonalAddressFragmentActions(Context context, ProgressBar progressBar, RecyclerView recyclerView, CardView newAddressCard, FrameLayout addressLayout, LinearLayout addressDialogLayout) {
        if (null == personalAddressFragmentActions) {
            personalAddressFragmentActions = new PersonalAddressFragmentActionsSender(context, progressBar, recyclerView, newAddressCard, addressLayout, addressDialogLayout);
        }
    }

    public static PersonalAddressFragmentActionsSender getPersonalAddressFragmentActions() {
        return personalAddressFragmentActions;
    }

    public void destroyPersonalAddressFragmentActions() {
        if (null != personalAddressFragmentActions) {
            personalAddressFragmentActions = null;
            System.gc();
        }
    }

    public PersonalAddressFragmentController getPersonalAddressFragmentController() {
        return personalAddressFragmentController;
    }

    public void getExistingAddresses() {
        try {
            this.progressBar.setVisibility(View.VISIBLE);
            String email = new ClientDB(this.context).getUserEmail();
            this.getPersonalAddressFragmentController().getExistingAddresses(email);
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void setExistingAddresses(JSONArray addresses) {
        try {
            this.addressAdapter = new Addresses(this.recyclerView, this.context);
            if (addressAdapter.populateAddresses(getAddressDataObjects(addresses))) {
                this.progressBar.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private AddressDataObject getAddressFromJSON(JSONObject jsonObject) throws Exception {
        AddressDataObject addressDataObject = new AddressDataObject();
        addressDataObject.setId(jsonObject.getInt(ADDRESS_ID));
        addressDataObject.setLabel(jsonObject.getString(ADDRESS_LABEL));
        addressDataObject.setAddress(jsonObject.getString(ADDRESS_TEXT));
        addressDataObject.setCity(jsonObject.getString(ADDRESS_CITY));
        return addressDataObject;
    }

    public List<AddressDataObject> getAddressDataObjects(JSONArray addresses) {
        List<AddressDataObject> addressDataObjects = new ArrayList<>();
        try {
            for (int addressNumber = 0; addressNumber < addresses.length(); addressNumber++) {
                JSONObject addressObject = addresses.getJSONObject(addressNumber);
                addressDataObjects.add(this.getAddressFromJSON(addressObject));
            }
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
        return addressDataObjects;
    }

    @Override
    public void fetchDataForAddressDialog(EAddressManipulationChoice eAddressManipulationChoice, AddressHolder addressHolder) {
        try {
            this.progressBar.setVisibility(View.VISIBLE);
            switch (eAddressManipulationChoice) {
                case ADD:
                    this.newAddressRequestAction.getAddressRequiredData(eAddressManipulationChoice, this);
                    break;
                case EDIT:
                    this.currentlyUnderEditAddressHolder = addressHolder;
                    this.newAddressRequestAction.getAddressRequiredData(eAddressManipulationChoice, this);
                    break;
            }
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void openNewAddressDialog(EAddressManipulationChoice addressManipulationChoice, JSONObject response) {
        try {
            AddressAdditionalDataObject addressAdditionalDataObject = Utilities.getInstance().parseAdditionalAddressInfo(response);
            this.progressBar.setVisibility(View.GONE);
            switch (addressManipulationChoice) {
                case ADD:
                    CustomComponents.getInstance().newAddressDialog(this.context, this, addressAdditionalDataObject);
                    break;
                case EDIT:
                    CustomComponents.getInstance().editAddressDialog(this.context, this.addressDialogLayout, this.currentlyUnderEditAddressHolder.getAddressDataObject(), addressAdditionalDataObject).show();
                    break;
            }
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void sendNewAddressRequest(String label, String address, String city) {
        try {
            if (!label.equals("") && !address.equals("") && !city.equals("")) {
                this.progressBar.setVisibility(View.VISIBLE);
                String email = new ClientDB(this.context).getUserEmail();
                this.newAddressRequestAction.sendNewAddressRequest(this, label, address, city, email);
            } else {
                Snackbar.make(this.addressLayout, this.context.getString(R.string.incomplete_info_cannot_proceed), Snackbar.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void receiveNewAddressResponse(JSONObject response) {
        try {
            if (SUCCESS == Integer.parseInt(response.getString(STATUS))) {
                FlurryAgent.logEvent(
                        LogEvents.EVENT_ID_NEW_ADDRESS_ADDITION,
                        LogEvents.getEventData(
                                LogEvents.EVENT_ID_NEW_ADDRESS_ADDITION,
                                new String[]{
                                        LogEvents.TRIGGERED_ADDRESS_ADDITION_MY_ACCOUNT
                                }
                        )
                );
                JSONObject newAddress = response.getJSONObject(NEW_ADDRESS);
                this.addressAdapter.getAddressDataObjects().add(this.getAddressFromJSON(newAddress));
                this.addressAdapter.getAddressAdapter().notifyItemInserted(
                        this.addressAdapter.getAddressDataObjects().size() + 1
                );
            } else {
                Snackbar.make(this.addressLayout, response.getString(MESSAGE), Snackbar.LENGTH_LONG).show();
            }
            this.progressBar.setVisibility(View.GONE);
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void deleteAddress(AddressDataObject addressDataObject) {
        try {
            CustomComponents.getInstance().deleteAddressConfirmActionDialog(
                    this.context,
                    this.context.getString(
                            R.string.address_deletion_confirmation,
                            addressDataObject.getLabel()
                    ),
                    addressDataObject.getId()
            ).show();
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void startAddressDeletion(int addressId) {
        try {
            this.progressBar.setVisibility(View.VISIBLE);
            HashMap<String, Integer> deleteAddressData = new HashMap<>();
            deleteAddressData.put(ADDRESS_ID, addressId);
            this.getPersonalAddressFragmentController().makeDeleteAddressRequest(deleteAddressData);
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void deletedDataCleanUp(JSONObject response) {
        try {
            if (SUCCESS == Integer.parseInt(response.getString(STATUS))) {
                int addressIdToClean = Integer.parseInt(response.getString(ADDRESS_ID));
                for (int i = 0; i < this.addressAdapter.getAddressDataObjects().size(); i++) {
                    AddressDataObject addressDataObject = this.addressAdapter.getAddressDataObjects().get(i);
                    if (addressDataObject.getId() == addressIdToClean) {
                        this.addressAdapter.getAddressDataObjects().remove(i);
                        this.addressAdapter.getAddressAdapter().notifyItemRemoved(i);
                        break;
                    }
                }
            } else {
                Snackbar.make(this.addressLayout, response.getString(MESSAGE), Snackbar.LENGTH_LONG).show();
            }
            this.progressBar.setVisibility(View.GONE);
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void startAddressEdit(String label, String address, String city, Integer addressId) {
        try {
            if (!label.equals("") && !address.equals("") && !city.equals("")) {
                this.progressBar.setVisibility(View.VISIBLE);
                String email = new ClientDB(this.context).getUserEmail();
                HashMap<String, Object> addressEditParams = new HashMap<>();
                addressEditParams.put(LABEL_PARAM, label);
                addressEditParams.put(ADDRESS_PARAM, address);
                addressEditParams.put(CITY_PARAM, city);
                addressEditParams.put(EMAIL_PARAM, email);
                addressEditParams.put(ADDRESS_ID, addressId);
                this.getPersonalAddressFragmentController().makeEditAddressRequest(addressEditParams);
            } else {
                Snackbar.make(this.addressLayout, this.context.getString(R.string.incomplete_info_cannot_proceed), Snackbar.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void postAddressEditChores(JSONObject response) {
        try {
            this.progressBar.setVisibility(View.GONE);
            if (SUCCESS == Integer.parseInt(response.getString(STATUS))) {
                AddressDataObject addressDataObject = this.getAddressFromJSON(response.getJSONObject(ADDRESS_INFO));
                this.currentlyUnderEditAddressHolder.setAddressDataObject(addressDataObject);
                this.currentlyUnderEditAddressHolder.getLabel().setText(addressDataObject.getLabel());
            }
            Snackbar.make(this.addressLayout, response.getString(MESSAGE), Snackbar.LENGTH_LONG).show();
        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void openNewAddressDialog() {
        try {
            this.fetchDataForAddressDialog(EAddressManipulationChoice.ADD, null);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void editAddress(JSONObject response) {
        try {

        } catch (Exception ex) {
            this.logger.log(ERROR, TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
