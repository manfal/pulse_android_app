package com.human_pulse.actions;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.components.dataobjects.DeliveryChargesInfoDataObject;
import com.human_pulse.components.interfaces.IInternetConnectionObject;
import com.human_pulse.database.ClientDB;
import com.human_pulse.datastore.SplashScreenController;
import com.human_pulse.utilities.InternetConnectionUtility;
import com.human_pulse.utilities.Utilities;

import org.json.JSONObject;

import static com.human_pulse.constants.Constants.APP_VERSION_FLAG;
import static com.human_pulse.constants.Constants.CHARGEDPRICE;
import static com.human_pulse.constants.Constants.CLOSING_TIME;
import static com.human_pulse.constants.Constants.COMPANY_OPEN_RESPONSE;
import static com.human_pulse.constants.Constants.DEFAULT_EMAIL_ID;
import static com.human_pulse.constants.Constants.DEFAULT_USER_ROLE;
import static com.human_pulse.constants.Constants.IS_COMPANY_OPEN;
import static com.human_pulse.constants.Constants.MESSAGE;
import static com.human_pulse.constants.Constants.OPENING_TIME;
import static com.human_pulse.constants.Constants.STATUS;
import static com.human_pulse.constants.Constants.SUCCESS;
import static com.human_pulse.constants.Constants.THRESHOLDPRICE;

/**
 * Created by anfal on 1/14/2016.
 */

public class SplashScreenActions implements IInternetConnectionObject {

    private AppCompatActivity appCompatActivity = null;
    private ILog logger = null;
    private SplashScreenController splashScreenController = null;
    private ClientDB clientDB = null;
    private InternetConnectionUtility internetConnectionUtility = null;

    public SplashScreenActions(AppCompatActivity appCompatActivity) {
        this.logger = Logger.getLogger();
        this.appCompatActivity = appCompatActivity;
        this.internetConnectionUtility = new InternetConnectionUtility(this.appCompatActivity);
        internetConnectionUtility.registerAsObserver(this);
        this.splashScreenController = new SplashScreenController(this.appCompatActivity);
        this.clientDB = new ClientDB(this.appCompatActivity);
    }

    public void initiateUserInfoCheck() {
        try {
            if (this.internetConnectionUtility.checkInternetConnectivity()) {
                this.splashScreenController.getClientSyncInfo(this);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void updateClientInfoIfNecessary(JSONObject response) {
        try {
            if (SUCCESS == Integer.parseInt(response.getString(STATUS))) {
                DeliveryChargesInfoDataObject deliveryChargesInfoDataObject = new ClientDB(this.appCompatActivity).getDeliveryChargesInfo();
                int thresholdPrice = Integer.parseInt(response.getString(THRESHOLDPRICE));
                int chargedPrice = Integer.parseInt(response.getString(CHARGEDPRICE));
                int versionFlag = Integer.parseInt(response.getString(APP_VERSION_FLAG));
                if (deliveryChargesInfoDataObject.getChargedPrice() != chargedPrice || deliveryChargesInfoDataObject.getThresholdPrice() != thresholdPrice) {
                    //This case is necessary because if user is coming first time to website, there will be no value saved
                    //in db so in order to force delivery charges info this step is necessary.
                    if (!this.clientDB.isSessionNotEmpty()) {
                        this.clientDB.insertSessionRow(
                                DEFAULT_USER_ROLE,
                                DEFAULT_EMAIL_ID,
                                thresholdPrice,
                                chargedPrice
                        );
                    } else {
                        deliveryChargesInfoDataObject = new DeliveryChargesInfoDataObject();
                        deliveryChargesInfoDataObject.setChargedPrice(chargedPrice);
                        deliveryChargesInfoDataObject.setThresholdPrice(thresholdPrice);
                        this.clientDB.updateDeliveryChargesInSession(deliveryChargesInfoDataObject);
                    }
                }
                JSONObject companyTimingInfo = response.getJSONObject(COMPANY_OPEN_RESPONSE);
                boolean isCompanyOpen = companyTimingInfo.getBoolean(IS_COMPANY_OPEN);
                if (!isCompanyOpen) {
                    String openingTime = companyTimingInfo.getString(OPENING_TIME);
                    String closingTime = companyTimingInfo.getString(CLOSING_TIME);
                    Utilities.getInstance().startStoreActivityFromSplashScreen(this.appCompatActivity, versionFlag, openingTime, closingTime);
                } else {
                    Utilities.getInstance().startStoreActivityFromSplashScreen(this.appCompatActivity, versionFlag, null, null);
                }
            } else {
                Toast.makeText(this.appCompatActivity, response.getString(MESSAGE), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void startReconnecting() {
        this.internetConnectionUtility.showNoInternetMessage();
    }

    @Override
    public void tryToReconnect() {

    }
}
