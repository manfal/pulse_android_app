package com.human_pulse.actions;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.app.R;
import com.human_pulse.components.interfaces.IInternetConnectionObject;
import com.human_pulse.components.observers.CartObserver;
import com.human_pulse.components.observers.LeftNavigationObserver;
import com.human_pulse.customclasses.CustomWebViewClient;
import com.human_pulse.utilities.InternetConnectionUtility;

import static com.human_pulse.constants.Constants.PAGE_NAME;
import static com.human_pulse.constants.HttpRoutes.CONTACT_PAGE_ROUTE;
import static com.human_pulse.constants.HttpRoutes.HELP_PAGE_ROUTE;
import static com.human_pulse.constants.HttpRoutes.LOCATION_PAGE_ROUTE;
import static com.human_pulse.constants.HttpRoutes.PRIVACY_POLICY_PAGE_ROUTE;
import static com.human_pulse.constants.HttpRoutes.TERMS_AND_CONDITIONS_PAGE_ROUTE;

/**
 * Created by anfal on 2/2/2016.
 */
public class CustomerHelpActions implements IInternetConnectionObject {

    private static CustomerHelpActions customerHelpActions = null;
    private ILog logger = null;
    private AppCompatActivity appCompatActivity = null;
    private WebView webView = null;
    private DrawerLayout drawerLayout = null;
    private ProgressBar progressBar = null;
    private InternetConnectionUtility internetConnectionUtility = null;
    private String pageName = null;

    private CustomerHelpActions(AppCompatActivity appCompatActivity) {
        logger = Logger.getLogger();
        this.appCompatActivity = appCompatActivity;
        this.webView = (WebView) this.appCompatActivity.findViewById(R.id.ch_page_web_view);
        this.progressBar = (ProgressBar) this.appCompatActivity.findViewById(R.id.ch_page_progress);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.getSettings().setLoadsImagesAutomatically(true);
        this.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        this.webView.setWebViewClient(new CustomWebViewClient(this.progressBar));
        this.internetConnectionUtility = new InternetConnectionUtility(this.appCompatActivity);
        this.internetConnectionUtility.registerAsObserver(this);
        Toolbar toolbar = (Toolbar) this.appCompatActivity.findViewById(R.id.ch_page_toolbar);
        this.drawerLayout = (DrawerLayout) this.appCompatActivity.findViewById(R.id.ch_page_drawer_layout);
        this.appCompatActivity.setSupportActionBar(toolbar);
        LeftNavigationObserver.setLeftNavigationObserverInstance(this.appCompatActivity, this.drawerLayout, toolbar);
        CartObserver.setCartObserverInstance(this.appCompatActivity);
        Bundle metaData = this.appCompatActivity.getIntent().getExtras();
        if (null != metaData) {
            this.loadNewWebPage(metaData.getString(PAGE_NAME));
        }
    }

    public static CustomerHelpActions getCustomerHelpActions() {
        return customerHelpActions;
    }

    public static void setCustomerHelpActions(AppCompatActivity appCompatActivity) {
        if (null == customerHelpActions) {
            customerHelpActions = new CustomerHelpActions(appCompatActivity);
        }
    }

    public void destroyCustomerHelpActions() {
        if (null != customerHelpActions) {
            customerHelpActions = null;
            System.gc();
        }
    }

    public void loadNewWebPage(String pageName) {
        try {
            this.pageName = pageName;
            this.loadPage();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void loadPage() {
        try {
            if (this.internetConnectionUtility.checkInternetConnectivity()) {
                this.webView.loadUrl(this.getPageUrlFromPageName(this.pageName));
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    private String getPageUrlFromPageName(String pageName) {
        String URL = null;
        if (pageName.equals(this.appCompatActivity.getString(R.string.text_help))) {
            URL = HELP_PAGE_ROUTE;
        } else if (pageName.equals(this.appCompatActivity.getString(R.string.text_locations))) {
            URL = LOCATION_PAGE_ROUTE;
        } else if (pageName.equals(this.appCompatActivity.getString(R.string.text_tnc))) {
            URL = TERMS_AND_CONDITIONS_PAGE_ROUTE;
        } else if (pageName.equals(this.appCompatActivity.getString(R.string.text_contact))) {
            URL = CONTACT_PAGE_ROUTE;
        } else if (pageName.equals(this.appCompatActivity.getString(R.string.text_privacy_policy))) {
            URL = PRIVACY_POLICY_PAGE_ROUTE;
        }
        return URL;
    }

    public DrawerLayout getDrawerLayout() {
        return this.drawerLayout;
    }

    @Override
    public void startReconnecting() {
        try {
            this.progressBar.setVisibility(View.GONE);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void tryToReconnect() {
        try {
            this.progressBar.setVisibility(View.VISIBLE);
            this.loadPage();
            LeftNavigationObserver.getLeftNavigationObserverInstance().fetchLeftNavigation();
            CartObserver.getCartObserverInstance().instantiateCart();
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
