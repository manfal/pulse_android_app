package com.human_pulse.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.human_pulse.components.dataobjects.CartItemDataObject;
import com.human_pulse.components.dataobjects.DeliveryChargesInfoDataObject;
import com.human_pulse.database.contracts.Contracts;
import com.human_pulse.database.helpers.ClientDBHelper;

import java.util.ArrayList;
import java.util.List;

import static com.human_pulse.constants.Constants.DEFAULT_EMAIL_ID;

/**
 * Created by anfal on 12/9/2015.
 */
public class ClientDB {

    private ClientDBHelper clientDBHelper = null;

    public ClientDB(Context context) {
        this.clientDBHelper = new ClientDBHelper(context);
    }

    public long insertSessionRow(String role, String email, int threshOldPrice, int chargedPrice) {
        SQLiteDatabase sqLiteDatabase = this.clientDBHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Contracts.SessionsEntry.COLUMN_NAME_ROLE, role);
        values.put(Contracts.SessionsEntry.COLUMN_NAME_EMAIL, email);
        values.put(Contracts.SessionsEntry.COLUMN_NAME_THRESHOLDPRICE, threshOldPrice);
        values.put(Contracts.SessionsEntry.COLUMN_NAME_CHARGEDPRICE, chargedPrice);
        long newRowId = sqLiteDatabase.insert(Contracts.SessionsEntry.TABLE_NAME, null, values);
        sqLiteDatabase.close();
        return newRowId;
    }

    public void deleteSessionRecords() {
        if (isSessionNotEmpty()) {
            SQLiteDatabase sqLiteDatabase = this.clientDBHelper.getWritableDatabase();
            sqLiteDatabase.delete(Contracts.SessionsEntry.TABLE_NAME, null, null);
            sqLiteDatabase.close();
        }
    }

    public DeliveryChargesInfoDataObject getDeliveryChargesInfo() {
        DeliveryChargesInfoDataObject deliveryChargesInfoDataObject = new DeliveryChargesInfoDataObject();
        if (isSessionNotEmpty()) {
            SQLiteDatabase sqLiteDatabase = this.clientDBHelper.getReadableDatabase();
            String[] projection = new String[]{
                    Contracts.SessionsEntry.COLUMN_NAME_CHARGEDPRICE,
                    Contracts.SessionsEntry.COLUMN_NAME_THRESHOLDPRICE
            };
            Cursor cursor = sqLiteDatabase.query(Contracts.SessionsEntry.TABLE_NAME, projection, null, null, null, null, null);
            if (cursor.moveToFirst()) {
                deliveryChargesInfoDataObject.setThresholdPrice(
                        cursor.getInt(cursor.getColumnIndex(Contracts.SessionsEntry.COLUMN_NAME_THRESHOLDPRICE))
                );
                deliveryChargesInfoDataObject.setChargedPrice(
                        cursor.getInt(cursor.getColumnIndex(Contracts.SessionsEntry.COLUMN_NAME_CHARGEDPRICE))
                );
            }
            sqLiteDatabase.close();
            cursor.close();
        }
        return deliveryChargesInfoDataObject;
    }

    public boolean isSessionNotEmpty() {
        int rowCount = 0;
        SQLiteDatabase sqLiteDatabase = this.clientDBHelper.getReadableDatabase();
        String count = "SELECT COUNT(*) FROM " + Contracts.SessionsEntry.TABLE_NAME;
        Cursor cursor = sqLiteDatabase.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            rowCount = cursor.getInt(0);
        }
        sqLiteDatabase.close();
        cursor.close();
        return rowCount > 0;
    }

    public String getUserEmail() {
        String email = null;
        if (isSessionNotEmpty()) {
            SQLiteDatabase sqLiteDatabase = this.clientDBHelper.getReadableDatabase();
            String[] projection = new String[]{
                    Contracts.SessionsEntry.COLUMN_NAME_EMAIL
            };
            Cursor cursor = sqLiteDatabase.query(Contracts.SessionsEntry.TABLE_NAME, projection, null, null, null, null, null, Integer.toString(1));
            if (cursor.moveToFirst()) {
                email = cursor.getString(cursor.getColumnIndex(Contracts.SessionsEntry.COLUMN_NAME_EMAIL));
            }
            sqLiteDatabase.close();
            cursor.close();
        }
        return email;
    }

    public int updateDeliveryChargesInSession(DeliveryChargesInfoDataObject deliveryChargesInfoDataObject) {
        SQLiteDatabase sqLiteDatabase = this.clientDBHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Contracts.SessionsEntry.COLUMN_NAME_THRESHOLDPRICE, deliveryChargesInfoDataObject.getThresholdPrice());
        values.put(Contracts.SessionsEntry.COLUMN_NAME_CHARGEDPRICE, deliveryChargesInfoDataObject.getChargedPrice());
        int updateResult = sqLiteDatabase.update(Contracts.SessionsEntry.TABLE_NAME, values, null, null);
        sqLiteDatabase.close();
        return updateResult;
    }

    public int updateUserEmailToDefaultForLogout() {
        SQLiteDatabase sqLiteDatabase = this.clientDBHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Contracts.SessionsEntry.COLUMN_NAME_EMAIL, DEFAULT_EMAIL_ID);
        int updateResult = sqLiteDatabase.update(Contracts.SessionsEntry.TABLE_NAME, values, null, null);
        sqLiteDatabase.close();
        return updateResult;
    }

    public boolean isOrderHistoryNotEmpty() {
        int rowCount = 0;
        SQLiteDatabase sqLiteDatabase = this.clientDBHelper.getReadableDatabase();
        String count = "SELECT COUNT(*) FROM " + Contracts.OrderHistoryEntry.TABLE_NAME;
        Cursor cursor = sqLiteDatabase.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            rowCount = cursor.getInt(0);
        }
        sqLiteDatabase.close();
        cursor.close();
        return rowCount > 0;
    }

    public int updateOrderHistoryRow(int itemId, int orderedQuantity) {
        SQLiteDatabase sqLiteDatabase = this.clientDBHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_CHECKOUT_QUANTITY, orderedQuantity);
        String whereClause = Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_ID + " = " + itemId;
        int updateResult = sqLiteDatabase.update(Contracts.OrderHistoryEntry.TABLE_NAME, values, whereClause, null);
        sqLiteDatabase.close();
        return updateResult;
    }

    public long insertOrderHistoryRow(CartItemDataObject cartItemDataObject) {
        SQLiteDatabase sqLiteDatabase = this.clientDBHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_ID, cartItemDataObject.getItemId());
        values.put(Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_NAME, cartItemDataObject.getItemName());
        values.put(Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_WEIGHT_OR_QUANTITY, cartItemDataObject.getItemWeightOrQuantity());
        values.put(Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_PRICE, cartItemDataObject.getItemPrice());
        values.put(Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_DISCOUNT, cartItemDataObject.getItemDiscount());
        values.put(Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_CHECKOUT_QUANTITY, cartItemDataObject.getQuantityToCheckout());
        long insertResult = sqLiteDatabase.insert(Contracts.OrderHistoryEntry.TABLE_NAME, null, values);
        sqLiteDatabase.close();
        return insertResult;
    }

    public void deleteOrderHistoryRecord(int itemId) {
        SQLiteDatabase sqLiteDatabase = this.clientDBHelper.getWritableDatabase();
        String whereClause = Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_ID + " = " + itemId;
        sqLiteDatabase.delete(Contracts.OrderHistoryEntry.TABLE_NAME, whereClause, null);
        sqLiteDatabase.close();
    }

    public List<CartItemDataObject> getOrderHistoryItemsFromDB() {
        List<CartItemDataObject> cartItemDataObjects = new ArrayList<>();
        if (this.isOrderHistoryNotEmpty()) {
            SQLiteDatabase sqLiteDatabase = this.clientDBHelper.getReadableDatabase();
            String[] projection = new String[]{
                    Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_ID,
                    Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_NAME,
                    Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_WEIGHT_OR_QUANTITY,
                    Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_PRICE,
                    Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_DISCOUNT,
                    Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_CHECKOUT_QUANTITY
            };
            Cursor cursor = sqLiteDatabase.query(Contracts.OrderHistoryEntry.TABLE_NAME, projection, null, null, null, null, null, null);
            if (cursor.moveToFirst()) {
                do {
                    CartItemDataObject cartItemDataObject = new CartItemDataObject();
                    cartItemDataObject.setItemId(cursor.getInt(cursor.getColumnIndex(Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_ID)));
                    cartItemDataObject.setItemName(cursor.getString(cursor.getColumnIndex(Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_NAME)));
                    cartItemDataObject.setItemWeightOrQuantity(cursor.getString(cursor.getColumnIndex(Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_WEIGHT_OR_QUANTITY)));
                    cartItemDataObject.setItemPrice(cursor.getInt(cursor.getColumnIndex(Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_PRICE)));
                    cartItemDataObject.setItemDiscount(cursor.getInt(cursor.getColumnIndex(Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_DISCOUNT)));
                    cartItemDataObject.setQuantityToCheckout(cursor.getInt(cursor.getColumnIndex(Contracts.OrderHistoryEntry.COLUMN_NAME_ITEM_CHECKOUT_QUANTITY)));
                    cartItemDataObjects.add(cartItemDataObject);
                } while (cursor.moveToNext());
            }
            sqLiteDatabase.close();
            cursor.close();
        }
        return cartItemDataObjects;
    }

    public void deleteOrderHistoryRecords() {
        if (isOrderHistoryNotEmpty()) {
            SQLiteDatabase sqLiteDatabase = this.clientDBHelper.getWritableDatabase();
            sqLiteDatabase.delete(Contracts.OrderHistoryEntry.TABLE_NAME, null, null);
            sqLiteDatabase.close();
        }
    }
}
