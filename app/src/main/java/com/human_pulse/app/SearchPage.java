package com.human_pulse.app;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.human_pulse.actions.SearchActions;
import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.customclasses.FontCompatActivity;

/**
 * Created by anfal on 12/6/2015.
 */
public class SearchPage extends FontCompatActivity {

    private ILog logger = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_page);
        this.logger = Logger.getLogger();
        SearchActions.setSearchActionsInstance(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SearchActions.getSearchActionsInstance().destroySearchActionsInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.cart_action:
                try {
                    if (!SearchActions.getSearchActionsInstance().getSearchItemPageLayout().isDrawerOpen(Gravity.RIGHT)) {
                        SearchActions.getSearchActionsInstance().getSearchItemPageLayout().openDrawer(Gravity.RIGHT);
                    }
                } catch (Exception ex) {
                    this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
                }
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    @Override
    public void onBackPressed() {
        try {
            if (SearchActions.getSearchActionsInstance().getSearchItemPageLayout().isDrawerOpen(Gravity.RIGHT)) {
                SearchActions.getSearchActionsInstance().getSearchItemPageLayout().closeDrawer(Gravity.RIGHT);
            } else if (null != SearchActions.getSearchActionsInstance().getItemDetailLayoutManipulator().getAnimator()) {
                SearchActions.getSearchActionsInstance().getItemDetailLayoutManipulator().hideItemDetails();
            } else {
                SearchActions.getSearchActionsInstance().finishSearchActivity();
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }
}
