package com.human_pulse.app;

import android.os.Bundle;
import android.util.Log;

import com.human_pulse.actions.ResetPasswordActions;
import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.customclasses.CustomAccountActivity;

public class ResetPasswordPage extends CustomAccountActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password_page);

        ILog logger = Logger.getLogger();
        try {
            super.setLogger(logger);
            ResetPasswordActions.setResetPasswordActionsInstance(this);
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ResetPasswordActions.getResetPasswordActionsInstance().destroyResetPasswordActionsInstance();
    }
}
