package com.human_pulse.app;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;

import com.human_pulse.actions.MyAccountActions;
import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.customclasses.CustomCompleteToolBarMenuActivity;
import com.human_pulse.utilities.Utilities;

/**
 * Created by anfal on 1/1/2016.
 */
public class MyAccountPage extends CustomCompleteToolBarMenuActivity {

    private ILog logger = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myaccount_page);

        logger = Logger.getLogger();

        try {
            MyAccountActions.setMyAccountActionsInstance(this);
            super.assignDrawerLayout(MyAccountActions.getMyAccountActionsInstance().getLeftNavDrawer());
            MyAccountActions.getMyAccountActionsInstance().setupMyAccountFragments();
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyAccountActions.getMyAccountActionsInstance().destroyMyAccountActionsInstance();
    }

    @Override
    public void onBackPressed() {
        try {
            if (MyAccountActions.getMyAccountActionsInstance().getLeftNavDrawer().isDrawerOpen(Gravity.RIGHT)) {
                MyAccountActions.getMyAccountActionsInstance().getLeftNavDrawer().closeDrawer(Gravity.RIGHT);
            } else {
                Utilities.getInstance().startNewActivityByFinishingOlder(this, StorePage.class);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }
}