package com.human_pulse.app;

import android.app.Application;

import com.flurry.android.FlurryAgent;

import static com.human_pulse.analytics.logging.Constants.VERBOSE;
import static com.human_pulse.constants.Constants.FLURRY_API_KEY;

/**
 * Created by anfal on 12/10/2015.
 */

public class ParentApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FlurryAgent.init(this, FLURRY_API_KEY);
        FlurryAgent.setLogEnabled(true);
        FlurryAgent.setLogEvents(true);
        FlurryAgent.setVersionName(BuildConfig.VERSION_NAME);
        FlurryAgent.setLogLevel(VERBOSE);
    }
}
