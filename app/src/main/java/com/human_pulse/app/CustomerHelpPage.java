package com.human_pulse.app;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;

import com.human_pulse.actions.CustomerHelpActions;
import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.customclasses.CustomCompleteToolBarMenuActivity;
import com.human_pulse.utilities.Utilities;

/**
 * Created by anfal on 2/2/2016.
 */
public class CustomerHelpPage extends CustomCompleteToolBarMenuActivity {

    private ILog logger = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_help_page);

        this.logger = Logger.getLogger();

        try {
            CustomerHelpActions.setCustomerHelpActions(this);
            super.assignDrawerLayout(CustomerHelpActions.getCustomerHelpActions().getDrawerLayout());
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CustomerHelpActions.getCustomerHelpActions().destroyCustomerHelpActions();
    }

    @Override
    public void onBackPressed() {
        try {
            if (CustomerHelpActions.getCustomerHelpActions().getDrawerLayout().isDrawerOpen(Gravity.RIGHT)) {
                CustomerHelpActions.getCustomerHelpActions().getDrawerLayout().closeDrawer(Gravity.RIGHT);
            } else {
                Utilities.getInstance().startNewActivityByFinishingOlder(this, StorePage.class);
            }
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }
}
