package com.human_pulse.app;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;

import com.human_pulse.actions.StoreActions;
import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.customclasses.CustomCompleteToolBarMenuActivity;


public class StorePage extends CustomCompleteToolBarMenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_page);
        ILog logger = Logger.getLogger();
        try {
            StoreActions.setStoreActionInstance(this);
            super.assignDrawerLayout(StoreActions.getStoreActionInstance().getStoreDrawer());
            StoreActions.getStoreActionInstance().fetchStoreDepartments();
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.store_toolbar_menu, menu);
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        StoreActions.getStoreActionInstance().destroyStoreActionInstance();
    }

    @Override
    public void onBackPressed() {
        if (StoreActions.getStoreActionInstance().getStoreDrawer().isDrawerOpen(Gravity.RIGHT)) {
            StoreActions.getStoreActionInstance().getStoreDrawer().closeDrawer(Gravity.RIGHT);
        } else {
            super.onBackPressed();
        }
    }
}
