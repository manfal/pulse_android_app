package com.human_pulse.app;

import android.os.Bundle;
import android.util.Log;

import com.human_pulse.actions.CheckoutPageActions;
import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.customclasses.FontCompatActivity;
import com.human_pulse.utilities.Utilities;

/**
 * Created by anfal on 1/27/2016.
 */
public class CheckoutPage extends FontCompatActivity {
    private ILog logger = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_page);

        this.logger = Logger.getLogger();

        try {
            CheckoutPageActions.setCheckoutPageActionsInstance(this);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CheckoutPageActions.getCheckoutPageActionsInstance().destroyCheckoutPageActionsInstance();
    }

    @Override
    public void onBackPressed() {
        try {
            Utilities.getInstance().startNewActivityByFinishingOlder(this, StorePage.class);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex.getCause());
        }
    }
}
