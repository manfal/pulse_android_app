package com.human_pulse.datastore;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.human_pulse.actions.CheckoutPageActions;
import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.utilities.VolleyInstance;

import org.json.JSONObject;

import java.util.HashMap;

import static com.human_pulse.constants.Constants.INVOICE_REVIEW_REQUEST_PARAMS;
import static com.human_pulse.constants.Constants.REQUEST_RETRY_TIMEOUT;
import static com.human_pulse.constants.Constants.REQUST_RETRY_COUNT;
import static com.human_pulse.constants.Constants.USER_GET_ACCOUNT_INFO_PARAM;
import static com.human_pulse.constants.HttpRoutes.ADD_PHONE_NUMBER_ROUTE;
import static com.human_pulse.constants.HttpRoutes.CUSTOM_ORDER_REQUEST;
import static com.human_pulse.constants.HttpRoutes.GET_INVOICE_REVIEW_INFO_ROUTE;
import static com.human_pulse.constants.HttpRoutes.GET_PRE_CHECKOUT_DATA_ROUTE;
import static com.human_pulse.constants.HttpRoutes.STORE_DELIVERY_ITEMS_REQUST;

/**
 * Created by anfal on 1/28/2016.
 */
public class CheckoutPageController {
    private ILog logger = null;
    private Context context = null;

    public CheckoutPageController(Context context) {
        this.logger = Logger.getLogger();
        try {
            this.context = context;
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void getPreCheckoutData(String email) {
        try {
            String getPreCheckoutData = String.format(
                    GET_PRE_CHECKOUT_DATA_ROUTE + USER_GET_ACCOUNT_INFO_PARAM,
                    email
            );
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, getPreCheckoutData, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CheckoutPageActions.getCheckoutPageActionsInstance().populatePreCheckoutData(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);

        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void sendPhoneNumberRequest(HashMap<String, String> phoneNumberData) {
        try {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ADD_PHONE_NUMBER_ROUTE, new JSONObject(phoneNumberData), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CheckoutPageActions.getCheckoutPageActionsInstance().postPhoneNumberAdditionTasks(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);

        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void fetchInvoiceReviewData(String email, String addressLabel, String couponCode, int orderCost) {
        try {
            String invoiceReviewRequestString = String.format(
                    GET_INVOICE_REVIEW_INFO_ROUTE + INVOICE_REVIEW_REQUEST_PARAMS,
                    email,
                    addressLabel,
                    couponCode,
                    orderCost
            );
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, invoiceReviewRequestString, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CheckoutPageActions.getCheckoutPageActionsInstance().loadInvoiceReviewData(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);

        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void sendCheckoutItems(HashMap<String, Object> checkoutItems) {
        try {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, STORE_DELIVERY_ITEMS_REQUST, new JSONObject(checkoutItems), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CheckoutPageActions.getCheckoutPageActionsInstance().cleanUpAfterCheckout(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);

        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void sendCustomOrder(HashMap<String, String> customOrder) {
        try {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, CUSTOM_ORDER_REQUEST, new JSONObject(customOrder), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CheckoutPageActions.getCheckoutPageActionsInstance().cleanUpAfterCheckout(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
