package com.human_pulse.datastore;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.human_pulse.actions.DepartmentCategoryFragmentActions;
import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.enumerations.EItemFetchChoice;
import com.human_pulse.utilities.VolleyInstance;

import org.json.JSONObject;

import java.util.Map;

import static com.human_pulse.constants.Constants.BATCH_SIZE_KEY;
import static com.human_pulse.constants.Constants.BATCH_START_KEY;
import static com.human_pulse.constants.Constants.DEPARTMENT_ITEMS_PARAMS;
import static com.human_pulse.constants.Constants.ITEM_LIST;
import static com.human_pulse.constants.Constants.REQUEST_RETRY_TIMEOUT;
import static com.human_pulse.constants.Constants.REQUST_RETRY_COUNT;
import static com.human_pulse.constants.HttpRoutes.DEPARTMENT_ITEMS_ROUTE;

/**
 * Created by anfal on 12/28/2015.
 */
public class DepartmentItemsFragmentController {
    private ILog logger = null;
    private Context context = null;

    public DepartmentItemsFragmentController(Context context) {
        this.logger = Logger.getLogger();
        try {
            this.context = context;
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public synchronized void getDepartmentItems(String email, int departmentId, int categoryId, Map<String, Integer> batch, final EItemFetchChoice fetchChoice, final DepartmentCategoryFragmentActions departmentCategoryFragmentActions) {
        try {
            String departmentItemsRequestString = String.format(
                    DEPARTMENT_ITEMS_ROUTE + DEPARTMENT_ITEMS_PARAMS,
                    departmentId,
                    categoryId,
                    batch.get(BATCH_START_KEY),
                    batch.get(BATCH_SIZE_KEY),
                    email
            );
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, departmentItemsRequestString, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        switch (fetchChoice) {
                            case FETCHING_FIRST_TIME:
                                departmentCategoryFragmentActions.setDepartmentItems(response.getJSONArray(ITEM_LIST));
                                break;
                            case FETCHING_TO_APPEND:
                                departmentCategoryFragmentActions.setPreLoadedDepartmentItems(response.getJSONArray(ITEM_LIST));
                                break;
                        }
                    } catch (Exception ex) {
                        logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
