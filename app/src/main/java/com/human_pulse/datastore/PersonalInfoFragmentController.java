package com.human_pulse.datastore;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.human_pulse.actions.PersonalInfoFragmentActions;
import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.utilities.VolleyInstance;

import org.json.JSONObject;

import java.util.HashMap;

import static com.human_pulse.constants.Constants.REQUEST_RETRY_TIMEOUT;
import static com.human_pulse.constants.Constants.REQUST_RETRY_COUNT;
import static com.human_pulse.constants.Constants.USER_GET_ACCOUNT_INFO_PARAM;
import static com.human_pulse.constants.Constants.USER_INFO;
import static com.human_pulse.constants.HttpRoutes.EDIT_ACCOUNT_PASSWORD_ROUTE;
import static com.human_pulse.constants.HttpRoutes.EDIT_USER_PERSONAL_INFO_ROUTE;
import static com.human_pulse.constants.HttpRoutes.USER_ACCOUNT_INFO_ROUTE;

/**
 * Created by anfal on 1/3/2016.
 */
public class PersonalInfoFragmentController {

    private ILog logger = null;
    private Context context = null;

    public PersonalInfoFragmentController(Context context) {
        this.logger = Logger.getLogger();
        try {
            this.context = context;
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void getUserAccountInfo(String userEmail) {
        try {
            String userInfoRequestString = String.format(
                    USER_ACCOUNT_INFO_ROUTE + USER_GET_ACCOUNT_INFO_PARAM,
                    userEmail
            );

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, userInfoRequestString, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        PersonalInfoFragmentActions.getPersonalInfoActionInstance().displayUserAccountInfo(response.getJSONObject(USER_INFO));
                    } catch (Exception ex) {
                        logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void makeEditPasswordRequest(HashMap<String, String> editPasswordParams) {
        try {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, EDIT_ACCOUNT_PASSWORD_ROUTE, new JSONObject(editPasswordParams), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        PersonalInfoFragmentActions.getPersonalInfoActionInstance().postPasswordEditChores(response);
                    } catch (Exception ex) {
                        logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void makeEditPersonalInfoRequest(HashMap<String, String> editPersonalInfoParams) {
        try {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, EDIT_USER_PERSONAL_INFO_ROUTE, new JSONObject(editPersonalInfoParams), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        PersonalInfoFragmentActions.getPersonalInfoActionInstance().postPersonalInfoEditChores(response);
                    } catch (Exception ex) {
                        logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
