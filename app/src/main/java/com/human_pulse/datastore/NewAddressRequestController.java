package com.human_pulse.datastore;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.components.interfaces.IAddressResponseSenderReceiver;
import com.human_pulse.enumerations.EAddressManipulationChoice;
import com.human_pulse.utilities.VolleyInstance;

import org.json.JSONObject;

import java.util.HashMap;

import static com.human_pulse.constants.Constants.REQUEST_RETRY_TIMEOUT;
import static com.human_pulse.constants.Constants.REQUST_RETRY_COUNT;
import static com.human_pulse.constants.HttpRoutes.GET_ADD_ADDRESS_INFO_ROUTE;
import static com.human_pulse.constants.HttpRoutes.NEW_ADDRESS_ROUTE;

/**
 * Created by anfal on 1/28/2016.
 */
public class NewAddressRequestController {

    private ILog logger = null;
    private Context context = null;

    public NewAddressRequestController(Context context) {
        this.logger = Logger.getLogger();
        try {
            this.context = context;
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void makeNewAddressRequest(HashMap<String, String> newAddress, final IAddressResponseSenderReceiver addressResponseSenderReceiver) {
        try {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, NEW_ADDRESS_ROUTE, new JSONObject(newAddress), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        addressResponseSenderReceiver.receiveNewAddressResponse(response);
                    } catch (Exception ex) {
                        logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void getAddressRequiredData(final EAddressManipulationChoice eAddressManipulationChoice, final IAddressResponseSenderReceiver addressResponseSenderReceiver) {
        try {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, GET_ADD_ADDRESS_INFO_ROUTE, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        addressResponseSenderReceiver.openNewAddressDialog(eAddressManipulationChoice, response);
                    } catch (Exception ex) {
                        logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception ex) {
            this.logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
