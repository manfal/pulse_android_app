package com.human_pulse.datastore;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.human_pulse.actions.StoreActions;
import com.human_pulse.analytics.logging.Constants;
import com.human_pulse.analytics.logging.ILog;
import com.human_pulse.analytics.logging.Logger;
import com.human_pulse.utilities.VolleyInstance;

import org.json.JSONObject;

import static com.human_pulse.constants.Constants.DEPARTMENT_LIST;
import static com.human_pulse.constants.Constants.REQUEST_RETRY_TIMEOUT;
import static com.human_pulse.constants.Constants.REQUST_RETRY_COUNT;
import static com.human_pulse.constants.HttpRoutes.STORE_DEPARTMENTS_ROUTE;

/**
 * Created by anfal on 12/13/2015.
 */
public class StoreController {
    private ILog logger = null;
    private Context context = null;

    public StoreController(Context context) {
        this.logger = Logger.getLogger();
        try {
            this.context = context;
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }

    public void getStoreDepartments() {
        try {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, STORE_DEPARTMENTS_ROUTE, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        StoreActions.getStoreActionInstance().populateStoreDepartments(response.getJSONArray(DEPARTMENT_LIST));
                    } catch (Exception ex) {
                        logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(error), error.getCause());
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(REQUEST_RETRY_TIMEOUT, REQUST_RETRY_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
        } catch (Exception ex) {
            logger.log(Constants.ERROR, Constants.TAG_ERROR, Log.getStackTraceString(ex), ex);
        }
    }
}
